# FAQs

----------

1. [How do I make sure the WAHTS is charging?](#how-do-i-make-sure-the-wahts-is-charging)
2. [How do I reboot the WAHTS?](#how-do-i-reboot-the-WAHTS)
3. [The connection is dropping between the WAHTS and TabSINT. What should I do?](#the-connection-is-dropping-between-the-wahts-and-tabsint.-what-should-I-do)
4. [I keep seeing the word "CHA", what is that?](#i-keep-seeing-the-word-"cha",-what-is-that)


### How do I make sure the WAHTS is charging?

Make sure the WAHTS is turned *On* while charging, by sliding the switch in the red ear cup to the right. 

<div style="text-align:center"><img src="switch.png" width="60%"/></div>

At this time, there is no light or LED to indicate the WAHTS is charging or to indicate when charging is complete. A few hours should be plenty to fully charge the WAHTS. Once charged, battery levels can be checked by connecting to CHAMI or [TabSINT](../QuickStart/WAHTS#Connecting-WAHTS-to-TabSINT).
  
### How do I reboot the headset?


  
### The connection is dropping between the WAHTS and TabSINT. What should I do?

###I keep seeing the word "CHA", what is that?