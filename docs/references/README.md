# References

## Tablet Speech In Noise Test (TabSINT)

- [2017 Internet & Audiology Presentation](http://creare-com.gitlab.io/tabsint/docs/references/2017-Internet-Audiology/index.html)
- [2016 IHCON Poster](http://creare-com.gitlab.io/tabsint/docs/references/2016-IHCON.pdf)
- [2016 American Auditory Society Presentation](http://creare-com.gitlab.io/tabsint/docs/references/2016-AAS/index.html)


## Wireless Automated Hearing-Test System (WAHTS)

- [2017 Internet & Audiology Poster](http://creare-com.gitlab.io/tabsint/docs/references/2017-Internet-Audiology-WAHTS.pdf)
- [Meinke, D. K., Norris, J. A., Flynn, B. P., & Clavier, O. H. (2017). Going wireless and booth-less for hearing testing in industry. *International Journal of Audiology*, 56(sup1), 41-51.](http://dx.doi.org/10.1080/14992027.2016.1261189)
- [2016 NHCA Presentation](http://www.hearingconservation.org/resource/resmgr/2016_Conference/Presentations/NHCA_2016_-_Going_wireless_a.pdf)
