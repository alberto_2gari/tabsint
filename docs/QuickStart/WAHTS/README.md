# Wireless Automated Hearing-Test System (WAHTS) Quick Start Guide

<h3>*Note:*</h3>
----------------------------------------------------
>**In documentation and in TabSINT you may come across the word *CHA*. *CHA* is the nomenclature for the internal electronics inside the WAHTS and inside some other hearing assessment devices. For the sake of ease, consider the two terms interchangeable.**

# Review Contents
## WAHTS in the Box?
<div style="text-align:center"><img src="wahts-in-the-box.png" width="60%"/></div>

Unless otherwise arranged, your package should contain the following:

- One Wireless Automated Hearing-Test System (WAHTS)
- One micro USB cable with wall charger
- Rechargeable lithium-polymer battery (internal)
- One sturdy travel case to carry the above items

# Basic Operation

The WAHTS have an internal rechargeable lithium-polymer battery that must be charged before use. Once received, WAHTS should be turned *On* and may be left *On*. WAHTS only need to be turned off prior to shipment, due to shipping regulations.

## On/Off Switch

- Each WAHTS has an *On/Off* switch in the right/red ear cup. 
- Slide the switch to the *right* to turn it *On*. Sliding to the *left* disconnects the battery.

<div style="text-align:center"><img src="switch.png" width="60%"/></div>

- Once the WAHTS is turned *On*, switch should remain in the *On* position. The WAHTS only needs to be turned *Off* prior to shipping.


## Charging the WAHTS

- Plug the micro USB charger into the port in the left/blue ear cup and connect to the wall charger. <b>The switch must be in the *On* position for the WAHTS to charge.</b>

<div style="text-align:center"><img src="charging.png" width="60%"/></div>

<h3>Note:</h3>
----------------------------------------------------
>The micro USB chargers have an orientation. It is important to plug them in correctly to not damage the charging port.


<div style="text-align:center"><img src="port.png" width="60%"/></div>

- A few hours should be enough to fully charge the battery. There is no indicator on the WAHTS to inform the level of battery charge, however [connecting to the TabSINT](Connecting-WAHTS-to-TabSINT) program allows for review of battery charge. 




## Sleep and Wake 

The WAHTS will enter a low-power idle state automatically after 15 minutes of inactivity; Bluetooth communication with the WAHTS is impossible in it’s in the idle state.  

### To wake WAHTS from idle

- Tap the blue ear cup robustly - this will activate the “shake-to-wake” feature.  

![Shake to Wake](BT.mp4)


Looking closely through the black cloth inside the blue earcup, flashing green and blue lights should be visible after the headset has booted up and is ready for operation.

## Operating

The headsets communicate over Bluetooth Low Energy (also known as Bluetooth Smart, or Version 4.0+). 

# Connecting WAHTS to TabSINT

If you haven't yet installed TabSINT, return to the [Quick Start TabSINT](..QuickStart/TabSINT/) portion of this document.

- Enter the *Admin View* of TabSINT
- Under the *Setup* tab, scroll down to the *WAHTS* panel
- Make sure the Tablet's Bluetooth is turned on, or you will see the message "Bluetooth radio disabled"

<div style="text-align:center"><img src="BT-enabled.png" width="60%"/></div>

- Make sure the headset is turned *On* with blue lights flashing ![Shake to Wake](BT.mp4)
- Select the *Connect* button and a pop up will appear with the Bluetooth Devices that are active and in range. 
- Select the serial number of the headset you wish to connect

<div style="text-align:center"><img src="pop-up.png" width="60%"/></div>

- Once connected a *CHA* panel will appear. This panel identifies the *Name* or serial number, the *Firmware* installed, and the *Battery Level* of the connected CHA (WAHTS).

<div style="text-align:center"><img src="cha.png" width="60%"/></div>

To begin using the WAHTS with TabSINT, review the [Getting Started](..UserGuide/getting-started/) and [WAHTS](..UserGuide/wahts/) section of the User Guide.  




