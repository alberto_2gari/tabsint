# Common Response Area Properties #

The following options can be used with any response area.

## Options ##

- ``autoSubmit``:
  
  **Type**: `boolean`
  
  **Description**: Submit as soon as the response is defined.  [Default = False].  This should not be used for responseAreas that allow multiple inputs
  

- ``responseRequired``:
  
  **Type**: `boolean`
  
  **Description**: Require response to be defined.  Setting to false allows empty submission.  [Default = True]
  

- ``enableSkip``: 
  
  **Type**: `boolean`
  
  **Description**: Show a 'skip' button that allows user to skip this question.  [Default = False],
  

## Protocol ##
```
{
   ...
   "responseArea":{
      "type":"any-response-areas",
      "autoSubmit": true,
      "responseRequired": true,
      "enableSkip": false
   }
{
```

## Schema ##
```
"commonResponseAreaProperties": {
  "description": "Response areas - common properties supported across all response areas",
  "type": "object",
  "properties": {
    "autoSubmit": {
      "description": "Submit as soon as the response is defined.  [Default = False].  This should not be used for responseAreas that allow multiple inputs",
      "type": "boolean"
    },
    "responseRequired": {
      "description": "Require response to be defined.  Setting to false allows empty submission.  [Default = True]",
      "type": "boolean"
    },
    "enableSkip": {
      "description": "Show a 'skip' button that allows user to skip this question.  [Default = False]",
      "type": "boolean"
    }
  }
},
```
