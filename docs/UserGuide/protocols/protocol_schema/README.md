# Protocol Schema #

The complete TabSINT protocol schema is [hosted in the the source code repository](https://gitlab.com/creare-com/tabsint/tree/master/www/res/protocol/schema). See [http://json-schema.org/](http://json-schema.org/) for more information on how these files are structured.
