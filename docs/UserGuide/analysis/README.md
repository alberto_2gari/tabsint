# Interpreting Results #

## Data Format ##

Tests results are stored on the tablet and server in JSON format.  The schema for this file is copied below in the
[Results Schema](#results-schema) section.

### JSON Format ###

Exam results can be downloaded in their native JSON format. JSON can be loaded into most analysis environments, including Python and MATLAB using the [JSONLab toolbox](https://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-files).

JSON-formatted exam results contain all of the information that was uploaded to the server in a highly accessible format. It is likely the best format for advanced post-processing of results in MATLAB or Python.

### Excel or CSV Format ###

The Excel and .CSV formats are easy to load and may be the easiest format to use for simple
or ad-hoc post-processing.

These files may be opened in common spreadsheet software such as
Microsoft Excel.  Viewing in Excel is helpful for a "quick-look" at the data.

Each row in the Excel or .CSV file represents a single 'result'. Consequently, there
are *many* rows for each exam that is downloaded. Data that apply to the entire exam
(such as siteID) are duplicated for every presentation.

Although this format is verbose, it enables very rapid sorting of the data by any of
the exposed parameters. Once the data are sorted, various statistical analyses can
be applied.

These files can also be loaded into MATLAB using its CSV or XLSX import facilities.

## Results Schema ##

See [JSON Schema Documentation](http://json-schema.org/) for more info on how to use and read JSON Schema files.
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "HFFD Exam Results Schema (unversioned)",
  "required": ["protocolId", "protocolHash", "testResults"],
  "properties": {
    "buildName": {
      "type": "string",
      "description": "Unique identifier for the tabSINT build on the tablet reporting results."
    },
    "siteId": {
      "type": "string",
      "description": "Unique numerical site identifier where the exam was taken.  This is converted to 'siteName' on the server."
    },
    "testDateTime": {
      "type": "string",
      "format": "date-time",
      "description": "Date/time that the exam was started."
    },
    "qrString": {
      "type": "string",
      "description": "The string decoded from the QR code associated with the exam."
    },
    "gender": {
      "enum": [ "m", "f"]
    },
    "age": {
      "type": "integer"
    },
    "protocolId": {
      "type": "string",
      "description": "Unique numerical protocol id.  This is converted to 'protocolName' on the server."
    },
    "protocolHash": {
      "type": "string",
      "description": "A unique hash identifying the protocol used to obtain these results."
    },
    "audiometryResults": {
      "type": "object",
      "description": "Results of audiometry testing as reported in QR code."
    },
    "nCorrect": {
      "type": "number",
      "description": "How many responses were correct?"
    },
    "nIncorrect": {
      "type": "number",
      "description": "How many responses were incorrect? (only 'false' responses are counted.)"
    },
    "nResponses": {
      "type": "number",
      "description": "How many total responses were there? (includes responses that are not right/wrong, such as questionnaire responses.)"
    },

    "testResults": {
      "required": ["responses"],
      "description": "JSON object containing any test results information not captured above. Is likely to contain fields not formally defined in this spec.",
      "properties": {
        "responses": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/response"
          }
        },
        "softwareVersion": {
          "properties": {
            "version": {
              "type": "string",
              "description" : "The TabSINT software semantic version"
            },
            "date": {
              "type": "string",
              "description": "The TabSINT software build date"
            },
            "rev": {
              "type": "string",
              "description": "The TabSINT software revision number"
          }
        }
       },
        "tabletUUID":{
          "type": "string",
          "description": "The tablet unique identifier"
        },
        "tabletModel":{
          "type": "string",
          "description": "The tablet model"
        },
        "tabletLocation":{
          "latitude": {
            "type":"string",
            "description":"latitude"
          },
          "longitude": {
            "type":"string",
            "description":"longitude"
          }
        },
        "partialResults":{
          "type":"boolean",
          "description":"true if the exam was ended early and the results are partial"
        }
      }
    }
  },
  "definitions": {
    "response": {
      "description": "Details of subject response to a single presentation.",
      "properties": {
        "presentationId": {
          "type": "string",
          "description": "Unique (w/in this exam) identifier for this presentation."
        },
        "response": {
          "type": "string",
          "description": "A string representing the response."
        },
        "correct": {
          "type": "boolean",
          "description": "True if this is the 'correct' response. False if it is incorrect. Undefined if this is not a right/wrong question."
        },
        "numberCorrect":{
          "type": "number",
          "description": "number of correct responses within a question.  For example, 0-5 for OmtResponseArea"
        },
        "numberIncorrect":{
          "type": "number",
          "description": "number of incorrect responses within a question.  For example, 0-5 for OmtResponseArea"
        },
        "eachCorrect":{
          "type": "array",
          "description": "Array.  True/False if correct is set for that choice, null if correct is not defined."
        },
        "responseStartTime": {
          "type": "string",
          "format": "date-time",
          "description": "Time that each question appears in standard format"
        },
        "responseElapTimeMS": {
          "type": "number",
          "description": "Response between question appearing and question being submitted, in milliseconds"
        },
        "notes": {
          "type": "string",
          "description": "Contains any warnings generated by the TabSINT program during execution of the page"
        },
        "otherResponse": {
          "type": "string",
          "description": "User-supplied input to the 'other' field of a checkbox response area"
        },
        "changedFields": {
          "type": "object",
          "description": "page fields modified by a functionRegistry function, called by the protocol for an individual page"
        }

      }
    }
  }
}
```
