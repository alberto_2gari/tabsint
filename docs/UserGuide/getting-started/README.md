# Getting Started #

This brief section gives an overview of how to start, configure, and use the TabSINT software.

## Starting TabSINT ##

TabSINT opens to the welcome screen, showing the options:

<div style="text-align:center"><img src="welcome.png" width="60%"/></div>

- **Exam View**: This option will open the exam view for the current active protocol. If no protocol has been loaded, the page will display *Exam Disabled*.
- **Admin View**: This option will open the administrator console and may require a password to access. The admin console provides the ability to load protocols, view results, and configure the application.
- **Documentation**: This option will open this documentation in a local browser window on the tablet.
  

---

## Configuring TabSINT ##

To access the configuration options in TabSINT:

- From the welcome screen (or the options menu in the top-right), select **Admin View**
- If prompted, type in the **Admin PIN** and click *OK*
- You are now in the admin area, with access to [Configuration Tab](#configuration-tab), [Protocols Tab](#protocols-tab), and [Results Tab](#results-tab)
  

## Configuration Tab ##

- ``Admin Mode``: Includes additional configuration options, displays expandable **Debug View** menus at the bottom of exam pages, and suppresses **Admin Pin** prompts. Leave this option *unchecked* unless you are developing protocols or making configuration changes. 
- ``Disable Logs``: Disables automatic software logging in the TabSINT software.
- ``Disable Automatic Volume Control``: This option will disable TabSINT from setting the volume to 100% on every page. Check this box if you would like to set the volume of the app manually using the volume buttons on the outside of the device.
  

- ``Server``: Select the appropriate location for TabSINT to source new protocols and upload results. See the `Data-Interface <data-interface.html>`_ section for more information.
  
  - ``TabSINT Server``: Use a TabSINT specific server to download protocols and export results based on site names
  - ``Gitlab``: Use your own Gitlab repository to download protocols, export results and for using a common media repository
  - ``SD Card``: Use your own SD card to download protocols and export results


<div style="text-align:center"><img src="general-config.png" width="60%"/></div>

### Gitlab Configuration ###

See [Gitlab](../data-interface/gitlab) documentation for more information.

- ``Host``: The web address to your gitlab account (i.e. ``https://gitlab.com/``)
- ``Token``: The personal access token to your gitlab account
- ``Group``: The group name of your gitlab repository. This can include subgroups

<div style="text-align:center"><img src="gitlab-server-config.png" width="60%"/></div>

### TabSINT Server Configuration ###

- ``URL``: The web address to your TabSINTs server
- ``Username`` and ``Password``: Your login credentials
- ``Valid``: This will validate your login credentials against the input url
  
<div style="text-align:center"><img src="tabsint-server-config.png" width="60%"/></div>

### Plugin Configuration ###

- Plugin configuration panels will be listed below the server configuration options
- Plugin panels will be distinguished by an outlet icon and different color background
  

### Calibration Configuration ###

- ``Headset``: Ensure that this matches the type of headset that you have connected to the tablet (if any).
  
  - If this value is set incorrectly, then the tablet audio will **not** be calibrated properly.

- ``Tablet``: The current tablet model will be displayed under the headset. This cannot be changed.

<div style="text-align:center"><img src="calibration-config.png" width="60%"/></div>

### Update TabSINT ###

This panel provides a link to the latest version of TabSINT. 
For public releases of TabSINT, this link will display the version of the latest TabSINT release.

This link can be configured in build configuration of the file. 
See the [Developer Documentation](../../DeveloperGuide) for more information.

### Protocols Tab ###

<div style="text-align:center"><img src="calibration-config.png" width="60%"/></div>

### Protocols ###

The protocols view lists the protocols already loaded into tabsint.
The currently active protocol will be shaded yellow.

- TabSINT can hold many protocols locally, but only one protocol is active at a time
- Each protocol currently available on the tablet is listed by name, date and server type
  
Protocols are loaded, updated, and deleted by tapping on the Protocols table.

- ``Load``: Load, Validate, and Activate the selected protocol active in the Exam View
- ``Update``: Check for new versions (for gitlab server and TabSINT server only)
- ``Delete``: Remove that protocol from the locally available protocols.
  
  - The protocol information will be removed, and the protocol files will be deleted unless the protocol was loaded via the Device Storage.
    


### Protocol Source View ###

Based on the **Server** seleted in the [Configuration Tab](#configuration-tab), different options will be active under the protocols view.
For more information on sourcing protocols, see the [Data Interface](../data-interface) section.

- **Download Protocol from Gitlab Server** (if ``Protocol Source`` is set to ``Gitlab``)
  
  - ``Name``: Enter the respository (a.k.a. *project*) name for the desired protocol.  This project must be within your configured gitlab namespace.
  - See [Data-Interface/Gitlab](../data-interface/gitlab#deploying-protocols) for more information

- **Add Local Protocols** (if ``Protocol Source`` is set to ``SD Card``)
  
  - ``Add Protocol``: Opens a window to select a protocol directory from the SD Card on the tablet
  - See [Data-Interface/SD-Card](../data-interface/sd_card#deploying-protocols) for more information

- **Download Protocol from TabSINT Server** (if ``Protocol Source`` is set to ``TabSINT Server``)
  
  - ``Site Name``: Ensure that this site matches siteName configured on the TabSINT server web interface. This value should be provided by the study coordinator, if required.
    


## Results Tab ##

<div style="text-align:center"><img src="results.png" width="60%"/></div>

### Completed Tests ###

This view lists all the test results currently stored in TabSINT's memory.

- Details include protocol name, number of presentations completed, protocol source, and start time
- Tap a specific result to pull up a window showing the details of that result
  
  - Tap any field to expand.  For example, to see responses to specific questions, tap ``testResults``, then
    ``responses``
  - ``Upload``: For Gitlab and TabSINT results servers.  Upload this specific result now.
  - ``Export``: Export results to a local file on the SD Card (The Results Directory can be changed if the results
    location is set to ``SD Card``)

- ``Export All``: Save all results to a local file in the specified ``Results Directory``
  
<div style="text-align:center"><img src="results-detail.png" width="60%"/></div>

### Test Export View ###

Depending on the **Server** seleted in the **Configuration** Tab, different views will be active under the *Completed Tests* view.
For more information on outputing results, see the [Data-Interface](../data-interface) section.

- Local Output (if ``Server`` is set to ``SD Card``)
  
  - ``Change Results Directory``: Open a file browser to select a new folder on the tablet SD Card for exporting results

- Upload Results (if ``Server`` is set to ``TabSINT Server`` or ``Gitlab``)
  
  - ``Automatically upload exams``: If selected, then results are uploaded automatically upon completion and/or whenever network connectivity is available.
    - Creare recommends leaving this option enabled.

  - ``Upload All Results``: Push all stored results to the TabSINT Server
    


---

## Administering an Exam ##

While administering an exam may depend on the exact protocol in use, the steps to begin and end an exam are the same:

1. Load a protocol in **Protocols** tab of the **Admin View**.
2. Navigate to the **Exam View**.
3. Be sure that the subject is ready to start the exam, and communicate any necessary oral instructions.
4. Press **Begin** and hand the tablet to the subject.
5. Administer the protocol. When the protocol finishes, the subject should return the tablet to the administrator.
6. Once the exam reaches the final screen, the results will automatically queue for export. The exam results will also get backed up in a local text file on the tablet for safe keeping. At this point, the exam results should be  be viewable in **Results** tab of the **Admin View**.
   - If TabSINT is configured to upload results to a remote server and *Automatically upload exams* is selected, the exam result will attempt to get uploaded at this point.  If the exam is successfully uploaded, it will be removed from the **Results** tab. ####

Press the ``New Exam`` button if you would like to run another exam on the same protocol.

---

## Exporting Exam Results ##

Stored results can be viewed on the **Results** tab of the *Admin View*.
Results are kept in the queue on the tablet until they are uploaded to a configured server or deleted by an administrator.

<div style="text-align:center"><img src="results.png" width="60%"/></div>

### Export Single Result ###

- Select the desired *server* for results ouput in the **Configuration** tab
  
  - Make sure your selected results output is properly configured

- Click the result entry in the table found in the *Completed Tests* view of the **Results** tab.
- Select **Export** to export the results to a file on the SD card, or **Upload** to send the result to the chosen server.
  

### Exporting all results ###

To export all results, press the **Export All** button below the *Completed Tests* table.

- Exporting results **will not** remove them from the *Completed Tests* table.
  

If you have selected a server for your results ouput, you can press **Upload All** in the *Upload Results* view below *Completed Tests*.

- Uploading results **will** remove results from the *Completed Tests* table.
  

---

## Volume Errors and Alerts When Using Headsets ##

> **Note:** These alerts and errors will most likely only show up when a Headset is plugged in. 
> Before giving the tablet to a subject to administer an exam, plug in a Headset, reset the exam, and press begin to make sure volume control is functioning properly.


TabSINT attempts to change the volume of the tablet to 100% on many different tablet events.
This controls the output volume of the tablet while playing calibrated media.

Creare has specifically adapted tablets to be used with TabSINT that allows TabSINT full control over media volume.
TabSINT may have trouble resetting the volume on tablets that have not been adapted to support this feature and will present a pop-up message:
"Listening at a high volume for a long time may damage your hearing.  The volume will be increased above safe levels."

### To Enable Full-Scale Volume Control From Within TabSINT ###

- In most cases you have the option to press 'Cancel' or 'OK'.
- Press 'OK' to allow TabSINT to set volume levels in the future.  This setting should persist until the device is rebooted.
- Press 'OK' for any TabSINT 'ERROR' messages about volume not being properly set.
- Reset the exam to allow the change to take effect.
- The TabSINT 'ERROR' messages should stop appearing once the changes take effect.  If they do not, continue reading.
  

If you see the error message:
"ERROR: Volume not properly set.  Return device to administrator.  See documentation for how to avoid this error in the future." but you do NOT see the "Listening at high volume..." message, then follow the instructions below to enable full-scale volume control.

### To Enable Full-Scale Volume Control Using Phone Settings ###

- Go to the 'Settings' menu for the phone or tablet.  This may require sliding down from the top right, sliding up from the bottom right, or pressing a home button.
- Go to the volume section.
- Plug in a headset.
- Try to raise the volume to max level.
- If you get a warning about high volume, select the option that allows you to turn the volume up, usually the 'OK' option.
