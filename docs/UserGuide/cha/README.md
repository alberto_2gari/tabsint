
# WAHTS

The WAHTS is a hearing assessment device that can be used as a peripheral to TabSINT.

## Basics

To run a protocol with WAHTS exam pages:

1.  Make sure the WAHTS is powered on
2.  Navigate to the admin page in TabSINT, and scroll to the *WAHTS* panel
3.  Select the appropriate Bluetooth Mode
4.  Tap *Connect*
5.  Select the appropriate WAHTS from the list that pops up
6.  The connection is complete when WAHTS info, such as battery level and WAHTS serial number, appear in the panel

## CHAMI

For advanced configuration, the WAHTS can be configured using the **CHAMI** Matlab interface.

## Streaming Audio to the WAHTS

The WAHTS can be used as a standard bluetooth headset to play any audio from the tablet.  To use the WAHTS as a bluetooth
headset, make sure to select "Creare Headset" from the *Headset* drop-down.  With *Headset* set to "Creare Headset", the
audio from any media will play through the WAHTS.

## Loading and Playing Audio Files on the WAHTS

TabSINT can play audio files that are stored on the WAHTS. 
These audio files may be located within the WAHTS file system, or they may be audio files loaded onto the WAHTS by the user using CHAMI.

## Loading Audio Files

To load audio files onto the WAHTS using CHAMI, see the `write_file` method in the CHAMI user guide.
Files loaded by the user onto the WAHTS will be placed in the `USER` directory. 
When referencing a file in a TabSINT protocol, the path must be prefixed by `C:USER`.

## Playing Audio Files on the WAHTS

Any exam page can play audio files that are stored on the WAHTS. 
Playing wav files on the cha is very similar to playing wav files on the tablet.
Instead of using the `wavfiles` key, you would now use the `chaWavFiles` key.
See the `chaWavFiles` key within the `page` field of the protocol schema for the for precise syntax.

Below are two examples showing how to play a wav file on the WAHTS. The first example shows how to reference a wav file that is preloaded on the WAHTS, and the second shows how to reference a wav file that is loaded by the user onto the WAHTS:

```json
{
  "id":"wahtsWavFileExample",
  "title":"ChaWavs",
  "questionMainText": "This page plays a wav file stored on the CHA.  What did you hear?",
  "chaWavFiles":[
    {
      "path": "C:HINT/LIST1/TIS001.WAV"
    }
  ],
  "responseArea":{
    "type":"multipleChoiceResponseArea",
    "choices":[
      {
        "id":"Choice 1"
      },
      {
        "id":"Choice 2"
      }
    ]
  }
}
```

```json
{
  "id":"chaWavFileExample",
  "title":"ChaWavs",
  "questionMainText": "This page plays a wav file stored on the CHA.  What did you hear?",
  "chaWavFiles":[
    {
      "path": "C:USER/dir/myfile.wav"
    }
  ]
}
```
