
# Audiometry

TabSINT and The CHA peripheral offer Hughson Westlake audiometry exams with a number of options:

* Automated vs Manual
* Threshold vs Screener

## Automated Screener

The Automated Screener exam repeats pulse trains at the same frequency and level a set number of times, and keeps track of how many
the listener hears.  The result is Pass/Fail, based on whether the listener heard a set number of pusle trains or not.

The implementation uses the HughsonWestlake responseArea, with the optional *Screener* field.

### Example: Automated Audiometry Screener

```json
{
  "id": "left_ear",
  "title": "Audiometry Screener Demo",
  "questionMainText": "Level: 45dB SPL, Ear: Left",
  "helpText": "Follow instructions",
  "instructionText": "Tap the button once for each set of sounds you hear",
  "responseArea": {
    "type": "chaHughsonWestlake",
    "examInstructions" : "Tap the button once for each sound you hear.",
    "examProperties": {
      "LevelUnits": "dB SPL",
      "Lstart": 45,
      "F":1000,
      "TonePulseNumber":3,
      "OutputChannel": "HPL0",
      "UseSoftwareButton": true,
      "PollingOffset": 1000,
      "MinISI":1000,
      "MaxISI":3000,
      "Screener": true,
      "PresentationMax": 3,
      "NumCorrectReq": 2
    }
  }
}
```

## Automated Threhsold

The Automated Threshold exam repeats pulse trains at the same frequency but with dynamic level, in search of the listener's
threshold hearing level at the set frequency.  The result is a hearing threshold and the level progression the algorithm
followed to converge on the returned threshold.

### Example: Automated Audiometry Threshold

```json
{
  "id": "left_ear",
  "title": "Audiometry Hughson Westlake Demo",
  "questionMainText": "Starting Level: 45dB SPL, Ear: Left",
  "helpText": "Follow instructions",
  "instructionText": "Tap the button once for each set of sounds you hear",
  "responseArea": {
    "type": "chaHughsonWestlake",
    "examInstructions" : "Tap the button once for each sound you hear.",
    "examProperties": {
      "LevelUnits": "dB SPL",
      "Lstart": 45,
      "F":1000,
      "TonePulseNumber":3,
      "OutputChannel": "HPL0",
      "UseSoftwareButton": true,
      "PollingOffset": 1000,
      "MinISI":1000,
      "MaxISI":3000
    }
  }
}
```

## Manual Audoimetry

For more flexibility and control over the exam, audiometry exams can also be run manually.  In manual audiometry
response areas, the test administrator can select frequency, level, and ear for each sound presentation.  The administrator
also indicates when a threshold has been found for each frequency/ear combination.  Any combination can be repeated or
skipped.

### Example: Manual Audiometry Threshold

```json
{
  "id": "manualaudiometry",
  "title": "Manual Audiometry",
  "questionMainText": "Manual Audiometry",
  "helpText": "Follow instructions",
  "responseArea": {
    "type": "chaManualAudiometry",
    "presentationList": [
      { "F": 1000 },
      { "F": 2000 },
      { "F": 3000 },
      { "F": 4000 },
      { "F": 6000 },
      { "F": 1000 },
      { "F": 500 },
      { "F": 250 }
    ],
    "examProperties": {
      "LevelUnits": "dB SPL",
      "TonePulseNumber": 5,
      "Lstart": 50,
      "OutputChannel": "HPR0",
      "UseSoftwareButton": true,
      "PollingOffset": 600
    }
  }
}
```

For the screener version, the test administrator indicates pass/fail instead of indicating a threshold has been found.

### Example: Manual Audiometry Screener

```json 
{
  "id": "ManualScreener",
  "title": "Manual Screener",
  "questionMainText": "Manual Screener",
  "helpText": "Follow instructions",
  "responseArea": {
    "type": "chaManualAudiometry",
    "minLevel": 0,
    "maxLevel": 50,
    "responseType": "pass-fail",
    "presentationList": [
      { "F": 1000 },
      { "F": 500 },
      { "F": 1000 },
      { "F": 2000 },
      { "F": 3000 },
      { "F": 4000 },
      { "F": 6000 },
      { "F": 8000 }
    ],
    "examProperties": {
      "LevelUnits": "dB HL",
      "Lstart": 25,
      "TonePulseNumber": 5,
      "OutputChannel": "HPL0",
      "UseSoftwareButton": true,
      "PollingOffset": 1000,
      "MinISI":1000,
      "MaxISI":3000
    }
  }
}
```

## Run a list of Audiometry Exams

To run several autiometry exams with similar properties, use the *chaAudiometryList* responseArea.

* Set page properties for all exams, such as *autoSubmit* or *repeatOnce*, in the *commonResponseAreaProperties* field.
* Set exam properties for all exams, such as *F* or *Lstart*, in the *commonExamProperties* field. These properties will
be applied to every exam.
* Set individual exam properties for each exam in *presentationList*.  These exam properties will override *commonExamProperties*.

```json 
{
  "id": "AudiometryList",
  "title": "Left Ear List",
  "questionMainText": "Hughson-Westlake",
  "helpText": "Follow instructions",
  "instructionText": "This test measures your hearing sensitivity.  You will hear sounds at different pitches one ear at a time.  Your task is to tap the button when you hear a sound, no matter how soft the sound may be.",
  "responseArea": {
    "type": "chaAudiometryList",
    "presentationList": [
      {"F": 500},
      {"F": 1000},
      {"F": 2000, "Lstart": 50}
    ],
    "commonExamProperties": {
      "Lstart": 40,
      "UseSoftwareButton": true,
      "LevelUnits": "dB SPL",
      "OutputChannel": "HPL0"
    },
    "commonResponseAreaProperties": {
      "autoBegin": true,
      "autoSubmit": true,
      "repeatOnce": true
    }
  }
}
```

## Repeating Individual Audiometry Exams

Individual Audiometry exams have the ability to automatically repeat an exam if the exam does not successfully find a
threshold.

* *repeatIfFailedOnce* - Display a message encouraging the listener to listen carefully and then repeat a failed exam.
This option will present a repeat of the failed exam, using all the same examProperties.  The failed exam results will
be overwritten with the results of the repeat.
* *getNotesIfFailedTwice* - Collect notes from the test administrator upon a second failure.  This option
will display a message asking the listener to hand the tablet to the study administrator and provide a notes text box.

```json
{
  "id": "left_ear",
  "title": "Audiometry Hughson Westlake Demo",
  "questionMainText": "Starting Level: 45dB SPL, Ear: Left",
  "helpText": "Follow instructions",
  "instructionText": "Tap the button once for each set of sounds you hear",
  "responseArea": {
    "type": "chaHughsonWestlake",
    "examInstructions" : "Tap the button once for each sound you hear.",
    "repeatIfFailedOnce": true,
    "getNotesIfFailedTwice": true,
    "examProperties": {
      "F":1000,
      ...
    }
  }
}
```

## Repeating Groups of Audiometry Exams

The *AudiometryList* exam makes it possible to run a group of audiometry exams.  The following options are available for
repeat logic to handle cases where one or more exams with the group do not successfully find a threshold:

* *repeatExamsThatFailedOnce* - Once all exams in the group have been run, if any failed, the failed exams will be repeated after a message
to the listener asking them to listen carefully.
* *getNotesIfAnyExamsFailedTwice* - Once all failed exams have been repeated once, if any repeats failed, show the listener a
message asking them to hand the tablet to the study administrator, and provide a notes text box for the administrator
to enter any relevant notes.

```json 
{
  "id": "AudiometryList",
  "title": "Left Ear List",
  "questionMainText": "Hughson-Westlake",
  "helpText": "Follow instructions",
  "instructionText": "This test measures your hearing sensitivity.  You will hear sounds at different pitches one ear at a time.  Your task is to tap the button when you hear a sound, no matter how soft the sound may be.",
  "responseArea": {
    "type": "chaAudiometryList",
    "repeatExamsThatFailedOnce": true,
    "getNotesIfAnyExamsFailedTwice": true,
    "presentationList": [
      {"F": 500},
      {"F": 1000},
      {"F": 2000, "Lstart": 50}
    ],
    "commonExamProperties": {
      "Lstart": 40,
      "UseSoftwareButton": true,
      "LevelUnits": "dB SPL",
      "OutputChannel": "HPL0"
    },
    "commonResponseAreaProperties": {
      "autoBegin": true,
      "autoSubmit": true
    }
  }
}
```
