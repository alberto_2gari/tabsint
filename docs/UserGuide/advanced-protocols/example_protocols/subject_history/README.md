# Subject History Example #

## Description ##

This short protocol runs a Hughson Westlake audiometry exam at a Frequency specified by either a subject's history or by a user input.
   

## Protocol ##

Below is an example *protocol.json*.  The protocol includes:

- A subject id response area, where the user can input a subject id

- If subject history is available for the subject id entered, the user will go directory to the audiometry exam
- If subject history is *not* available, the user will be directed to an input response area to enter it
- The preprocessing function on the audiometry exam handles how to specify the frequency input
  
```json
{
  "title": "Demonstration of Subject History",
  "pages": [
    {
      "id": "subjectId",
      "title": "Subject ID",
      "questionMainText": "Enter the Subject ID",
      "responseArea": {
        "type": "subjectIdResponseArea"
      },
      "followOns":[
        {
          "conditional":"_.has(flags.subjectHistory, result.response)",
          "target": {
            "reference": "HW_demonstration"
          }
        },
        {
          "conditional": "!_.has(flags.subjectHistory, result.response)",
          "target": {
            "reference":"inputF"
          }
        }
      ]
    }
  ],
  "subProtocols": [
    {
      "protocolId": "inputF",
      "pages":[
        {
          "id": "inputF",
          "title": "Input F Left",
          "questionMainText": "Input Frequency",
          "responseArea": {
            "type": "integerResponseArea"
          }
        },
        {
          "reference" : "HW_demonstration"
        }
      ]
    },
    {
      "protocolId": "HW_demonstration",
      "pages": [
        {
          "id": "HW",
          "title": "Hughson-Westlake Level Exam",
          "preProcessFunction": "retrieveF",
          "responseArea": {
            "type": "chaHughsonWestlake",
            "examInstructions": "Tap the button once for each set of sounds you hear",
            "examProperties": {
              "LevelUnits": "dB SPL",
              "F": "F",
              "OutputChannel": "HPL0",
              "UseSoftwareButton": false
            }
          }
        }
      ]
    }
  ]
}
```

## Javascript ##

Below is an example *customJs.js* to accompany the *protocol.json*.

```javascript
(function() {

  tabsint.register('retrieveHAF', function(api) {

    var Freq;
    var history = api.flags.subjectHistory;
    var subject = api.examResults.subjectId;

    // Try to retrieve HAF for either left of right ear
    try {
      Freq = retrieveF();
    } catch (e) {
      console.log('WARNING: Failed to retrieve HAF data from history. Error: ' + angular.toJson(e));
      Freq = undefined;   // This will throw an error in the exam to alert us
    }

    // Find Frequency in subject history or from Input Response Area
    function retrieveF() {
      var F;

      // if there is no history entry for this subject, try get input from previous integer response area
      if (_.isUndefined(history[subject])) {

        // look through all previous responses, find the one with the presentationId = 'inputF'
        _.each(api.examResults.testResults.responses, function(response) {
          if (response.presentationId === 'inputF'){
            F = parseInt(response.response);   // make an integer out the response
          }
        });
      }

      // otherwise, get F from subject history
      else {
        F = _.last(history[subject]).F
      }

      return F;
    }

    return {
      responseArea: {
        examProperties: {
          F: Freq
        }
      }
    };
  });


})();
```
