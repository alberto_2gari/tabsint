(function() {

  tabsint.controller('SummaryResultsCtrl', function ($scope, $cordovaFile) {
      var score = ($scope.nCorrect - $scope.nIncorrect)/$scope.nResponses;

      var path = 'testData',
      fileName = 'summaryResultsSection1.txt',
      text = 'The results from section 1 were: '+score;

      $cordovaFile.writeFile(cordova.file.externalRootDirectory, fileName, text);

  });

})();