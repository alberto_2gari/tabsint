(function() {

  tabsint.register('calculateProgress', function(dm) {

    var response = dm.result.response;
    var length = dm.examResults.responses.length;
    var progress;
    if (response === 'y') {
      progress = 'Question '+length+'\/5 in Section 4';// escaped forward slash
    } else {
      progress = 100 * length / 10;
    }
    
    // Return the proper object structure. In this case, we replace
    // 'progressBarVal' with a new value ('progress'). This change
    // will be recorded in the exam results for the current page.
    var retObject = {
      progressBarVal: progress
    };

    return retObject;
  });

});



