# Dynamically Calculated Page Properties #

The page ```preProcessFunction``` allows more advanced adaptive logic implementation BEFORE each page is displayed.  These functions can be used to adaptively modify any page property, including question texts, followOns, question type, flags, or the progress bar value.

## Implementation Overview ##

- Create a function that calculates any new or modified page properties and returns an object
  with just those modified properties. 
- If a page references that function (by name) as its `preProcessFunction`, then TabSINT runs the function and
  alters the 'page' specification before it's displayed.
- Any changes to the page are stored with the exam results for that page, so that during post-processing it is
  clear exactly what page was presented.
  

Note that the the pre-processing function only needs to specify the fields that need to be changed; these fields are updated, and all other
fields on the page remain the same.

## Required Code ##

The minimum required code for a dynamic function is:

### customJs.js ###

```javascript
(function() {

  tabsint.register('functionName', function(dm){
    var returnObject;

    // logic, use dm fields such as dm.result, read/write flags, etc.

    return returnObject;// returned fields, if any, will overwrite or add to current page fields
  };

})();
```

The controller must be registered using the global tabsint service ```tabsint.register('functionName', function() {})```.

### protocol.json ###

```
"pages":[
  {
    "id":"multichoice001",
    ...
    "preProcessFunction": "functionName"
  }
]
```

## Objects and Data Available to Dynamic Functions ##

The following functions and objects can be accessed via the input variable (```dm``` above) in a dynamic function.  For example, ```dm.page``` accesses the current page object.

**Libraries and Convenience Functions**:

- Most *native Javascript* functions (see [AngularJS's $eval](https://docs.angularjs.org/guide/expression) for specifics).
- The javascript Math library: standard math functions including:
  
  - ```Math.abs``` for absolute value
  - ```Math.min``` for minimum of two numbers
  - ```Math.max``` for maximum of two numbers

- The [Underscore Javascript Library](http://underscorejs.org): Provides numerous useful convenience functions and
  functional programming tools, such as:
  
  - ```filter``` to restrict items in a list based on some criteria
  - ```countBy``` to count items based on some criteria
  - ```shuffle``` to randomize a list
  - ```map```, ```reduce```, ```collect```, and numerous other important functional programming tools
    


**Exam Results (read-only)**

- ```result```: A copy of the response fom the previous (most recent) question.
- ```examResults```: A copy of the exam's entire results structure, similar to that downloaded for
  post-analysis. It contains the following fields:
  
  - ```protocolHash```
  - ```protocolId```
  - ```qrString```
  - ```siteId```
  - ```testDateTime```: for example, '2014-07-07T15:55:30.942Z'
  - ```testResults```: An array of test results objects
    
    - ```responses```: An array of response fields, including ```correct```, ```eachCorrect```, ```otherResponse```, ```presentationId```, ```response```, ```responseElapTimeMS```, ```responseStartTime```
      



**Modifiable State Objects (read/write)**

- ```flags```: All set flags. Refer to flags fields using dot notation,  e.g., ```flags.q1Answered``` for a flag named 'q1Answered'.
  

**Page Fields (read-only)**

- ```page```: The current page, including all page fields established by the *protocol.json*.
  

## Dynamically Altering Flags ##

Flags can be changed directly and can be used to store data or to pass data from page to page.  Flags are reset at the beginning of each exam. Fields are accessed and created using the 'dot' notation, i.e. ```flags.myVar = 2;```.

## Dynamically Changing Page Fields ##

The page field is read-only and cannot be altered directly. To modify page fields, return an object with a structure following the structure in the [JSON Schema](/docs/UserGuide/protocols/protocol_schema/).  Page field changes will be appended to the results structure for each page, to document what was changed.
   
```javascript
var retObject = {
  pageFieldToChange: newValue,
  questionMainText: newTextValue,
  progressBarVal: newProgressVal
  ...
};
return retObject;
```

It is important to note that *objects* (typically defined with curly braces `{}`) only need to contain the *changed*
fields, and that TabSINT does its best to deal intelligently with nested changes.

However, TabSINT completely replaces *arrays*  (typically defined with square `[]`).

For example, to change `questionMainText`, which is a direct child of page, we simply return `{questionMainText: newTextVariable}`
but for choices, which is a nested child of responseArea, we must return `{responseArea: {choices: newChoices}}` for the change
to be correctly placed.  Other fields, such as `{responseArea: {type:...}}' will be unaffected.

Note also that choices is an ARRAY according to the :ref:`json_schema`.  To add/change an element in an array,
save the current array to a new variable, add/change the element of interest, then return the updated array.

## Example:  Using Dynamic Functions to Modify Page Properties ##

Take for example, the progressBarVal, documented as a page field in the [JSON Schema](../../protocols/protocol_schema). Let's say you wanted to calculate the progress bar value using your own custom function, 'calculateProgress'.  This is how you would include your function in the protocol.json file using the preProcessFunction field:

```
{
  "title":"A Simple Exam With a Custom Function to Set the Progress Bar",
  "pages":[
    {
      "id":"multichoice001",
      "title":"Multiple Choice 1",
        "questionMainText":"Sample question.",
        "responseArea":{
          "type":"yesnoResponseArea"
        },
      "preProcessFunction": "calculateProgress"
    }
  ]
}
```

And how you would set the function in 'customJs.js', a single javascript file included with your protocol zip:

```
(function() {

  tabsint.register('calculateProgress', function(dm) {

    var response = dm.result.response;
    var length = dm.examResults.responses.length;
    var progress;
    if (response === 'y') {
      progress = 'Question '+length+'\/5 in Section 4';// escaped forward slash
    } else {
      progress = 100 * length / 10;
    }
    
    // Return the proper object structure. In this case, we replace
    // 'progressBarVal' with a new value ('progress'). This change
    // will be recorded in the exam results for the current page.
    var retObject = {
      progressBarVal: progress
    };

    return retObject;
  });

});
```

When page 'multichoice001' is loaded, the calculateProgress function will run and modify the progressBarVal

## Example:  Additional Page Property Modifications Using Dynamic Functions ##

```
(function() {

  tabsint.register('changeText', function(dm) {
    var response = dm.result.response;
    var newQuestionMainText;
    var newChoices;

    if (response === 'A'){
      newQuestionMainText = 'Do you like baseball?';
      newChoices = [{id:'A',text:'Yes I like Baseball'},{id:'B',text:'No, I do not like Baseball'}]
    } else if (response === 'B'){
      newQuestionMainText = 'Do you like cars?';
      newChoices = [{id:'A',text:'Yes I like Mustangs'},{id:'B',text:'No, I ride my bike'}]
    }

    return {
      questionMainText: newQuestionMainText,
      responseArea: {
        choices: newChoices}
    };
  });

  tabsint.register('addFollowOn', function(dm) {
    // add a followOn and conditional flag

    var newSetFlags = [
      {
        id:'DO_FOLLOW_ON',
        conditional:"result.response === 'y'"
      }
    ];

    var newFollowOns = [
      {
        conditional:'flags.DO_FOLLOW_ON',
        target:{
          id:'ynFollowOn',
          questionMainText:'Are you enjoying this follow-on?',
          wavfiles:[
            {
              path:'chirpFullScaleWRTRef.wav',
              targetSPL:'80'
            }
          ],
          responseArea:{
            type:'yesNoResponseArea'
          }
        }
      }
    ];

    return {
      followOns: newFollowOns,
      setFlags: newSetFlags
    };
  });

})();
```
