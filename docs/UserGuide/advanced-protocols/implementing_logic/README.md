# Implementing Logic #

TabSINT has many features to support developing dynamic, adaptive questionnaires.
There following section describes the many ways to implement dynamic logic and content.

## Custom Expressions ##

Custom expressions are used to define logic that can be used to create adaptive protocols.

The following section will describe how to define custom expressions.  The sections after will describe how to use these expressions to define page logic.

<div class="admonition warning">
<p class="first admonition-title">Warning</p>
<p class="last">Custom expressions are difficult to use and debug, please use with caution and only when necessary.</p>
</div>

### Syntax ###

Custom expressions are written using a safe subset of the Javascript programming language. 
The vast majority of Javascript expressions are legal in TabSINT.  

Specifically, TabSINT uses AngularJS's ```$eval(...)``` function to evaluate expressions because it is relatively safe, secure, and flexible.

For more information, see:

- A brief guide to [Javascript Syntax](http://www.w3schools.com/js/js_syntax.asp)

- A [Javascript Tutorial](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript)

- Specific details regarding [AngularJS's expression syntax](https://docs.angularjs.org/guide/expression)
  

### Namespace ###

In custom expressions, you can reference the following functions and variables:

**Libraries and Convenience Functions**:

- Most *native Javascript* functions (see [AngularJS's $eval](https://docs.angularjs.org/guide/expression) for specifics).
- The javascript Math library: standard math functions including:
  
  - ```Math.abs``` for absolute value
  - ```Math.min``` for minimum of two numbers
  - ```Math.max``` for maximum of two numbers
    

- The [Lodash Javascript Library](https://lodash.com): Provides numerous useful convenience functions and
  functional programming tools, such as:
  
  - ```filter``` to restrict items in a list based on some criteria
  - ```countBy``` to count items based on some criteria
  - ```shuffle``` to randomize a list
  - ```map```, ```reduce```, ```collect```, and numerous other important functional programming tools
    

- Custom TabSINT Functions, written solely to make these custom expressions
  shorter and more semantic:
  
  - ```arrayContains(strArray, item)```: Converts a JSON string array (such as that stored
    by a checkBoxResponseArea) to a Javascript array, and then checks whether ```item``` is in it.
    

**Exam Results and Exam State**

- ```flags```: A javascript object containing a copy of all set flags. Refer to it using dot notation,
  e.g., ```flags.q1Answered``` for a flag named *q1Answered*.
- ```result```: A copy of the response fom the previous (most recent) question.
- ```examResults```: A copy of the exam's entire results structure, similar to that downloaded for
  post-analysis. It contains the following fields:
  
  - ```protocolName```: Name of the active protocol
  - ```qrString```: QR Code, if the protocol includes a ```qrResponseArea```
  - ```testDateTime```: For example, '2014-07-07T15:55:30.942Z'
  - ```testResults```: An array of test results objects
  - ```subjectId```: Subject id for the current exam, if a prior subject ID response area is present
    
### Examples ###

|                                      Meaning                                     | Code                                                                                        |
|:--------------------------------------------------------------------------------:|---------------------------------------------------------------------------------------------|
|                       If the previous question was correct                       |                                     ```result.correct```                                    |
|                       If the previous result was incorrect                       |                                    ```!result.correct```                                    |
|                        If the previous response was "dog"                        |                               ```result.response === 'dog'```                               |
| If the previous response was 34 and the ```usesHearingProtection``` flag is set. |               ```(result.response === '34') && flags.usesHearingProtection```               |
|                   If the subject chose both earPlugs and other:                  | ```arrayContains(result.response, 'earPlugs') && arrayContains(result.response, 'other')``` |

## Flags ##

Flags can be set at the end of each page based on the user response.
The flag can then be used in a custom expression to control the logic flow of the test at any later point in time.
```
"pages":[
  {
    "id":"question1",
    "questionMainText":"How many years of service do you have?",
    "responseArea":{
      "type":"integerResponseArea"
    },
    "setFlags":[
      {
        "id": "integerFlag"
        "conditional":"result.response>5"
      }
    ]
  }
]
```

## Repeats ##

Repeats allow you to show the same question again, based on a conditional custom expression.

```
"pages":[
  ...
  {
    "repeatPage":{
      "nRepeats":2,
      "repeatIf":"result.response !== 'B'"
    }
  }
]
```

Note the question will be repeated up to 2 times (max 3 repeates) if the participant continues to answer *A*.

If ``repeatIf`` is undefined, the page will repeat ``nRepeats`` times.

See [Repeats Example](../example_protocols/repeats/) for an example protocol.

## Follow Ons ##

FollowOns are useful when one or more immediate followup questions should be asked if the response to the current question meets some criteria.

```
{
  "id":"question1",
  "questionMainText":"How many years of service do you have?",
  "responseArea":{
    "type":"integerResponseArea"
  },
  "followOns":[
    {
       "conditional":"result.response > 5",
       "target":{
          "id":"question2",
          "questionMainText":"How many times have you been deployed to Iraq or Afghanistan?",
          "responseArea":{
             "type":"integerResponseArea"
          }
       }
    }
  ]
}
```

The second question will only be asked if the subject answers that they have more than 5 years of service.

The conditional is implemented as a custom expression and must return a boolean (```true``` or ```false```). If the conditional returns ```true```, the target is executed. If the conditional returns ```false```, the target is ignored.

Multiple sets of conditionals and targets can be included in a single instance of ```followOns```.  
Each target can be defined as a single page or a reference to a subprotocol.

## Skip If ##

Pages can be skipped using the ```skipIf``` object. This object is evaluated prior to the page being rendered.

The custom expression used in this object can leverage the result of the previous question or any previously set flags.

```
"pages":[
  {
    "id":"question1",
    "questionMainText":"How many years of service do you have?",
    "responseArea":{
      "type":"integerResponseArea"
    }
  },
  {
    "id":"question2",
    "questionMainText":"This will be skipped if the previous response > 5",
    "responseArea":{
      "type":"integerResponseArea"
    },
    "skipIf": "result.response > 5"
  }
]
```

## Feedback ##

Feedback options allow you to provide feedback on certain questions after the user submits an answer.  The ```feedback``` field has two possible values:

- ```gradeResponse```: will mark answers correct (red) and incorrect (green).
- ```showCorrect```: will mark answers correct/incorrect AND reveal correct answers the user missed.
  
```
"responseArea":{
  ...
  "feedback": "gradeResponse"
}
```

<div style="text-align:center" height="450px"><img src="feedback_types.png"/></div>

See [Feedback Example](../example_protocols/feedback/) for an example protocol with feedback.

## Special References ##

### @PARTIAL ###

If an exam is terminated using the *End Exam and Submit Partial Results* link, the protocol can specify a final subprotocol to run before the test ends. This subprotocol must have the special reference ID ```@PARTIAL```.

```
"subProtocols":[
  {
    "protocolId": "@PARTIAL",
    "title":"Final Section",
    "pages":[
      ...
    ]
  }
]
```

Potential use cases include:

1. Display a page or sequence of pages asking for feedback on why the exam is being ended prematurely.
2. Collect required information that would otherwise be collected at the end of the exam.
   

### @END_ALL ###

The special reference ```@END_ALL``` will automatically end the test no matter where the user is in the protocols stack. This can be used to manually end a protocol early in a FollowOn, subprotocol, or other special circumstance.

```
{
  "id":"question1",
  "questionMainText":"How many years of service do you have?",
  "responseArea":{
    "type":"integerResponseArea"
  },
  "followOns":[
    {
      "conditional":"result.response > 5",
      "target":{
         "reference": "@END_ALL"
       }
    }
  ]
}
```


## Examples ##

### Kitchen Sink ###

This example shows many of the available features for implementing dynamic questionnaires.

```
{
   "title":"A Brief Example of Questionnaire Attributes",
   "pages":[
      {
         "id":"question1",
         "questionMainText":"What is your age?",
         "responseArea":{
            "type":"integerResponseArea"
         },
         "followOns":[
            {
               "conditional":"result.response>=21",
               "target":{
                  "id":"question1a",
                  "questionMainText":"Do you like to have beer or wine with your evening meal?",
                  "responseArea":{
                     "type":"yesNoResponseArea"
                  }
               }
            }
         ]
      },
      {
         "id":"question2",
         "questionMainText":"What is your favorite color?",
         "responseArea":{
            "type":"multipleChoiceResponseArea",
            "choices":[
               {
                  "id":"Red"
               },
               {
                  "id":"Green"
               },
               {
                  "id":"Blue"
               }
            ]
         },
         "setFlags":[
            {
               "conditional":"result.response=='Red'",
               "id":"likesRed"
            }
         ]
      },
      {
         "id":"question3",
         "questionMainText":"How frequently do you read for pleasure?",
         "responseArea":{
            "type":"likertResponseArea",
            "levels":5,
            "specifiers":[
               {
                  "level":0,
                  "label":"Never"
               },
               {
                  "level":2,
                  "label":"Occassionally"
               },
               {
                  "level":4,
                  "label":"Every day"
               }
            ]
         },
         "followOns":[
            {
               "conditional":"result.response>=2",
               "target":{
                  "reference":"reader"
               }
            }
         ]
      },
      {
         "id":"question4",
         "skipIf":"flags.likesRed",
         "questionMainText":"Do you dislike the color red?",
         "responseArea":{
            "type":"yesNoResponseArea"
         }
      }
   ],
   "subProtocols":[
      {
         "protocolId":"reader",
         "title":"Reader Survey",
         "pages":[
            {
               "id":"questionR1",
               "questionMainText":"What type of reading do you enjoy?",
               "responseArea":{
                  "type":"checkboxResponseArea",
                  "choices":[
                     {
                        "id":"Novels"
                     },
                     {
                        "id":"Biography"
                     },
                     {
                        "id":"Nonfiction"
                     },
                     {
                        "id":"News"
                     }
                  ],
                "other":"Other"
               }
            }
         ]
      }
   ]
}
```

### Running Subprotocols ###

This example shows how to randomly run one out of several available subprotocols. 

```
{
  "title":"Example: Running One of Several Subprotocols",
  "randomization":"WithoutReplacement",
  "timeout":{
    "nMaxPages":1
  },
  "pages":[
    {
      "reference":"sub1"
    },
    {
      "reference":"sub2"
    }
  ],
  "subProtocols":[
    {
      "protocolId":"sub1",
      "title":"Subprotocol #1",
      "pages":[
        {
          "id": "info001",
          "questionMainText": "You are in subprotocol #1."
        },
        {
          "id": "info002",
          "questionMainText": "You are leaving subprotocol #1."
        }
      ]
    },
    {
      "protocolId":"sub2",
      "title":"Subprotocol #2",
      "pages":[
        {
          "id": "info001",
          "questionMainText": "You are in subprotocol #2."
        },
        {
          "id": "info002",
          "questionMainText": "You are leaving subprotocol #2."
        }
      ]
    }
  ]
}
```

## Other Tips and Tricks ##

There are full protocols distributed with this software which are good examples to work from when developing a new questionnaire. See the [Feature Demo](https://gitlab.com/creare-com/tabsint/tree/master/www/res/protocol/feature-demo/protocol.json) protocol available with the source code.

**Matlab Tools**

The jsonlab toolbox for Matlab can be downloaded [here](http://iso2mesh.sourceforge.net/cgi-bin/index.cgi?jsonlab).  

Matlab is useful when a large group of similar questions need to be included in a protocol.  Write the protocol with one question, load into Matlab, copy the relevant question, substitute appropriate values for the question and responses, and then write out as JSON.
