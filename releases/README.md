# TabSINT Releases


> Releases before TabSINT 2.0 are available for [TabSINT](https://gitlab.com/creare-com/tabsint/tags) and for [TabSINT with WAHTS](https://gitlab.com/creare-com/tabsint-cha/tags) separately on Gitlab. 
> Releases for TabSINT 2.0, which include support for the WAHTS, are listed below.


## v2.0.3

- Firmware: `2017-12-06_orca_3` 
- [Changelog](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)

[<img style="width:125px" src="download-for-android.png" />](https://gitlab.com/creare-com/tabsint/uploads/3efb928fb1cc8854fa571f0f8b9c89b7/tabsint.apk)


## v2.0.2

- Firmware: `2017-11-01_orca_1` 
- [Changelog](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)

[<img style="width:125px" src="download-for-android.png" />](https://gitlab.com/creare-com/tabsint/uploads/17836f62ac2168abf07849194ae4390d/tabsint.apk)


## v2.0.1

- Firmware: `2017-11-01_orca_1` 
- [Changelog](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)

[<img style="width:125px" src="download-for-android.png" />](https://gitlab.com/creare-com/tabsint/uploads/3691cc88fd17f0130e27a9ba58c2ae91/tabsint.apk)


## v2.0.0

- Firmware: `2017-11-01_orca_1` 
- [Changelog](https://gitlab.com/creare-com/tabsint/blob/master/CHANGELOG.md)

[<img style="width:125px" src="download-for-android.png" />](https://gitlab.com/creare-com/tabsint/uploads/d0505c222226f74af2467d51af288a1f/tabsint.apk)
