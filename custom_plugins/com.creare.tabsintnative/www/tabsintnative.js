/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */
var TabSINTNative = {
  initialize : function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "initialize", []);
  },
  resetAudio : function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "resetAudio", []);
  },
  getAudioVolume : function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "getAudioVolume", []);
  },
  sendExternalData: function(successCallback, errorCallback, externalAppName, data) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "sendExternalData", [externalAppName, data]);
  },
  getExternalDataIntent : function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "getExternalDataIntent");
  },
  setExternalDataIntentHandler : function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "TabSINTNative", "setExternalDataIntentHandler");
  }
}
module.exports = TabSINTNative;
