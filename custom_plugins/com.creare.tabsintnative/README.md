TabSINT Native Plugin
==============

This plugin is a gateway to implement certain functionality that is not available via
Cordova's javascript interface. It can:

* Override (inactivate) the menu, back, and volume buttons.
* Turn on "immersive" mode to hide the status bar, etc.

