package com.creare.tabsintnative;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import android.app.Activity;
//import android.content.Intent;
//import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.webkit.WebView;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

// for Immersive view...
import android.view.View;
import android.view.WindowManager;

// for Volume buttons
import android.media.AudioManager;

// for intent send data to another app
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;

import android.provider.MediaStore;
import android.database.Cursor;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.net.Uri;

import android.content.ContentResolver;
import android.webkit.MimeTypeMap;
import org.xwalk.core.XWalkPreferences;

/**
 * This calls out to the ZXing barcode reader and returns the result.
 *
 * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
 */
public class TabSINTNative extends CordovaPlugin {
  public static final String ACTION_INITIALIZE = "initialize";
  public static final String ACTION_RESET_AUDIO = "resetAudio";
  public static final String ACTION_GET_AUDIO_VOLUME = "getAudioVolume";
  public static final String ACTION_SEND_EXTERNAL_DATA = "sendExternalData";
  public static final String ACTION_GET_EXTERNAL_DATA_INTENT = "getExternalDataIntent";
  public static final String ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER = "setExternalDataIntentHandler";

  private static final String TAG = "ReceiverPlugin";

  private CallbackContext onNewIntentCallbackContext = null;
  private CallbackContext volumeCallbackContext = null;

  /**
   * Constructor.
   */
  public TabSINTNative() {
  }


  /**
   * Sets the context of the Command. This can then be used to do things like
   * get file paths associated with the Activity.
   *
   * @param cordova The context of the main Activity.
   * @param webView The CordovaWebView Cordova is running in.
   */
  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    // Always enable webview debugging
    XWalkPreferences.setValue(XWalkPreferences.REMOTE_DEBUGGING, true);
  }



  /**
   * Executes the request.
   *
   * This method is called from the WebView thread. To do a non-trivial amount of work, use:
   *     cordova.getThreadPool().execute(runnable);
   *
   * To run on the UI thread, use:
   *     cordova.getActivity().runOnUiThread(runnable);
   *
   * @param action          The action to execute.
   * @param args            The exec() arguments.
   * @param callbackContext The callback context used when calling back into JavaScript.
   * @return                Whether the action was valid.
   *
   * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
   */
  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

    if (action.equals(ACTION_INITIALIZE)) {
      //cordova.getActivity().setVolumeControlStream(AudioManager.STREAM_SYSTEM); // doesn't work for some reason...

      resetAudio();
      if (getAudioVolume() < 100) {
        JSONObject r = new JSONObject();
        r.put("error",false);
        r.put("code", 99);
        r.put("volume", getAudioVolume());
        r.put("message", "Volume not properly set.");
        callbackContext.error(r);
        return false;
      } else {
        callbackContext.success();
      }
      return true;
    } else if (action.equals(ACTION_RESET_AUDIO)) {
      resetAudio();
      if (getAudioVolume() < 100) {
        JSONObject r = new JSONObject();
        r.put("error",false);
        r.put("code", 99);
        r.put("volume", getAudioVolume());
        r.put("message", "Volume not properly set.");
        callbackContext.error(r);
        return false;
      } else {
        callbackContext.success();
      }
      return true;
    } else if (action.equals(ACTION_GET_AUDIO_VOLUME)) {
      JSONObject r = new JSONObject();
      r.put("success",true);
      r.put("volume", getAudioVolume());
      callbackContext.success(r);
      return true;
    } else if (action.equals(ACTION_SEND_EXTERNAL_DATA)) {
      return sendExternalData(args.getString(0), args.getString(1), callbackContext);
    } else if (action.equals(ACTION_GET_EXTERNAL_DATA_INTENT)) {
      return getExternalDataIntent(callbackContext);
    } else if (action.equals(ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER)) {
      return setExternalDataIntentHandler(callbackContext);
    } else {
      callbackContext.error("Invalid action");
      return false;
    }
  }


  public void resetAudio() {
    AudioManager audioManager = (AudioManager)this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
  }


  public int getAudioVolume() {
    AudioManager audioManager = (AudioManager)this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
    int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    int currSystemVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    int volLevel = Math.round((float) (currSystemVol * 100.0 / (float) maxVolume));
    return volLevel;
  }


  /**
   * Send data to another app
   * This method requires another installed app with appropriate intent filters.
   *
   * @param appName         string.  ex:  com.creare.receiver
   * @param data            string.  data = JSON.stringify(dataObject)
   * @param callbackContext The callback context used when calling back into JavaScript.
   * @return                Boolean regarding success
   */
  public boolean sendExternalData(final String appName, final String data, final CallbackContext callbackContext){
    if (data != null && (data instanceof String)) {
      if (appName != null && (appName instanceof String)) {
        Context activityContext = this.cordova.getActivity().getApplicationContext();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setPackage(appName);
        sendIntent.putExtra("TABSINT_DATA_JSON_STRING", data);
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activityContext.startActivity(sendIntent);
        try {
          JSONObject r = new JSONObject();
          r.put("success",true);
          r.put("message", "Receiver.sendDataExternal");
          r.put("data", data);
          callbackContext.success(r);
        } catch (JSONException e) {

        }
        return true;
      } else {
        try {
          JSONObject r = new JSONObject();
          r.put("success",false);
          r.put("message", "Could not send data to another app - no appName specified.  Name format is 'com.myorg.myapp'.");
          callbackContext.error(r);
        } catch (JSONException e) {

        }
        return false;
      }
    } else {
      try {
        JSONObject r = new JSONObject();
        r.put("success",false);
        r.put("message", "Could not send data to another app - Data must be a string.  If data is any other type, use JSON.stringify().");
        callbackContext.error(r);
      } catch (JSONException e) {

      }
      return false;
    }
  }


  /**
   * Send a JSON representation of the cordova intent back to the caller
   *
   * @param callbackContext
   */
  public boolean getExternalDataIntent (final CallbackContext callbackContext) {
      Intent intent = cordova.getActivity().getIntent();
      String action = intent.getAction();
      String type = intent.getType();
      if (Intent.ACTION_SEND.equals(action) && type != null) {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getIntentJson(intent)));
        return true;
      } else {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT));
        return true;
      }

  }

  /**
   * Register handler for onNewIntent event
   *
   * @param data
   * @param callbackContext
   * @return
   */
  public boolean setExternalDataIntentHandler (final CallbackContext callbackContext) {
      this.onNewIntentCallbackContext = callbackContext;

      PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
      result.setKeepCallback(true);
      callbackContext.sendPluginResult(result);

      return true;
  }

  /**
   * Triggered on new intent
   *
   * @param intent
   */
  @Override
  public void onNewIntent(Intent intent) {
      PluginResult pluginResult;
      String action = intent.getAction();
      String type = intent.getType();
      if (this.onNewIntentCallbackContext != null) {
          if (Intent.ACTION_SEND.equals(action) && type != null) {
            pluginResult = new PluginResult(PluginResult.Status.OK, getIntentJson(intent));
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          } else {
            pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          }
      }
  }

  /**
   * Return JSON representation of intent attributes
   *
   * @param intent
   * @return
   */
  private JSONObject getIntentJson(Intent intent) {
      JSONObject intentJSON = null;

      try {
          intentJSON = new JSONObject();

          intentJSON.put("type", intent.getType());
          intentJSON.put("data", intent.getStringExtra("TABSINT_DATA_JSON_STRING"));

          return intentJSON;
      } catch (JSONException e) {
          Log.d(TAG, TAG + " Error thrown during intent > JSON conversion");
          Log.d(TAG, e.getMessage());
          Log.d(TAG, Arrays.toString(e.getStackTrace()));

          return null;
      }
  }

}
