# Contributing 
 
Thank you for considering contributing to the TabSINT project. 

Please read the contributing guidelines below.

### Reporting an Issue

If you have a question on how something works, or its expected functionality, please first read the [Developer Guide](https://creare-com.gitlab.io/tabsint/docs/DeveloperGuide/) and the [User Guide](https://creare-com.gitlab.io/tabsint/docs/UserGuide/)

If you think you have found a bug, please start by making sure it hasn't already been reported in the [TabSINT Issue Tracker](https://gitlab.com/creare-com/tabsint/issues). You can search through existing issues to see if a similar bug report has already been filed.

If not, create a new issue, with the `bug` label, that briefly explains the problem, and provides a bit of background as to the circumstances that triggered it. Please do not group multiple topics into one issue, but instead each should be its own issue.

### Making a Feature Request

If you have a new feature idea, please start by making sure it hasn't already been requested on the [TabSINT Issue Tracker](https://gitlab.com/creare-com/tabsint/issues). You can search through existing feature requests to see if a similar feature has already been requested.

If not, create a new issue, with the `feature request` label, that describes the feature. Please do not group multiple topics into one request, but instead each should be its own issue.

### Issue Etiquette Guidelines

Be respectful and follow the guidelines here as well as the general guidelines for open-source projects.

### Pull Request Guidelines

If you have forked the TabSINT repository and implemented changes that you would like to see incorporated, you can create a merge request.  When in doubt, keep your requests small - do not bundle more than one *feature* or *bug fix* per request. Doing so makes it very hard to accept it if one of the fixes has issues.

It's always better to create two smaller merge requests than one big one.

### Style

Always use two spaces, no tabs. This goes for any HTML, CSS, or Javascript.

### License

By contributing your code to the `creare-com/tabsint` Repository, you agree to license your contribution under the [Apache2 License](LICENSE).
