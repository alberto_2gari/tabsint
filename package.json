{
  "name": "tabsint",
  "version": "2.0.3",
  "repository": {
    "type": "git",
    "url": "git+https://gitlab.com/creare-com/tabsint.git"
  },
  "description": "Open source platform for administering tablet based hearing-related exams, as well as general-purpose questionnaires",
  "bugs": {
    "url": "https://gitlab.com/creare-com/tabsint/issues"
  },
  "author": "Creare",
  "homepage": "http://creare-com.gitlab.io/tabsint",
  "license": "Apache-2.0",
  "dependencies": {
    "com.creare.notifications": "./custom_plugins/com.creare.notifications",
    "com.creare.tabsintnative": "./custom_plugins/com.creare.tabsintnative",
    "cordova-android": "6.3.0",
    "cordova-ios": "4.5.1",
    "cordova-plugin-bluetooth-status": "1.0.4",
    "cordova-plugin-crosswalk-webview": "2.3.0",
    "cordova-plugin-device": "1.1.1",
    "cordova-plugin-file": "4.3.3",
    "cordova-plugin-file-transfer": "1.6.3",
    "cordova-plugin-fullscreen": "1.1.0",
    "cordova-plugin-geolocation": "2.1.0",
    "cordova-plugin-inappbrowser": "1.2.1",
    "cordova-plugin-insomnia": "4.2.0",
    "cordova-plugin-media": "2.1.0",
    "cordova-plugin-network-information": "1.2.0",
    "cordova-plugin-splashscreen": "3.1.0",
    "cordova-plugin-whitelist": "1.2.1",
    "cordova-plugin-zip": "3.1.0",
    "cordova-sqlite-storage": "1.4.1",
    "ionic-plugin-keyboard": "2.2.1",
    "phonegap-plugin-barcodescanner": "4.1.0"
  },
  "devDependencies": {
    "angular-gettext": "2.3.10",
    "angular-gettext-cli": "1.2.0",
    "bower": "1.8.0",
    "concurrently": "3.1.0",
    "cordova": "7.1.0",
    "dir-compare": "1.3.0",
    "fs-extra": "3.0.1",
    "gitbook-cli": "2.3.2",
    "gitbook-plugin-collapsible-chapters": "0.1.8",
    "gitbook-plugin-downloadpdf": "1.0.3",
    "gitbook-plugin-heading-anchors": "1.0.3",
    "gitbook-plugin-page-toc": "1.0.2",
    "istanbul": "0.4.4",
    "jasmine-core": "2.5.2",
    "jshint": "2.9.4",
    "jshint-stylish": "2.2.1",
    "json": "9.0.6",
    "karma": "1.7.1",
    "karma-chrome-launcher": "2.2.0",
    "karma-coverage": "1.1.1",
    "karma-es6-shim": "1.0.0",
    "karma-jasmine": "1.1.0",
    "karma-mocha-reporter": "2.2.1",
    "karma-ng-html2js-preprocessor": "1.0.0",
    "karma-requirejs": "1.1.0",
    "karma-spec-reporter": "0.0.26",
    "live-server": "1.2.0",
    "lodash": "4.17.4",
    "onchange": "3.2.1",
    "protractor": "4.0.14",
    "replace": "0.3.0",
    "requirejs": "2.3.2",
    "script-help": "0.1.0",
    "shx": "0.2.2",
    "tv4": "1.2.7",
    "xml2js": "0.4.17"
  },
  "engines": {
    "npm": ">=3.10.0",
    "node": ">=6.9.0"
  },
  "scripts": {
    "help": "script-help",
    "postinstall": "npm run bower.install && npm run docs.install",
    "set-config": "node bin/node/set-config.js",
    "postset-config": "npm run config",
    "config": "node bin/node/config-rev.js && npm run show-build",
    "show-config": "node bin/node/show-config.js",
    "show-build": "node bin/node/show-build.js",
    "config.name": "json -f www/tabsintConfig.json filename",
    "validate": "node bin/node/validate.js",
    "write-version": "node bin/node/write-version.js",
    "extract-translations": "angular-gettext-cli --files \"www/+(scripts|tabsint_plugins)/**/*.+(js|html)\" --exclude \"**/*.spec.js\" --dest \"translations/extract.pot\" --marker-name i18n",
    "compile-translations": "angular-gettext-cli --compile --files \"translations/*.po\" --dest \"www/res/translations/translations.json\" --format json",
    "build": "npm run config && npm run write-version && npm run tabsint-plugins.app && npm run lint",
    "build.release": "npm run build && npm run test && npm run docs",
    "clean": "npm run clean.dist && npm run clean.docs && npm run clean.cordova && npm run clean.app",
    "clean.app": "shx rm -rf ./www/tabsint_plugins && shx rm -f ./www/version.json",
    "clean.cordova": "shx rm -rf ./plugins && shx rm -rf ./platforms",
    "clean.docs": "shx rm -rf ./docs/build",
    "clean.dist": "shx rm -rf ./dist",
    "bower": "bower",
    "bower.install": "bower install",
    "lint": "npm run lint.tabsint && npm run lint.tabsint-plugins",
    "lint.tabsint": "jshint --reporter=node_modules/jshint-stylish --exclude-path .gitignore ./www/scripts",
    "lint.tabsint-plugins": "jshint --reporter=node_modules/jshint-stylish ./www/tabsint_plugins",
    "test": "npm run lint && npm run test.nolint",
    "test.nolint": "karma start test/karma.conf.js",
    "test.tabsint-plugins": "npm run tabsint-plugins.app && npm run test",
    "test.verbose": "npm run test.nolint -- --log-level debug --client.captureConsole",
    "test.debug": "npm run test.verbose -- --single-run=false --debug",
    "plugins": "npm run plugins.cordova && npm run tabsint-plugins",
    "plugins.cordova": "node bin/node/plugins-cordova.js",
    "tabsint-plugins": "npm run tabsint-plugins.app && npm run tabsint-plugins.docs",
    "tabsint-plugins.app": "node bin/node/plugins-app.js",
    "tabsint-plugins.app.dev": "node bin/node/plugins-app-dev.js",
    "tabsint-plugins.app.dev.watch": "onchange \"tabsint_plugins\" --wait --kill-others -- npm run tabsint-plugins.app.dev",
    "tabsint-plugins.docs": "node bin/node/plugins-docs.js",
    "tabsint-plugins.checkout": "node bin/node/plugins-checkout.js",
    "tabsint-plugins.debug": "node bin/node/plugins-debug.js",
    "docs": "npm run clean.docs && npm run docs.build && npm run docs.pdf",
    "docs.install": "gitbook fetch latest && gitbook install",
    "docs.build": "gitbook build . docs/build && shx cp -r www docs/build/docs/demo/www",
    "docs.serve": "shx rm -rf _book && gitbook serve",
    "docs.pdf": "(which calibre && gitbook pdf . docs/build/tabsint.pdf) || shx echo \"\nFailed to create documentation pdf. \nConfirm you have calibre (https://calibre-ebook.com) installed on your system path.\n\" ",
    "preupdate-webdriver": "npm install",
    "update-webdriver": "webdriver-manager update",
    "preprotractor": "npm run update-webdriver",
    "protractor": "protractor test/e2e/protractor.conf.js",
    "live-server": "live-server --open=\"www\" --watch=\"www\" --ignore=\"www/res, *.json, *.css\" ",
    "serve": "concurrently \"npm run tabsint-plugins.app.dev.watch\" \"npm run live-server\" --kill-others ",
    "preserve": "npm run build",
    "cordova": "cordova",
    "cordova.reset.ios": "npm run clean.cordova && cordova platform add ios && npm run plugins.cordova",
    "cordova.prepare.ios": "cordova prepare ios && shx cp config/ios/TabSINT-Info.plist platforms/ios/tabsint/TabSINT-Info.plist && npm run plugins.cordova",
    "cordova.build.ios": "npm run build && npm run cordova.prepare.ios",
    "cordova.run.ios": "npm run build && npm run cordova.prepare.ios",
    "run.ios": "npm run cordova.run.ios",
    "runx.ios": "npm run lint && npm run cordova.run.ios",
    "cordova.reset.android": "npm run clean.cordova && cordova platform add android && npm run plugins.cordova",
    "cordova.prepare.android": "cordova prepare android && shx cp bin/util/build-extras.gradle platforms/android/build-extras.gradle && npm run plugins.cordova",
    "cordova.build.android": "npm run build && npm run cordova.prepare.android && cordova build android",
    "cordova.run.android": "npm run build && npm run cordova.prepare.android && cordova run android",
    "run.android": "npm run cordova.run.android",
    "runx.android": "npm run lint && cordova run android",
    "dist.dir": "shx mkdir -p dist",
    "dist.config": "npm run dist.dir && shx cp www/tabsintConfig.json dist/config.json",
    "dist.docs": "npm run dist.dir && shx cp -r docs/build dist/docs",
    "dist.android": "npm run clean.dist && npm run dist.dir && npm run dist.docs && npm run dist.config && shx cp platforms/android/build/outputs/apk/android-release.apk dist/tabsint.apk",
    "dist.ios": "shx echo todo",
    "release.ios": "npm run clean && npm run build.release && npm run cordova.prepare.ios && cordova build ios --release --buildConfig config/tabsint-release.json && npm run dist.ios",
    "release.android": "npm run clean && npm run build.release && npm run cordova.prepare.android && cordova build android --release --buildConfig config/tabsint-release.json && npm run dist.android"
  },
  "scriptHelp": {
    "set-config": {
      "name": "set-config",
      "description": "Set a build configuration file",
      "usage": [
        "npm run set-config [config-file] [--tabsintadmin]"
      ],
      "examples": [
        {
          "example": "npm run set-config config/example_config.json",
          "note": ""
        }
      ]
    },
    "show-config": {
      "name": "show-config",
      "description": "Show the current set build-configuration file",
      "examples": [
        {
          "example": "npm run show-config",
          "note": ""
        }
      ]
    },
    "validate": {
      "name": "validate",
      "description": "Validate a protocol against the TabSINT protocol schema",
      "usage": [
        "npm run validate [path-to-protocol]"
      ],
      "examples": [
        {
          "example": "npm run validate protocols/demo",
          "note": ""
        }
      ]
    }
  },
  "cordova": {
    "platforms": [
      "android",
      "ios"
    ],
    "plugins": {
      "ionic-plugin-keyboard": {},
      "cordova-plugin-file-transfer": {},
      "cordova-plugin-zip": {},
      "cordova-plugin-geolocation": {},
      "cordova-plugin-media": {},
      "cordova-plugin-splashscreen": {},
      "cordova-plugin-inappbrowser": {},
      "cordova-plugin-network-information": {},
      "cordova-plugin-file": {},
      "cordova-plugin-device": {},
      "cordova-plugin-whitelist": {},
      "cordova-plugin-bluetooth-status": {},
      "cordova-plugin-insomnia": {},
      "cordova-plugin-fullscreen": {},
      "phonegap-plugin-barcodescanner": {},
      "cordova-sqlite-storage": {},
      "com.creare.tabsintnative": {},
      "com.creare.notifications": {},
      "cordova-plugin-crosswalk-webview": {
        "XWALK_VERSION": "23+",
        "XWALK_COMMANDLINE": "--disable-pull-to-refresh-effect",
        "XWALK_MODE": "embedded",
        "XWALK_MULTIPLEAPK": "false"
      }
    }
  }
}