// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function (config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // Start these browsers, currently available:
    browsers: ['ChromeHeadlessCI'],

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine', 'requirejs', 'es6-shim'],
    reporters: ['mocha', 'coverage'],
    mochaReporter: {
      output: 'minimal',
      ignoreSkipped: true
    },

    // Chrome custom flags
    customLaunchers: {
      ChromeHeadlessCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },

    client: { captureConsole: false },   // True: Show console.log messages, False: suppress console.log messages
    logLevel: config.LOG_ERROR,  // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG

    // timeout - sometimes this kills the tests
    browserNoActivityTimeout: 100000,

    // list of files / patterns to load in the browser
    files: [
      {pattern: 'www/bower_components/angular/angular.min.js', included: false},
      {pattern: 'www/bower_components/angular-gettext/dist/angular-gettext.min.js', included: false},
      {pattern: 'www/bower_components/angular-mocks/angular-mocks.js', included: false},
      {pattern: 'www/bower_components/d3/d3.min.js', included: false},
      {pattern: 'www/bower_components/ngstorage/ngStorage.min.js', included: false},
      {pattern: 'www/bower_components/angular-sanitize/angular-sanitize.min.js', included: false},
      {pattern: 'www/bower_components/lodash/dist/lodash.min.js', included: false},
      {pattern: 'www/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js', included: false},
      {pattern: 'www/bower_components/tv4/tv4.js', included: false},
      {pattern: 'www/bower_components/jquery/dist/jquery.min.js', included: false},
      {pattern: 'www/bower_components/ngCordova/dist/ng-cordova.min.js', included: false},
      {pattern: 'www/bower_components/es6-shim/es6-shim.min.js', included: false},
      {pattern: 'www/bower_components/json-formatter/dist/json-formatter.min.js', included: false},
      {pattern: 'www/scripts/*.js', included: false},
      {pattern: 'www/scripts/**/*.js', included: false},
      {pattern: 'www/tabsint_plugins/*.js', included: false},
      {pattern: 'www/tabsint_plugins/**/*.js', included: false},
      {pattern: 'www/tabsint_plugins/**/*.json', included: false},
      {pattern: 'www/res/protocol/**/*.js', included: false},
      {pattern: 'www/*.json', included: false},
      {pattern: 'www/res/protocol/**/*.json',  included: false},
      {pattern: 'www/res/protocol/*.json',  included: false},
      {pattern: 'config/*.json', included: false},
      //{pattern: 'www/res/protocol/custom_response_areas/*.js', included: false},

      {pattern: 'test/**/*.json', included: false},

      {pattern: 'www/require-config.js', included: true}
    ],

    // generate js files from html templates
    preprocessors: {
      'www/views/*.html': 'ng-html2js',
      'www/scripts/**/*.html': 'ng-html2js',
      'www/scripts/**/*.js': ['coverage']
    },

    ngHtml2JsPreprocessor: {
      // strip this from the file path
      stripPrefix: 'www/',

      // setting this option will create only a single module that contains templates
      // from all the files, so you can load them all with module('foo')
      moduleName: 'htmls'
    },


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true
  });
};
