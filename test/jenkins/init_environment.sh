#!/bin/bash -ex

# Git repo already is already checked out by Jenkins.
# Do a completely fresh start with only git checkout left behind:
git clean -df

# Make sure system conda/anaconda is up to date.
conda update conda --yes --quiet
conda update anaconda --yes --quiet

# Create our new conda environment, and ACTIVATE it.
#  Note that these packages are cached in system anaconda, so this actually 
#  creates minimal network traffic.
conda create -p conda-test-env pytest numpy pandas pip coverage nose ipython runipy seaborn jsonschema sphinx --yes --quiet
source activate ./conda-test-env

# install recommonmark
pip install recommonmark

# check global node dependencies
node --version
npm --version
bower --version

# configure git to use https
git config --global url."https://".insteadOf git://

# prune, then install node dependencies
npm prune
npm install
npm list

# install bower dependencies
bower prune
bower install
bower list

# set up tabsint plugins directory - note that we are skipping the install of cordova plugins
# cd bin
# python tabsint.py --tabsint-plugins

