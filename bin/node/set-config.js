#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var process = require('process');

if (require.main === module) {

  // take config file name from the command line and copy
  var filename = process.argv.slice(2)[0];
  config.copy(filename);
}
