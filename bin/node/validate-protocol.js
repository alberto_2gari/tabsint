#!/usr/bin/env node

'use strict';

var tv4 = require('tv4');  // tiny validator
var _ = require('lodash');
var fs = require('fs-extra');
var log = require('./util/log');


if (require.main === module) {

  // input protocol directory from command line
  var path = process.argv.slice(2)[0];
  var protocol = path.split('/').slice(-1)
  path = path + '/' + protocol + '.json';

  // try to load this file
  var p;
  p = JSON.parse(fs.readFileSync(path));

  // start validation process
  var errors = [];
  var msg;

  function addSchema(s) {

    // derive schema path, remove precedig 'definitions' if tv4 adds it
    var sPath = s.endsWith('.json') ? s : s + '.json';
    sPath = sPath.startsWith('definitions/definitions/') ? sPath.slice(12) : sPath;

    // load schema
    var schema = JSON.parse(fs.readFileSync('www/assets/protocol/schema/' + sPath));

    // add schema
    tv4.addSchema(s, schema);

    // find out dependent schemas
    var missing = tv4.getMissingUris();

    // handle missing schemas one by one
    if (missing.length > 0) {
      addSchema(missing[0]);
    }

    return schema;
  }

  // convienence function for interpreting nested tv4 errors
  function getErrors(err) {
    if (!err.subErrors) {
      errors.push({dataPath: err.dataPath, message: err.message});
    } else {
      _.forEach(err.subErrors, function(sub) {
        getErrors(sub);
      });
    }
  }

  // add root, recursively will add all dependenent schemas
  var protocolSchema = addSchema('schema');

  // validate
  var valid = tv4.validate(p, protocolSchema, false, true);


  // handle missing schemas
  if (tv4.missing.length > 0) {
    errors.push('The following schemas are missing from validation: ' + JSON.stringify(tv4.missing, null, 2));
    valid = false;
  }

  // collect actual errors
  if (tv4.error) {
    getErrors(tv4.error);
  }

  // handle invalid
  if (!valid) {
    log.error('Protocol schema validation failed with errors: \n' + JSON.stringify(errors, null, 2));
    process.exit(1);
  } else {
    log.info('Protocol successfully validated');
  }
}
