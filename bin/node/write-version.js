#!/usr/bin/env node

'use strict';

var config = require('./util/config.js');
var log = require('./util/log.js');
var cmd = require('./util/cmd.js');

var _ = require('lodash');
var fs = require('fs-extra');
var xml2js = require('xml2js');
var process = require('process');

if (require.main === module) {
  log.debug('Writing version.json');

  var c = config.read();
  var parser = new xml2js.Parser(); 
  var builder = new xml2js.Builder();

  // get list of cordova plugins form command line
  var cordova_plugins = cmd.run('npm run cordova -- plugins ls');
  if (cordova_plugins) {
    cordova_plugins = cordova_plugins.replace('\n',',').split(',');
    cordova_plugins = _.map(cordova_plugins, function(p){ return p.split('"')[0]});
  }

  // get versions from config.xml
  parser.parseString(fs.readFileSync('config.xml'), function (err, configxml) {
    if (err) {
      log.error('Error parsing config.xml', err);
    } else {
      afterParsingXml(configxml);
    }
  });

  function afterParsingXml(configxml) {

    // update config.xml if the version switched
    if (configxml['widget']['$']['version'] !== process.env.npm_package_version) {
      // Update android version code if the package_version is updated
      configxml['widget']['$']['android-versionCode'] = parseInt(configxml['widget']['$']['android-versionCode']) + 1

      // update the config.xml version with the npm package.json version
      configxml['widget']['$']['version'] = process.env.npm_package_version;

      log.debug('Updated config.xml to version ' + process.env.npm_package_version);
      fs.writeFileSync('config.xml', builder.buildObject(configxml));
    }

    // create the version.json object based on build info
    var version = {
      'tabsint':configxml['widget']['$']['version'],
      'date': (new Date()).toJSON(),
      'rev': c.rev,
      'version_code': configxml['widget']['$']['android-versionCode'],
      'deps': {
        'user_agent': process.env.npm_config_user_agent,
        'node': process.env.npm_config_node_version,
        'bower': process.env.npm_package_devDependencies_bower,
        'cordova': process.env.npm_package_devDependencies_cordova
      },
      'plugins': cordova_plugins
    }

    fs.writeFileSync('www/version.json', JSON.stringify(version, null, 2));
    log.debug('Wrote out version.json');
  }


}
