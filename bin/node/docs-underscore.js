var replace = require("replace");
var fs = require('fs-extra');
var path = require('path');
var process = require('process');

console.log('DOCS:  Removing underscores from top-level documentation directory');

if (require.main === module) {


  // Replace references to the directory/file with the underscore in it.
  function replaceFileReference(oldName, newName) {
    replace({
      regex: oldName,
      replacement: newName,
      paths: ['www/res/documentation'],
      // include: [],
      // exclude: [],
      recursive: true,
      silent: true,
    });
  }

  // List all files in a directory in Node.js recursively in a synchronous fashion
  // function walkSync(dir, filelist) {
  //     var path = path || require('path');
  //     var fs = fs || require('fs'),
  //         files = fs.readdirSync(dir);
  //     filelist = filelist || [];
  //     files.forEach(function(file) {
  //         if (fs.statSync(path.join(dir, file)).isDirectory()) {
  //             filelist = walkSync(path.join(dir, file), filelist);
  //         }
  //         else {
  //             filelist.push(path.join(dir, file));
  //         }
  //     });
  //     return filelist;
  // };
  //
  // var fileList = [];
  // walkSync('www/res/documentation', fileList)
  // fileList.forEach(function(file){
    // if (file.indexOf('_') > -1) {
    //   var newFile = file.replace('_','');
    //   removeUnderscore(file, newFile);
    //   console.log(file);
    // }
  // })


  // gets the top level.  This may be enough.  If not, see above commented code
  var dir = 'www/res/documentation';
  var files = fs.readdirSync(dir);

  files.forEach(function(file){
    if (file.indexOf('_') > -1) {
      console.log('DOCS:  Removing underscores from: ' + file);
      var newFile = file.replace('_','');

      // replace all references to this file
      replaceFileReference(file, newFile);
      
      // THEN rename the file.  async issues if other way around.
      fs.moveSync(path.join(dir, file), path.join(dir, newFile));
    }
  })

  console.log('DOCS:  Finished removing underscores!');
}