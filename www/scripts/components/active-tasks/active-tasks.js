/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.active-tasks', [])

    .directive('activeTasks', function () {
      return {
        templateUrl: 'scripts/components/active-tasks/active-tasks.html',
        controller: function($scope, tasks, disk, adminLogic) {
          $scope.tasks = tasks;
          $scope.disk = disk;
          $scope.adminLogic = adminLogic;
        }
      };
    });

});