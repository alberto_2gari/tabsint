define(['angular'], function (angular) {
  'use strict';

angular.module('tabsint.components.response-areas.multiple-choice', ['tabsint.components.response-areas.basic-response-areas'])

  .controller('MultipleChoiceResponseAreaCtrl', function ($scope, page, $location, $anchorScroll, $timeout) {
      $scope.page = page;
      $scope.enableOther = false;

      // set the button disbled for x msecs where x is specified by page.dm.delayEnable, HG 12/7/15
      $scope.buttonDisabled = true;
      $timeout(function(){
        $scope.buttonDisabled = false;
      }, page.dm.responseArea.delayEnable);
      // set the button disbled for x msecs where x is specified by page.dm.delayEnable, HG 12/7/15

      var yesNo = [
        {
          id: 'yes',
          text: 'Yes'
        },
        {
          id: 'no',
          text: 'No'
        }
      ];

      // to add feedback to the multichoice respAreas, HG 12/7/15
      if (page.dm.responseArea.feedback !== angular.undefined){
        page.dm.showFeedback = function(){
          if (page.dm.responseArea.feedback === 'gradeResponse'){
            $scope.gradeResponse = true;
          } else if (page.dm.responseArea.feedback === 'showCorrect') {
            $scope.showCorrect = true;
          }
        };
      }
      // to add feedback to the multichoice respAreas, HG 12/7/15


    function update() {
      $scope.choices = angular.copy(page.dm.responseArea.choices || yesNo);
      if (page.dm.responseArea.other) {
        $scope.enableOther = true;
        $scope.choices.push(
          {
            "id": "Other",
            "text": page.dm.responseArea.other
          });
      }
    }

    update();

    $scope.$watch('page.result.response', function () {
      if (page.dm.responseArea.other && ( page.result.response === 'Other' )) {
        $scope.enableOtherText = true;
        $location.hash('otherInput');
        $anchorScroll();
      } else {
        $scope.enableOtherText = false;
        page.result.otherResponse = undefined;
      }
    });

    // function to identify the location of the response word on the responseArea, HG 12/7/15
    $scope.multichosen = function (word) {
      return (page.result.response === word.id);
    };

  });

});
