define(['angular'], function (angular) {
  'use strict';

angular.module('tabsint.components.response-areas.button-grid', [])

    .controller('ButtonGridResponseAreaCtrl', function ($scope, page) {

      function update() {
        $scope.rows = page.dm.responseArea.rows;   // dump rows into its own variable
      }

        $scope.gradeResponse = false;
        $scope.showCorrect = false;

        // callback for page.dm
        if (page.dm.responseArea.feedback !== angular.undefined){
          page.dm.showFeedback = function(){
            if (page.dm.responseArea.feedback === 'gradeResponse'){
              $scope.gradeResponse = true;
            }
            else if (page.dm.responseArea.feedback === 'showCorrect') {
              $scope.showCorrect = true;
            }
          };
        }

      update();
    });
});
