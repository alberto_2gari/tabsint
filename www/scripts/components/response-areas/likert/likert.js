/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.likert', [])

    .controller('LikertResponseAreaCtrl', function ($scope, page) {
      $scope.page = page;
      $scope.verticalSpacing={'padding-bottom':$scope.page.dm.responseArea.verticalSpacing};

      function update () {
        var nLevels = page.dm.responseArea.levels;
        $scope.levels = _.map(_.range(nLevels), function(n) {return (n).toString();});
        $scope.topLabels = _.map(_.range(nLevels), function() {return '';});
        $scope.bottomLabels = _.map(_.range(nLevels), function() {return '';});



        if ( page.dm.responseArea.specifiers ) {
          _.forEach(page.dm.responseArea.specifiers, function (specifier) {
            if (specifier.position ==='below') {
              $scope.bottomLabels[specifier.level] = specifier.label;
            } else if ((specifier.position ==='above') || (specifier.position === undefined)) {
              $scope.topLabels[specifier.level] = specifier.label;
            }
          });
        }

        // Can we remove the 'odd' labels to make the layout more spacious?
        function isOdd(num) { return num % 2; }

        if (isOdd(nLevels)) {
          var allIdxs = _.range(nLevels);
          var oddIdxs = _.filter(allIdxs, function(num){ return isOdd(num); });
          var evenIdxs = _.filter(allIdxs, function(num){ return !isOdd(num); });

          if ( _.every(oddIdxs, function (idx) { return $scope.topLabels[idx] === ''; } ) ) {
            $scope.topLabelsAreSpacious = true;
            $scope.topLabels = _.map(evenIdxs, function (idx) { return $scope.topLabels[idx]; });
          }

          if ( _.every(oddIdxs, function (idx) { return $scope.bottomLabels[idx] === ''; } ) ) {
            $scope.bottomLabelsAreSpacious = true;
            $scope.bottomLabels = _.map(evenIdxs, function (idx) { return $scope.bottomLabels[idx]; });
          }
        }

      }
      update();

    })
    .controller('LikertChoiceController', function ($scope, examLogic, page) {
      update();

      function update() {
        $scope.chosen = function () {
          return ($scope.level === page.result.response);
        };

        var emoticons;
        if (page.dm.responseArea.useEmoticons) {
          emoticons = [
            {
              src: 'img/emoticons/strongly_disagree.png'
            },
            {
              src: 'img/emoticons/disagree.png'
            },
            {
              src: 'img/emoticons/no_opinion.png'
            },
            {
              src: 'img/emoticons/agree.png'
            },
            {
              src: 'img/emoticons/strongly_agree.png'
            }
          ];
        }

        $scope.btnSrc = function (ind) {
          var btnSrc;
          if (page.dm.responseArea.useEmoticons) {
            btnSrc = emoticons[ind].src;
          } else {
            if ($scope.chosen()) {
              btnSrc = 'img/radio_selected_64_64.png';
            } else {
              btnSrc = 'img/radio_unselected_64_64.png';
            }
          }

          return btnSrc;
        };

        $scope.choose = function () {
          // toggle chosen/unchosen.
          if (page.result.response === $scope.level) {
            page.result.response = undefined; //toggle off.
          } else {
            page.result.response = $scope.level; // choose.
          }

          // AUTO-SUBMIT:
          if (page.dm.responseArea.autoSubmit){ //to allow auto-submit, HG 07/12/16
             examLogic.act.submit();
          }
        };
      }
    });

});
