define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.multiple-input', [])

    .controller('MultipleInputResponseAreaCtrl', function ($scope, page) {
      $scope.page = page;
      $scope.list = page.dm.responseArea.inputList;
      var inputTypeForAll = page.dm.responseArea.inputTypeForAll;

      // defaults
      $scope.list.forEach(function(item){
        if (item.inputType === angular.undefined){
          item.inputType = inputTypeForAll || 'text';
        }

        item.rows = item.rows || 1;

        if (item.value === angular.undefined){
          item.value = undefined;
        }
        
      });

      // set response to values in the list
      $scope.page.result.response = _.map($scope.list, function(item){ return item.value; });

      // Dropdown logic
      $scope.selectResponse = function(itemIdx, option) {
        $scope.page.result.response[itemIdx] = option;
      };

      // submission logic
      $scope.page.dm.isSubmittable = function () {
        var ret = true;
        $scope.list.forEach(function(item, idx){
          if (item.required && isUndefined(item, $scope.page.result.response[idx])){
            ret = false;
          }
        });
        return ret;
      };

      // custom isUndefined function to handle text
      function isUndefined(item, val) {
        if (item.inputType === 'text' || item.inputType === 'number') {
          return val === '' || val === null || val === undefined;
        } else {
          return angular.isUndefined(val);
        }
      }



    });
});