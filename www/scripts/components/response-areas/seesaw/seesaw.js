define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.seesaw', [])

    .controller('SeeSawResponseAreaCtrl', function ($scope, page) {
      page.dm.isSubmittable = function(){
        return (!page.dm.responseArea.responseRequired ||
              (page.result.response !== ''));
      };

      page.result.response = ''; // RXC 12/2/2014 for angular1.3 compatibility
      $scope.page = page;

      $scope.chosen = function (side) {
        if (side === 'L') {
          return ($scope.i === page.result.response);
        } else if (side === 'R') {
          return ((4-$scope.i) === page.result.response);
        }
      };

      $scope.btnSrc = function (side) {
        var btnSrc;
        if ($scope.chosen(side)) {
          btnSrc = 'img/radioSeeSaw_selected_24_24.png';
        } else {
          btnSrc = 'img/radioSeeSaw_unselected_24_24.png';
        }
        return btnSrc;
      };

      $scope.choose = function (side) {
        if (side === 'L') {
          page.result.response = $scope.i;
        } else if (side === 'R') {
          page.result.response = (4 - $scope.i);
        }
      };


      function update () {

      }

      //$scope.$watchCollection('page.dm.responseArea', update);
      update();


//      $scope.$watch('selected', function () {
//        page.result.response = $scope.selected;
//      });

    });

});