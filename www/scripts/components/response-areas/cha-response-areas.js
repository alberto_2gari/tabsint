/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.cha-response-areas', [])
    .controller('ChaResponseAreaCtrl', function ($scope, $q, $timeout, examLogic, page, adminLogic, cha, chaExams, chaCheck, logger, disk) {
      $scope.disk = disk;
      $scope.cha = cha;               // Assume $scope.dm is defined from examLogic in parent scope.
      $scope.page = page;
      $scope.chaExams = chaExams;

      $scope.page.dm.isSubmittable = function () {
        return (page.result.response !== undefined);
      };

      // set callback for dropped connections
      cha.connectionDropCB = function() {
        logger.debug('CHA - Running cha connectionDropCB to reset Exam');
        cha.abortExams();
        resetExam();
      };

      function resetExam() {
        chaExams.resetPage();
        $scope.chaError = false;                  // initialize errors to false

        page.result.chaInfo = chaExams.getChaInfo();

        $scope.page.dm.instructionText = page.dm.instructionText;   // set instruction text to top level
        $scope.skippable = page.dm.responseArea.skip;
        $scope.pausable = page.dm.responseArea.pause;

        // Exam definitions
        if (chaExams.complexExamType){
          // do nothing
        } else {
          var examType = page.dm.responseArea.type.substring(3);         // remove 'cha' from prefix
          chaExams.setup(examType, page.dm.responseArea.examProperties); // defines chaExams.examType and chaExams.examProperties
        }

        $scope.isAudiometryExam = chaCheck.isAudiometryExam(chaExams.examType);

        if (page.dm.responseArea.autoBegin && cha.state === 'connected'){
          $scope.startExam(chaExams.examType, chaExams.examProperties);
        } else {
          chaExams.state = 'start';
        }

      }

      $scope.repeatChaExam = function(){
        resetExam();
        $timeout(function(){return $scope.startExam(chaExams.examType, chaExams.examProperties);},0); //todo $timeout may not be needed
      };

      // skip exam
      $scope.skipExam = function () {
        page.result.response = 'skipped';
        examLogic.act.submit();
      };

      function switchToExamView() {
        chaExams.state = 'exam';

        // reset the MainText and SubText to the original page. This will happen when a test is failed and the person repeats the test
        $scope.page.dm.questionMainText = page.dm.questionMainText;
        $scope.page.dm.questionSubText = page.dm.questionSubText;
        $scope.page.dm.instructionText = page.dm.examInstructions;  // custom instructions for each response area - blank if no instructions provided
      }

      function startNoiseIfCalledFor() {
        if (angular.isDefined(page.dm.responseArea.maskingNoise)) {
          cha.startNoiseFeature(page.dm.responseArea.maskingNoise);
        }
      }


      // Start exam, pass exam properties to cha
      $scope.startExam = function(examType, examProperties) {
        return cha.requestStatus()
          .then(cha.handleStatus)
          .then(switchToExamView)
          .then(startNoiseIfCalledFor)
          .then(function() {
            if (angular.isDefined(page.dm.responseArea.delay)){
              return $timeout(function(){ return cha.queueExam(examType, examProperties);},page.dm.responseArea.delay);
            } else {
              return cha.queueExam(examType, examProperties);
            }
          })
          .catch(function(err) {cha.errorHandler.main(err);});
      };

      cha.errorHandler.responseArea = function(err) {
        // Display error to user
        $scope.chaError = true;

        if (err.msg) {
          $scope.errorMessage = 'Error: ' + angular.toJson(err.msg);
        } else {
          $scope.errorMessage = 'Internal error, please restart the exam';
        }

        // make page submittable
        $scope.page.dm.isSubmittable = function () {
          return true;
        };

      };

      resetExam();

    });

});
