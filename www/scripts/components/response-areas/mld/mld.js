/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.cha.mld', [])
    .directive('mldExam', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/response-areas/mld/mld.html',
        controller: 'MLDExamCtrl'
      };
    })
    .controller('MLDExamCtrl', function ($scope, cha, examLogic, page, chaExams, chaRun, logger, $timeout) {
      var pollingTimeout, userResponses = [], currentState;
      var examProps = page.dm.responseArea.examProperties || {};

      // If requireResponse, play each presentation in full with yes/no disabled,
      // then enabled buttons asking listener if they heard tones.
      // If !requireResponse, only show red button, move smoothly from pres to pres
      $scope.requireResponse = angular.isDefined(examProps.RequireResponse) ? examProps.RequireResponse : true;
      $scope.responseDisabled = true;

      // Function to run at exam initiation - happens when the responseArea changes
      function newMLDExam() {
        resetResults();

        return getPresentationInfo();
      }

      function getPresentationInfo() {
        return cha.requestResults()
          .then(presentationHandler)
          .catch(function(err) {cha.errorHandler.main(err);});
      }

      // This is less a results handler in this case, more an exam handler
      function presentationHandler(resultFromCha){
        currentState = resultFromCha.State;
        // console.log('-- received results from CHA: ' + currentState);
        if (currentState === 'BETWEEN' || currentState === 'PLAYING') {
          // BETWEEN is during the timePause interval
          pollingTimeout = $timeout(getPresentationInfo, 500);

          page.result = $.extend({}, page.result, resultFromCha); // don't need to do this so many times, but extend should prevent duplicates
        } else if (currentState === 'WAITING_FOR_RESULT') {
          // WAITING_FOR_RESULT can only be entered if requireResult is true

          // Put presentation properties in the result
          page.result = $.extend({}, page.result, resultFromCha); // don't need to do this so many times, but extend should prevent duplicates
          if ($scope.requireResponse) {enableResponseYesNoButtons();}

        } else if (currentState === 'DONE') {    // End of exam
          page.result = $.extend({}, page.result, resultFromCha);
          page.result = $.extend({}, page.result, {
            response: 'Exam Results',
            chaInfo: chaExams.getChaInfo(),
            userResponses: userResponses
          });
          examLogic.act.submit();
        }
      }

      function enableResponseYesNoButtons() {
        $scope.responseDisabled = false;
      }

      function disableResponseYesNoButtons() {
        $scope.responseDisabled = true;
      }

      // grade the user inputs and show correct answers
      $scope.submitUserInput = function(heardSound) {
        logger.info('CHA processing MLd response: ' + heardSound);

        $timeout.cancel(pollingTimeout);

        userResponses.push({Condition: page.result.Condition, Response: heardSound});

        if ($scope.requireResponse) {disableResponseYesNoButtons();}

        cha.examSubmission('MLD$Submission', {UserResponse: heardSound? 1:0})
          .then(getPresentationInfo)
          .catch(function(err) {cha.errorHandler.main(err);});
      };

      function resetResults() {
        // Adjust display
        var presentationId = page.result.presentationId;
        page.result = {};
        page.result = {
          examType: chaExams.examType,
          presentationId: presentationId,
          responseStartTime: new Date()
        };
      }

      return newMLDExam();

    });

});
