/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.audiometry-table', [])
    .controller('AudiometryTableCtrl', function($scope, page, chaResults){
      page.dm.hideProgressbar = true;
      page.dm.isSubmittable = function () {
        return true;
      };
      // Update page view, get rid of instructions
      $scope.page.dm.title = 'Audiometry Results';
      $scope.page.dm.questionMainText = 'Audiometry Results';
      $scope.page.dm.instructionText = '';

      // this function caclulates audiometry results based on the display id's passed. It will return an array of [resultsLost, audiogramData]
      $scope.resultsList = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[0];

      if (angular.isUndefined($scope.resultsList)){
        $scope.page.dm.questionSubText = 'No Results to Show';
      }
    });

});
