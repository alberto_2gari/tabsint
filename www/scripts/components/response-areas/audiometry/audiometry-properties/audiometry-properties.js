/* jshint bitwise: false */
/* globals ChaWrap: false */


define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.audiometry-properties', [])
    .directive('audiometryProperties', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/components/response-areas/audiometry/audiometry-properties/audiometry-properties.html',
        controller: 'AudiometryPropertiesCtrl'
      };
    })

    .controller('AudiometryPropertiesCtrl', function ($scope) {


    });
});
