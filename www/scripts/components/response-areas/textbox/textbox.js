
define(['angular'], function (angular) {
  'use strict';

angular.module('tabsint.components.response-areas.textbox', [])

  .controller('TextboxResponseAreaCtrl', function ($scope, page) {
    $scope.page = page;

    // prevent the user from pressing enter if the number of rows i
    $('textarea').keydown(function (e) {
      if (e.keyCode === 13) {
        e.preventDefault();
      }
    });

    // Clear textbox when the question ID is changed
    function update() {
      $scope.page.result.response = undefined;

      if (!!(page.dm.responseArea.rows)) {
        $scope.rows = page.dm.responseArea.rows;
      } else {
        $scope.rows = 1;
      }

      if (page.dm.responseArea.submitEmpty) {
        $scope.page.result.response = '';
      }
    }

    update();

    //$scope.$watch('page.dm.id', update);
  });

});