/* global WAVsave */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.nato', [])

 .controller('NatoResponseAreaCtrl', function (page, $timeout, logger, devices) {

    var vm = this;

    // defaults
    vm.disabled = true;
    vm.state = 'preparing';
    vm.sentence = '';
    vm.error = undefined;
    vm.microphone = 'internal';
    var submittable = false;
    var startTime, elapTime, filename;

    /**
     * Response Area init
     */
    vm.$onInit = function() {

      // set submittable logic
      page.dm.isSubmittable = function() {
        return submittable;
      };

      vm.sentence = page.dm.responseArea.sentence || '';
      if (page.dm.responseArea.microphone && _.includes(['internal', 'external'], page.dm.responseArea.microphone)) {
        vm.microphone = page.dm.responseArea.microphone;
      } else {
        logger.debug('Undefined or invalid microphone parameter set in NATO protocol. Using internal microphone');
      }

      // WAVsave Set Up
      WAVsave.init(vm.chooseMic, function(e) {
        vm.error('Failed to initialize WAVsave', e);
      });
    };

    /**
     * WAVsave choose microphone.
     * Assumes vm.microphone is sanitized
     */
    vm.chooseMic = function() {
      if (vm.microphone === 'external') {
        WAVsave.setMicAudioJack(function() {
          logger.info('WAVsave set mic to audio jack');
          vm.record();
        }, function(e) {
          vm.error('Failed while trying to set microphone to external', e);
        });
      } else {
        // default is BuiltIn, so go straight to recording
        vm.record();
      }

    };

    /**
     * WAVsave chooseMic success callback.
     * Start recording right away
     */
    vm.record = function() {
      WAVsave.record(function() {
        logger.info('WAVsave started recording');
        vm.state = 'recording';
        startTime = new Date();
      }, function(e) {
        vm.error('Failed while trying to start recording', e);
      });
    };

    /**
     * Controller tear down
     */
    vm.$onDestroy = function() {
      WAVsave.close();
    };

    /**
     * Stop recording
     */
    vm.stopRecording = function(){
      WAVsave.stop(stopRecordingSuccess, function(e) {
        vm.error('Failed to stop recording', e);
      });
    };

    /**
     * WAVsave stopped successfully
     */
    function stopRecordingSuccess() {
      var time = (new Date()).toJSON().replace(':','-').replace(':','-').split('.')[0];
      elapTime = (new Date()) - startTime;  // milliseconds - not used currently
      filename = devices.shortUUID + '_' + time;
      page.result.response = filename;

      WAVsave.saveWAVfile(function(){
        logger.log('INFO: Successfully saved wav file to filename: ' + filename);

        // change state
        vm.state = 'recorded';
        vm.finish();
      }, function(e) {
        vm.error('Failed while saving audio file', e);
      }, filename);
    }

    /**
     * Plays last recorded audio file
     */
    vm.play = function() {
      WAVsave.play(function(){
      }, function(e) {
        vm.error('Failed to start playing audio file', e);
      });
    };

    /**
     * Stop Playing
     */
    vm.stopPlaying = function(){
      WAVsave.stop(function() {
      }, function(e) {
        vm.error('Failed to stop playing', e);
      });
    };


    /**
     * Error handler for WavSav
     * @param  {string} msg - error message
     */
    vm.error = function(msg, err) {
      vm.state = 'error';
      vm.error = msg;
      logger.log('ERROR: '+ msg + '.  Return from WAVsave: ' + JSON.stringify(err));
      vm.finish();
    };

    /**
     * Finish up the response-area. Make page submittable
     */
    vm.finish = function() {
      submittable = true;
    };

  });
});
