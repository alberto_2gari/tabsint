define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.subject-id', [])

    .controller('SubjectIdResponseAreaCtrl', function ($scope, adminLogic, results, page) {
      $scope.page = page;

      //submission logic
      $scope.page.dm.isSubmittable = function () {
        return (!_.isUndefined(results.current.subjectId));
      };

      $scope.storeResponse = function() {
        results.current.subjectId = page.result.response;  // store in the top level exam subject-id field
      };

      $scope.generate = function() {
        // Generate a random n-digit ascii/letter-number
        var len = 5;
        var text = '';
        var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++) {
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        page.result.response = text;       // store in the current response
        $scope.storeResponse();
      };

    });
});
