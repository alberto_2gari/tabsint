define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.qr', [])

    .controller('QrResponseAreaCtrl', function ($scope, examLogic, page, results, notifications, logger, gettextCatalog) {

      page.dm.isSubmittable = function () {
        return ( !(page.dm.responseArea.required) ||
          (((page.dm.responseArea.scope || 'exam') === 'exam') && results.current.qrString) ||
          (((page.dm.responseArea.scope || 'exam') === 'page') && page.result.response) );
      };

      var scanner;
      try {
        scanner = cordova.plugins.barcodeScanner;
        $scope.qrCodeEnabled = true;
      } catch (err) {
        $scope.qrCodeEnabled = false;
        logger.error('Could not load barcode scanner: ' + angular.toJson(err, true));
      }

      $scope.scanQrCode = function () {
        if (!$scope.qrCodeEnabled) {
          logger.warn('QR Scanning not enabled.');
          return;
        }
        $scope.qrResults = '';
        scanner.scan(function (result) {
          logger.info('Barcode scanning result: ' + JSON.stringify(result));
          $scope.$apply(function () {
            if (result.cancelled) {
              notifications.alert('INFO: QR Scan cancelled.');
              return;
            }

            if (result.format !== 'QR_CODE') {
              notifications.alert(gettextCatalog.getString('Wrong code format: ') + result.format + '. '+gettextCatalog.getString('Should be QR_CODE.'));
              return;
            }

            page.result.response = result.text;
            if (( page.dm.scope || 'exam' ) === 'exam') {
              results.current.qrString = page.result.response;
            }

            logger.debug('QR Scan: ' + angular.toJson(result, true));

            // AUTO-SUBMIT:

            if (page.dm.isSubmittable()) {

              logger.debug('QR Auto-submitting...');
              examLogic.act.submit();
            }
          });
        }, function (result) {
          notifications.alert(gettextCatalog.getString('Barcode error: ') + angular.toJson(result, true));
          $scope.digest();
        });
      };
    });
 });
