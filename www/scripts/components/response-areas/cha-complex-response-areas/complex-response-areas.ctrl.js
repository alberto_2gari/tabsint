/**
 * Created by bpf on 3/8/2016.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('cha.complex-response-areas.ctrl', [])
    .controller('ChaComplexResponseAreaCtrl', function ($scope, $q, $timeout, page, adminLogic, cha, chaExams, chaCheck, logger, disk) {
      $scope.disk = disk;
      $scope.cha = cha;
      $scope.chaExams = chaExams;
      $scope.chaError = false;
      $scope.chaCheck = chaCheck;

      // Reset each exam before each page
      //$scope.$watch('page.dm.responseArea', resetExam); // TODO: replace this watch

      resetExam();

      function resetExam() {
        page.dm.hideProgressbar = true;

        try {
          chaExams.cancelPolling();
          cha.abortExams();
        } catch(e) {
          logger.warn('CHA Error aborting exams when resetExam() executed: ' + e);
        }

        // Exam definitions
        chaExams.complexExamType = page.dm.responseArea.type.substring(3); // used in the html to determine which response area to load

        console.log('types: '+ chaExams.complexExamType+', ' + chaExams.examType);
      }

    });

});
