/**
 * Created by bpf on 3/8/2016.
 */

define(['angular',
  './complex-response-areas.ctrl',
  './manual-audiometry/manual-audiometry',
  './manual-tone-generation/manual-tone-generation',
  './audiometry-list/audiometry-list',
  './sound-recognition/sound-recognition'
], function (angular) {
  'use strict';

  angular.module('tabsint.components.response-areas.cha.complex-response-areas', [
    'cha.complex-response-areas.ctrl',
    'cha.manual-audiometry',
    'cha.manual-tone-generation',
    'cha.audiometry-list',
    'cha.sound-recognition'
  ]);
});
