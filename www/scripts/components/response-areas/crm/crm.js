define(['angular'], function (angular) {
  'use strict';

angular.module('tabsint.components.response-areas.crm', [])    

    .controller('CrmResponseAreaCtrl', function ($scope, examLogic, page) {
      $scope.n = [1, 2, 3, 4, 5, 6, 7, 8];
      $scope.color = ['Blue', 'Red', 'White', 'Green'];
      $scope.space = [' '];
      $scope.btnStyle = {
        Blue: {background: 'rgb(119, 179, 255)'},
        Red: {background: 'rgb(248, 132, 132)'},
        White: {background: 'white'},
        Green: {background: 'rgb(122, 202, 122)'}
      };

      // container spacing
      $scope.spacing = {};
      if (page.dm.responseArea.verticalSpacing) {
        _.extend($scope.spacing, {'padding-bottom':page.dm.responseArea.verticalSpacing});
      }

      if (page.dm.responseArea.horizontalSpacing) {
        _.extend($scope.spacing, {
          'padding-right':page.dm.responseArea.horizontalSpacing/2,
          'padding-left':page.dm.responseArea.horizontalSpacing/2
        });
      }

      $scope.crmchoose = function (btncolor, number) {
        page.result.response = btncolor + ' ' + number;
        page.result.correct = (page.dm.responseArea.correct.toLowerCase() ===  page.result.response.toLowerCase());
        examLogic.act.submit();   // Auto-submitted after response is added and graded
      };
    });
});