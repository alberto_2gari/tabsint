/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular',
  './indicators/indicators',
  './build-details/build-details',
  './debug-view/debug-view',
  './header/header',
  './active-tasks/active-tasks',
  './authorize/authorize',
  './results-warnings/results-warnings',
  './response-areas/response-areas',
  './change-pin/change-pin',
  './partial-result/partial-result',
  './cha-admin/cha-admin',
  './cha-indicator/cha-indicator',
  './cha-info/cha-info'
  ], function (angular) {
  'use strict';

  angular.module('tabsint.components', [
    'tabsint.components.response-areas',
    'tabsint.components.indicators',
    'tabsint.components.build-details',
    'tabsint.components.debug-view',
    'tabsint.components.header',
    'tabsint.components.active-tasks',
    'tabsint.components.authorize',
    'tabsint.components.results-warnings',
    'tabsint.components.partial-result',
    'tabsint.components.change-pin',
    'tabsint.components.cha.admin',
    'tabsint.components.cha.indicator',
    'tabsint.components.cha.info'
  ]);

});
