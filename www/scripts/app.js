/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define([
  // load all dependency modules, as defined in require.config. Likely these will have been shim'd, or have their own AMD support
  // this is equivalent to loading all the scripts in index.html
  'angular',
  'jquery',
  'lodash',
  'es6shim',
  'd3',
  'ngStorage',
  'ngSanitize',
  'ui.bootstrap',
  'ngCordova',
  'jsonFormatter',
  'angularGettext',

  // load all our js files (leave off .js)
  './services/services',
  './components/components',
  './routes/routes',

  // load plugins
  'plugins/require'
  ],

function () {
  'use strict';
  angular.module('tabsint', [
    'ui.bootstrap',
    'ngStorage',
    'ngSanitize',
    'ngCordova',
    'jsonFormatter',
    'gettext',

    // services
    'tabsint.services',
    'tabsint.components',
    'tabsint.routes',

    // tabsint - plugins
    'tabsint.plugin-modules'
    ])

    .config(['$httpProvider', '$sceDelegateProvider', '$compileProvider', '$controllerProvider', function($httpProvider, $sceDelegateProvider, $compileProvider, $controllerProvider) {

      // customize $httpProvider
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];

      // whitelist urls
      $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'cdvfile://localhost/persistent/**',
        'file:///storage/emulated/0/**',
        'file:///data/data/com.creare.skhr.tabsint/files/**'
      ]);

      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file):/);
      $compileProvider.imgSrcSanitizationWhitelist(/^\s*((https?|ftp|file|cdvfile):|data:image\/)/);


      // tabsint window variable
      window.tabsint = {};
      window.tabsint.controller = $controllerProvider.register;
      window.tabsint.hasController = $controllerProvider.has;
    }])

    // directives "my-touchstart" and "my-touchend" to handle touch start and end precisely.
    // Evaluate bound scope function on "touchstart" and "touchstart" events
    .directive('myTouchstart', [function() {
      return function(scope, element, attr) {
        element.on('touchstart', function() {
          scope.$apply(function() {
            scope.$eval(attr.myTouchstart);
          });
        });
      };
    }])
    .directive('myTouchend', [function() {
      return function(scope, element, attr) {
        element.on('touchend', function() {
          scope.$apply(function() {
            scope.$eval(attr.myTouchend);
          });
        });
      };
    }])

    // functions to run on app first start - happens after dependency injection, so all modules should be available
    // once the device is ready, initialize all services
    .run(function($q, app, protocol, version, splashscreen, file, config, cordova, logger, devices, androidFullScreen, partialResult,
                  notifications, tabsintNative, plugins, network, gitlab, tabsintServer, disk, remote, sqLite, gettextCatalog, adminLogic) {

      cordova.ready()
        .then(sqLite.ready)
        .then(file.loadFileSystem)
        .then(devices.load) // this must happen before file, because file now checks device type for local directory setup
        .then(file.ready)
        .then(network.checkStatus)
        .then(function() {

          // necessary synchronous loading functions (these must happen in order)
          config.act.load();
          version.load();

          tabsintServer.$init();
          gitlab.$init();

          protocol.$init();
        })
        .then(logger.count)  // count up logs for admin page
        .catch(function(e) {
          logger.error('Error caught during load: ' + angular.toJson(e));
        })
        .finally(function() {
          splashscreen.hide();

          // add event listeners
          adminLogic.addEventListeners();

          // initialize the volume control through tabsintNative
          tabsintNative.resetAudio(function(msg){ logger.debug('tabsintNative.resetAudio: '+JSON.stringify(msg));}, tabsintNative.onVolumeError);

          // set immersive mode: navigation bar and title bar stay hidden unless swiped in
          androidFullScreen.immersiveMode();

          // load translations
          gettextCatalog.loadRemote('res/translations/translations.json');
          gettextCatalog.setCurrentLanguage(disk.language);

          plugins.runEvent('appLoad');
          logger.info('App Loaded');

          // check to see if partial result is still hanging on disk
          partialResult.check();

          app.deferred.resolve();
        });

      })

    .factory('app', function($q, $window) {

      var api = {
        deferred: undefined,
        ready: undefined,
        tablet: undefined,
        test: undefined,
        browser: undefined,
        debug: undefined
      };

      // for tablet/browser distinction
      if (window.cordova) {
        api.tablet = true;
      } else if (window.__karma__) {
        api.test = true;
      } else {
        api.browser = true;
      }

      api.deferred = $q.defer();
      api.ready = function() {
        return api.deferred.promise;
      };

      return api;
    });

  });
