/**
 * Created by rxc on 12/5/13.
 */
/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */



define(['tv4', 'test-dep', 'app'], function (tv4) {
  'use strict';

  beforeEach(module('tabsint'));

  describe('Protocol', function () {

    var adminLogic, schema, protocol, pm, notifications, logger, json, disk, config, version, plugins;

    beforeEach(inject(function (_adminLogic_, _protocol_, _pm_, _notifications_, _logger_, _json_, _disk_, _config_, _version_, _plugins_){
      adminLogic = _adminLogic_;
      protocol = _protocol_;
      pm = _pm_;
      notifications = _notifications_;
      logger = _logger_;
      json = _json_;
      disk = _disk_;
      config = _config_;
      version = _version_;
      plugins = _plugins_;
    }));

    describe('protocol.$init', function() {

      beforeEach(function() {
        protocol.$init();
      });

      it('should load local protocols', function () {
        expect(pm.local.length).toEqual(5);
      });

      it('should store meta data for local protocols and any plugin protocols onto the disk', function() {
        expect(disk.protocols.length).toEqual(pm.local.length + plugins.elems.localProtocols.length);
      });

      it('should set the disk.server protocol source variable based on the configuration file', function() {
        expect(disk.server).toEqual(config.server.type === 'tabsintServer' ? 'tabsintServer' : 'localServer');
      });

    });

    describe('protocol.validate', function() {

      beforeEach(function() {
        protocol.$init();
      });

      it('built in protocols (including plugin protocols) should pass protocol schema validation.', function () {
        _.forEach(disk.protocols, function(meta) {
          var p = json.load(meta.path + 'protocol.json');
          var validationResult = protocol.validate(p);
          //Added a more valuable error message. Note that this syntax is not documented or supported,
          //and may break in the future, but including this message makes debugging MUCH easier.
          expect(validationResult.valid).toBeTruthy('Protocol validation failed for :' + meta.path);
        });
      });


    });

    describe('protocol.load', function() {

      it('should notice when the minimum tabsint version is greater than the current version', function () {
        protocol.$init();
        config.act.load();
        version.load();

        protocol.load(_.find(pm.local, {name: 'feature_demo'}))
          .then(function(){
            expect(pm.root.protocolTabsintOutdated).toBeFalsy();
          });


        protocol.load(_.find(pm.local, {name: 'mock'}))
          .then(function(){
            expect(pm.root.protocolTabsintOutdated).toBeTruthy();
          });

      });

      xit('should alert when a calibration file exists without version fields.', function () {
        spyOn(notifications, 'alert');
        protocol.load('res/protocol/mock'); // does have version fields
        expect(notifications.alert).not.toHaveBeenCalled();

        protocol.load('res/protocol/style_dev'); // doesn't have version fields
        expect(notifications.alert).toHaveBeenCalled();
      });

    });


  });
});
