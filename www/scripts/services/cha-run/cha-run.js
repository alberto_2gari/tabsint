/**
 * Created by bpf on 3/2/2016.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cha.run', [])
    .factory('chaRun', function ($q, page, cha, chaExams, chaResults, logger) {

      var api = {};

      // Run Third Octave Band Nosie Measurement after another exam if called for
      api.runTOBifCalledFor = function(){

        var deferred = $q.defer();
        var measure = page.dm.responseArea.measureBackground;
        if (measure !== angular.undefined && measure === 'ThirdOctaveBands'){
          logger.debug('Measuring background using '+measure);
          var tmpMainText = page.dm.questionMainText;
          var tmpSubText = page.dm.questionSubText;
          page.dm.questionMainText = 'Measuring Background Noise';
          page.dm.questionSubText = 'Please sit quietly and do not move';
          chaExams.state = 'measureingBackgroundNoise';
          return cha.queueExam('ThirdOctaveBands', {})
            .then(chaExams.wait.forReadyState)
            .then(cha.requestResults)
            .then(function(results) {
              chaResults.addTOBResults(results);
              page.dm.questionMainText = tmpMainText;
              page.dm.questionSubText = tmpSubText;
            });

        } else { //ThirdOctaveBands not called for
          deferred.resolve();
          return deferred.promise;
        }
      };

      return api;
    });


});
