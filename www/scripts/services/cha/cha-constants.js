/* Constants and state enums for cha

Created by @jkp 2017-10-09
*/
define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cha.constants', [])
    .factory('chaConstants', function () {

      var api = {
        fileOpStates: {},
        errors: {},
        flags: {},
        chaTasks: {}
      };

      const FILE_OP_STATES = {
      //api.fileOpStates = {
        NONE: 'no_file_op',
        FIRMWARE_TRANSFER: 'firmwareTransfer',
        MEDIA_TRANSFER: 'mediaTransfer',
        CONFIRMING_FIRMWARE: 'confirmingFirmware',
        GET_DIRECTORY: 'getDirectory',
        GET_DIRECTORY_CRC: 'getDirectoryCrc'
      };

      Object.freeze(FILE_OP_STATES);
      api.fileOpStates = FILE_OP_STATES;

      const CHA_TASKS = {
        FILE_TRANSFER: 'file-transfer',
        CANCEL_FIRMWARE: 'cancel-firmware',
        UPDATE_FIRMWARE: 'update-firmware',
        CONNECT: 'connect',
        DISCONNECT: 'disconnect',
        REBOOT: 'reboot',
        REQUESTA2DP: 'requesta2dp',
        CONNECTA2DP: 'connecta2dp',
        ISCONNECTEDA2DP: 'isconnecteda2dp',
        DISCONNECTA2DP: 'disconnecta2dp',
        QUEUE: 'queue',
        ABORT: 'abort',
        REQUESTID: 'requestid',
        REQUESTPROBEID: 'requestprobeid',
      };

      Object.freeze(CHA_TASKS);
      api.chaTasks = CHA_TASKS;

      // CHA Flag codes
      //Full list of CHA Codes from HearingProducts/Shared/include/chaCodes.h
      //message from 'cha api.rst'
      const ERRORS =
      {
        0x00000001: {message: "Error initializing. The I2C bus is unavailable. Return to manufacturer for repair.",
                      level: "error", name: "CHA_FLAGS_I2C_FAIL"},
        0x00000002: {message: "The configuration data is invalid on at least one page; defaults have been used. Return to manufacturer for repair.",
                      level: "error", name: "CHA_FLAGS_CONFIG_FAIL"},
        0x00000004: {message: "Storage error: The SD card is missing, corrupt, or has the wrong password. Return to manufacturer for repair.",
                      level: "error", name: "CHA_FLAGS_FILE_SYSTEM_FAIL"},
        0x00000008: {message: "The Bluetooth module did not respond to configuration. Bluetooth classic may be unavailable. Return to manufacturer for repair.",
                      level: "error", name: "CHA_FLAGS_BT_FAIL"},
        0x00000010: {message: "The Bluetooth Low Energy module did not respond to configuration. Bluetooth LE may be unavailable. Return to manufacturer for repair.",
                      level: "error", name: "CHA_FLAGS_BTLE_FAIL"},
        0x00000020: {message: "The USB module did not configure properly. USB debugging mode may be unavailable. Return to manufacturer for repair.",
                      level: "error", name: "CHA_FLAGS_USB_FAIL"},
        0x00000040: {message: "The analog interface chip did not respond to configuration. Audio processing unavailable. Return to manufacturer for repair.",
                      level: "error", name: "CHA_FLAGS_AIC_FAIL"},
        0x00000080: {message: "The noise feature is active.",
                      level: "info", name: "CHA_FLAGS_NOISE_ON"},
        0x00000100: {message: "The noise feature is paused.",
                      level: "info", name: "CHA_FLAGS_NOISE_PAUSED"},
        0x00000200: {message: "The blue button is currently active.",
                      level: "info", name: "CHA_FLAGS_BUTTON_BLUE"},
        0x00000400: {message: "The red button is currently active.",
                      level: "info", name: "CHA_FLAGS_BUTTON_RED"},
        0x00000800: {message: "The device is charging.",
                      level: "info", name: "CHA_FLAGS_CHARGING"},
        0x00001000: {message: "The device is connected to a host PC via USB.",
                      level: "info", name: "CHA_FLAGS_USB_CONNECTED"},
        0x00002000: {message: "The device is running from the second stage bootloader firmware. The main device firmware is invalid. Consider contacting manufacturer for repair.",
                      level: "info", name: "CHA_FLAGS_BOOTLOADER"},
        0x00010000: {message: "Attenuator L1 is in circuit.",
                      level: "info", name: "CHA_FLAGS_ATTEN_L1"},
        0x00020000: {message: "Attenuator L2 is in circuit.",
                      level: "info", name: "CHA_FLAGS_ATTEN_L2"},
        0x00040000: {message: "Attenuator R1 is in circuit.",
                      level: "info", name: "CHA_FLAGS_ATTEN_R1"},
        0x00080000: {message: "Attenuator R2 is in circuit.",
                      level: "info", name: "CHA_FLAGS_ATTEN_R2"},
        0x00100000: {message: "Recording over USB is active.",
                      level: "info", name: "CHA_FLAGS_RECORDING"},
        0x02000000: {message: "The blue button has been pressed since the last status query.",
                      level: "info", name: "CHA_FLAGS_BUTTON_LATCH_BLUE",},
        0x04000000: {message: "The red button has been pressed since the last status query.",
                      level: "info", name: "CHA_FLAGS_BUTTON_LATCH_RED"},
      };

      Object.freeze(ERRORS);
      api.errors = ERRORS;

      //These constants are defined here because at least some are still used,
      //and they may prove useful elsewhere.
      const FLAGS =
      {
        CHA_FLAGS_I2C_FAIL :          0x00000001, //error"The I2C bus is unavailable." during "Initialization."
        CHA_FLAGS_CONFIG_FAIL :       0x00000002, //error "The configuration data is invalid on at least one page; defaults have been used." during "Initialization."
        CHA_FLAGS_FILE_SYSTEM_FAIL :  0x00000004, //error"The SD card is missing, corrupt, or has the wrong password." during "Initialization."
        CHA_FLAGS_BT_FAIL :           0x00000008, //error"The Bluetooth module did not respond to configuration." during "Initialization.",
        CHA_FLAGS_BTLE_FAIL :         0x00000010, //error"The Bluetooth Low Energy module did not respond to configuration." during "Initialization."
        CHA_FLAGS_USB_FAIL :          0x00000020, //error"The USB module did not configure properly." during "Initialization."
        CHA_FLAGS_AIC_FAIL :          0x00000040, //error "The analog interface chip did not respond to configuration." during "Initialization."
        CHA_FLAGS_NOISE_ON :          0x00000080, //info "The noise feature is active."  "When started."
        CHA_FLAGS_NOISE_PAUSED :      0x00000100, //info "The noise feature is paused.", "After noise started and paused."
        CHA_FLAGS_BUTTON_BLUE :       0x00000200, //info "The **blue** button is currently active.", "When pressed.",
        CHA_FLAGS_BUTTON_RED :        0x00000400, //info "The **red** button is currently active.", "When pressed.",
        CHA_FLAGS_CHARGING :          0x00000800, //info "The device is charging.", "When connected to USB power AND the battery is not charged.",
        CHA_FLAGS_USB_CONNECTED :     0x00001000, //info "The device is connected to a host PC.", "When connected to USB power.",
        CHA_FLAGS_BOOTLOADER :        0x00002000, //info "The device is running from the second stage bootloader firmware.", "The main device firmware is invalid."
        CHA_FLAGS_ATTEN_L1 :          0x00010000, //info "Attenuator L1 is in circuit.", "When required."
        CHA_FLAGS_ATTEN_L2 :          0x00020000, //info "Attenuator L2 is in circuit.", "When required.",
        CHA_FLAGS_ATTEN_R1 :          0x00040000, //info "Attenuator R1 is in circuit.", "When required."
        CHA_FLAGS_ATTEN_R2 :          0x00080000, //info "Attenuator R2 is in circuit.", "When required."
        CHA_FLAGS_RECORDING :         0x00100000, //info "Raw transmission of samples out the USB port is active.", "When the recording feature is used over USB.",
        CHA_FLAGS_BUTTON_LATCH_BLUE : 0x02000000, //info "The **blue** button has been pressed since the last status query.", "On button press.",
        CHA_FLAGS_BUTTON_LATCH_RED :  0x04000000, //info "The **red** button has been pressed since the last status query.", "On button press.",
      };

      Object.freeze(FLAGS);
      api.flags = FLAGS;

      return api;

    });

});
