/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.tasks', [])

    .factory('tasks', function ($timeout, $q, noSleep, app, $cordovaFile, devices) {
      // Service to keep track of active tasks and when TabSINT should be 'busy'
      // when 'busy', many buttons are disabled to prevent conflicting actions
      // Registering a task establishes busy state
      // When all tasks have been deregistered, state becomes not busy

      var tasks = {
        register: undefined,
        deregister: undefined,
        deregisterAll: undefined,
        isOngoing: undefined,
        numOngoing: undefined,
        list: {},
        busy: undefined,
        notBusy: undefined,
        disabled: undefined
      };

      // activity classes
      tasks.busy = function() {
        tasks.disabled = true;
      };

      tasks.notBusy = function() {
        tasks.disabled = false;

        // get free disk space once done with operation
        if (app.tablet) {
          devices.getDiskSpace();
        }
      };
      tasks.notBusy();

      tasks.numOngoing = function(){
        return _.keys(tasks.list).length;
      };

      tasks.isOngoing = function(task) {
        return _.has(tasks.list, task);
      };

      tasks.register = function(task, msg) {
        tasks.busy();
        _.debounce(noSleep.keepAwake, 500, true);
        tasks.list[task] = msg;

        return $timeout(function(){}, 20);
      };

      tasks.deregister = function(task) {
        delete tasks.list[task];

        if (tasks.numOngoing() < 1){
          noSleep.allowSleepAgain();
          tasks.notBusy();
        }

        return $timeout(function(){}, 20);
      };

      tasks.deregisterAll = function() {
        tasks.list = {};
        tasks.notBusy();
        noSleep.allowSleepAgain();
      };

      return tasks;
    });

});
