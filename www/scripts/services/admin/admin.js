/**
 * Created by RXC on 12/5/13.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.admin', [])
    .factory('adminLogic', function ($timeout, $window, app, file, disk, $q, notifications, tasks, logger, network, networkModel,
                                    tabletLocation, logExport, protocol, pm, media, gitlab, fileChooser, tabsintNative, remote, config,
                                    router, results, paths, tabsintServer, localServer, examLogic, devices, plugins, $uibModal, $rootScope,
                                    gettextCatalog, checksum, chaExams) {

      var admin = {};

      /**
       * Container to hold the state of the admin window panes (open/closed)
       * Only implementing on the "settings" (config) page currently
       * @type {Object}
       */
      admin.panes = {
        config: {
          general: true,
          cha: true
        },
        protocols: {}
      };

      /**
       * Method to switch to admin view
       */
      admin.switchToAdminView = function() {
        try {
          examLogic.act.stopAllMedia();
        } catch(e) {
          logger.debug(`Failed to stop all media switching to admin view with error : ${JSON.stringify(e)}`); 
        }

        try { chaExams.reset(); }
        catch (e) { logger.debug('CHA - chaExams.reset failed on switchToAdminView with error: ' + angular.toJson(e)); }

        plugins.runEvent('switchToAdminView');
        router.goto('ADMIN', true);
      };


      /**
       * Toggle Debug Mode
       */
      admin.toggleDebugMode = function() {
        if (disk.debugMode) {
          logger.debug('Admin mode enabled');
        }
      };

      /**
       * Toggle Disable Logs Mode
       */
      admin.toggleDisableLogs = function() {
        if (disk.disableLogs) {
          logger.debug('Logs are now disabled.');
        } else {
          logger.debug('re-enabled logging.');
        }
      };

      /**
       * Toggle Disable Volume Enforcement Mode
       */
      admin.toggleDisableVolume = function() {
        if (disk.disableVolume) {
          notifications.confirm(gettextCatalog.getString('Are you sure you want to disable Automatic Volume Control within TabSINT? ') +
              gettextCatalog.getString('This feature is essential to providing calibrated audio during tests.'),
            function(buttonIndex) {
              if (buttonIndex === 1) {
                logger.debug('Automatic Volume control is now disabled.');
              } else {
                disk.disableVolume = false;  // reset the volume flag to false if they cancel
              }
            });
        } else {
          logger.debug('Automatic Volume control re-enabled');
        }
      };

      /**
       * Toggle Enable Amin Skip Mode
       */
      admin.toggleAdminSkipMode = function() {
        if (disk.adminSkipMode) {
          notifications.confirm(gettextCatalog.getString('Are you sure you want to enable Skip Mode in all Exams? ')+
                gettextCatalog.getString('This is a testing feature and may not work properly with all protocols.'),
            function(buttonIndex) {
              if (buttonIndex === 1) {
                logger.debug('Admin skipping mode is now enabled.');
              } else {
                disk.disableVolume = false;  // reset the volume flag to false if they cancel
              }
            });
        } else {
          logger.debug('Admin skipping mode disabled.');
        }
      };


      /**
       * Used to toggle app developer mode
       */
      admin.toggleAppDeveloperMode = function(){
        if (!disk.appDeveloperMode) {
          disk.appDeveloperModeCount += 1;

          if (disk.appDeveloperModeCount >= 5) {
            logger.debug('App-Developer Mode turned on.');
            disk.appDeveloperMode = true;
            disk.appDeveloperModeCount = 0;

            loadWindow();
          }

          $timeout(function () {
              disk.appDeveloperModeCount = 0;
            }, 20000);
        } else {
          disk.appDeveloperMode = false;
          disk.appDeveloperModeCount = 0;

          delete $window.tabsintDev;
        }
      };

      // load appDeveloperMode when app first loads
      if (disk.appDeveloperMode) {
        loadWindow();
      }

      // put everything on window for dev purposes
      function loadWindow() {
        // add dependencies to window variable for debugging
        $window.tabsintDev = {
          examLogic: examLogic,
          adminLogic: admin,
          devices: devices,
          disk: disk,
          file: file,
          $q: $q,
          protocol: protocol,
          pm: pm,
          remote: remote,
          config: config,
          media: media,
          network: network,
          networkModel: networkModel,
          tabsintNative: tabsintNative,
          fileChooser: fileChooser,
          tasks: tasks,
          gitlab: gitlab,
          paths: paths,
          tabsintServer: tabsintServer,
          localServer: localServer,
          plugins: plugins,
          checksum: checksum,
          successCB: function(m){console.log('success: '+angular.toJson(m));},
          errCB: function(e){console.log('error: '+angular.toJson(e));}
        };
      }

      /**
       * Used to toggle external control mode
       */
      admin.toggleExternalMode = function(){
        if (disk.externalMode) {
          examLogic.act.reset();
        }
      };


      admin.sendInterAppData = function() {
        if (disk.interApp.appName) {
          var d = {
            message: disk.interApp.dataOut.message || 'Test InterApp Message',
            data: disk.interApp.dataOut.data || 'Test InterApp Data'
          };
          tabsintNative.sendExternalData(
            function(){logger.info('Successfully sent data ' + JSON.stringify(d) + ' to external app ' + disk.interApp.appName + '.')},
            function(e){
              logger.error('Sending data ' + JSON.stringify(d) + ' to external app ' + disk.interApp.appName + '.  Error: ' + JSON.stringify(e));
              var strmsg = JSON.stringify(e);
              if (strmsg.indexOf('No Activity found to handle Intent')) {
                notifications.alert(gettextCatalog.getString('Warning:  Error sending data to external app ') + disk.interApp.appName + gettextCatalog.getString('.  No app by that name can be found on this device.'));
              }
            },
            disk.interApp.appName,
            d
          );
        }
      };

      tabsintNative.setExternalDataHandler(
        function(d) {disk.interApp.dataIn = d; $rootScope.$apply(); },
        function(e) {logger.error('in interApp data handler: ' + JSON.stringify(e))}
      );


      /**
       * Toggle Keyboard space
       * @param {number} height - height, in px
       */
      admin.keyboardStyle = {'padding-top': '0px'};
      admin.keyboardSpace = function(height) {
        $timeout(function() {
          admin.keyboardStyle = {'padding-top': height + 'px'};
        }, 0);
      };


      /**
       * Add event listeners to app
       */
      admin.addEventListeners = function() {

        // ionic keyboard handler
        $window.addEventListener('native.keyboardshow', function(e) {
          admin.keyboardSpace(e.keyboardHeight);
        });

        $window.addEventListener('native.keyboardhide', function(e) {
          admin.keyboardSpace(0);
        });

        // network coming online handler
        var checkingNetworkStatus = false;          // init to false
        $window.addEventListener('online', function() {
            if (!networkModel.status && !checkingNetworkStatus) {
              checkingNetworkStatus = true;
              app.ready()
                .then(network.checkStatus)
                .then(function() {

                  // Update the GPS position
                  tabletLocation.updateCurrentPosition();

                  // auto-upload results
                  if (disk.autoUpload){
                    results.uploadAll();
                  } else {
                    logExport.upload();
                  }
                })
                .finally(function() {checkingNetworkStatus = false});
            }
          }, false);

        // network going offline handler
        $window.addEventListener('offline', function(){
            if (networkModel.status && !checkingNetworkStatus){
              checkingNetworkStatus = true;
              app.ready()
                .then(network.checkStatus)
                .finally(function() {checkingNetworkStatus = false});
            }
          }, false);
      };


      /**
       * Used in active-tasks - should be moved elsewhere ?
       */
      admin.cancelDownload = function() {
        if (file.downloadInProgress){
          logger.info(' Aborting download.');
          file.abort();
          disk.downloadInProgress = false;
        }
      };

      /**
       * Method to reload tabsint. 
       * Not currently used, keeping this around for now
       */
      admin.reloadTabsint = function() {
        logger.debug('Reloading TabSINT because of a custom response area in the current or previous protocol...');
        disk.reloadingBrowser = true; // flag to go back to Admin Page
        try {
          navigator.app.loadUrl(location.href);
        } catch(e) {
          logger.error('Failed to reload using  window.location = index.html');
          notifications.alert(gettextCatalog.getString('TabSINT needs to reload for the current protocol to function properly. Please re-open tabSINT manually after it closes.'));
          navigator.app.exitApp();
        }
      };

      return admin;

    });

});
