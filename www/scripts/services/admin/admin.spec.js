/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */
/**
 * Created by rxc on 12/5/13.
 */


define(['test-dep', 'app'], function () {
  'use strict';

  beforeEach(module('tabsint'));

  describe('AdminLogic', function () {

    var adminLogic, disk, $rootScope, pm, $httpBackend, examLogic, config, notifications, json, logger, timeout, sqLite, network, file, router, results;

    beforeEach(module(function ($provide) {
      $provide.value('authorize', {
        modalAuthorize: function (targetFn) {
          targetFn();
        } // assume authorized.
      });
    }));
    beforeEach(module(function ($provide) {
      $provide.value('chooseCha', {
        discover: function () {}
      });
    }));

    beforeEach(inject(function ($injector, _adminLogic_, _disk_, _$rootScope_, _$httpBackend_, _examLogic_, _pm_, _config_, _notifications_,  _json_, _logger_, _$timeout_, _results_, _sqLite_, _network_, _file_, _router_) {
      adminLogic = _adminLogic_;
      disk = _disk_;
      $rootScope = _$rootScope_;
      $httpBackend = _$httpBackend_;
      examLogic = _examLogic_;
      config = _config_;
      notifications = _notifications_;
      json = _json_;
      logger = _logger_;
      timeout = _$timeout_;
      sqLite = _sqLite_;
      network = _network_;
      file = _file_;
      router = _router_;
      results = _results_;
      pm = _pm_;
    }));


  });
});
