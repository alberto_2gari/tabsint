/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

// jshint ignore: start

define(['test-dep', 'app'], function () {
  'use strict';

  beforeEach(module('tabsint'));

  describe('Results Service', function () {

    var results, disk, file;

    beforeEach(inject(function ($injector,  _results_, _disk_, _file_) {
      results = _results_;
      disk = _disk_;
      file = _file_;
    }));

    function fakeResult(siteId) {
      return {siteId: siteId, testResults: {fake: 'fake result'}, testDateTime: (new Date()).toJSON()};
    }

    describe('results.save', function() {

      it('should properly push and delete results from the queue', function () {
        spyOn(file, 'createFile');

        var r1 = fakeResult(1), r2 = fakeResult(2);
        disk.server = 'localServer';   // do this so that results do not get uploaded

        results.save(r1);
        expect(_.includes(disk.queuedResults, r1)).toBeTruthy();
        expect(_.includes(disk.queuedResults, r2)).toBeFalsy();

        results.save(r2);
        expect(_.includes(disk.queuedResults, r1)).toBeTruthy();
        expect(_.includes(disk.queuedResults, r2)).toBeTruthy();

        results.delete(0);
        expect(_.includes(disk.queuedResults, r1)).toBeFalsy();
        expect(_.includes(disk.queuedResults, r2)).toBeTruthy();

        results.delete(0);
        expect(_.includes(disk.queuedResults, r1)).toBeFalsy();
        expect(_.includes(disk.queuedResults, r2)).toBeFalsy();

      });

      it('should backup results every time', function() {});

      it('should export results if its a localServer', function() {});

      it('should upload results if its a gitlab server or tabsintServer', function() {});

      it('should show alert if too many results are queued', function() {  });

    });

    xdescribe('results.upload', function() {

      // this was copied over from admin.spec.js and should be adapted to fit here

      describe('Upload', function () {
        function expectPost(val) {
          $httpBackend.expectPOST(config.server.url + 'InsertTest')
            .respond(200, '{"success":' + val + ',"status":"created","testid":10}');
        }

        var logSuccess;

        function expectLog() {
          $httpBackend.whenPOST(config.server.url + 'UploadLogJSON')
            .respond(function(method, url, data) {
              var val = false;
              if (logSuccess.length > 0){val = logSuccess.splice(0,1)[0];}
              if (val) {
                console.log('respond');
                data = JSON.parse(data);
                var acceptedLogs = [];
                data.logs.forEach(function (item) {
                  acceptedLogs.push(item.msgID);
                });
                return [200, '{"success":"true","acceptedLogs":' + angular.toJson(acceptedLogs) + '}'];
              } else {
                return [200, '{"success":"false"}'];
              }
            });
        }

        beforeEach(function(){
          expectLog();
        });

        function expectGet(num) {
          $httpBackend.expectGET(config.server.url + 'countTests')
            .respond(200, '{"success":true,"status":"counted","count":"' + num + '"}');
        }

        function fakeResult(siteId) {
          return {siteId: siteId, testResults: {fake: 'fake result'}, testDateTime: (new Date()).toJSON()};
        }

        beforeEach(function () {
          disk.queuedResults = [fakeResult(1), fakeResult(2), fakeResult(3)];
          disk.autoUpload = false;
          networkModel.status = true;
          spyOn(notifications, 'alert');
          spyOn(notifications, 'confirm');
        });

        afterEach(function () {
          $httpBackend.verifyNoOutstandingExpectation();
          $httpBackend.verifyNoOutstandingRequest();
        });

        it('should upload a single result properly', function () {
          adminLogic.act.uploadResults(1);
          expectGet(1);
          expectPost('true');
          expectGet(2);
          expectReportTrigger();
          logSuccess = [true];
          $httpBackend.flush();
          expect(disk.queuedResults.length).toEqual(2);
        });

        it('should fail gracefully in the single result case if success==false', function () {
          adminLogic.act.uploadResults();
          expectGet(1);
          expectPost('false');
          logSuccess = [true];
          $httpBackend.flush();
          expect(disk.queuedResults.length).toEqual(3);
          expect(notifications.alert).toHaveBeenCalled();
        });

        it('should fail gracefully in the single result case if testresults not incremented', function () {
          adminLogic.act.uploadResults();
          expectGet(1);
          expectPost('true');
          expectGet(1);
          logSuccess = [true];
          $httpBackend.flush();
          expect(disk.queuedResults.length).toEqual(3);
          expect(notifications.alert).toHaveBeenCalled();
        });

      });

      describe('Auto-uploader ', function () {
        function expectPost(val) {
          $httpBackend.expectPOST(config.server.url + 'InsertTest')
            .respond(200, '{"success":' + val + ',"status":"created","testid":10}');
        }

        function expectGet(num) {
          $httpBackend.expectGET(config.server.url + 'countTests')
            .respond(200, '{"success":true,"status":"counted","count":"' + num + '"}');
        }

        // must call this after each expectPost now - auto-uploading logs once after uploading result(s)
        var logSuccess;

        function expectLog() {
          $httpBackend.whenPOST(config.server.url + 'UploadLogJSON')
            .respond(function(method, url, data) {
              var val = false;
              if (logSuccess.length > 0){val = logSuccess.splice(0,1)[0];}
              if (val) {
                console.log('respond');
                data = JSON.parse(data);
                var acceptedLogs = [];
                data.logs.forEach(function (item) {
                  acceptedLogs.push(item.msgID);
                });
                return [200, '{"success":"true","acceptedLogs":' + angular.toJson(acceptedLogs) + '}'];
              } else {
                return [200, '{"success":"false"}'];
              }
            });
        }

        beforeEach(function(){
          expectLog();
        });

        beforeEach(function () {
          networkModel.status = true;
          disk.autoUpload = false;
          disk.queuedResults = [];
          disk.suppressAlerts = false;
          spyOn(notifications, 'alert');
          spyOn(notifications, 'confirm');
          config.act.load();
          disk.site.siteName = 'defaultName';
          disk.site.siteId = 7;
        });

        afterEach(function () {
          $httpBackend.verifyNoOutstandingExpectation();
          $httpBackend.verifyNoOutstandingRequest();
        });

        it('should alert if results are piling up and auto-upload is off', function () {
          if (disk.server === 'tabsintServer') {
            expect(disk.queuedResults.length).toEqual(0);
            var r1;
            for (var i = 0; i < results.queuedResultsWarnLength; i++) {
              r1 = 'c' + i;
              results.save(r1);
            }
            expect(disk.queuedResults.length).toEqual(results.queuedResultsWarnLength);
            expect(notifications.confirm).toHaveBeenCalled();
          }
        });

        it('should not alert if results are piling up and auto-upload is off, but suppressAlerts is enabled', function () {
          if (disk.server === 'tabsintServer') {
            disk.suppressAlerts = true;

            expect(disk.queuedResults.length).toEqual(0);
            var r1;
            for (var i = 0; i < results.queuedResultsWarnLength; i++) {
              r1 = 'c' + i;
              results.save(r1);
            }
            expect(disk.queuedResults.length).toEqual(results.queuedResultsWarnLength);
            expect(notifications.confirm).not.toHaveBeenCalled();
          }
        });

        it('should alert when user toggles auto-upload on but device is not online', function () {
          if (disk.server === 'tabsintServer') {
            networkModel.status = false;

            disk.autoUpload = true;
            adminLogic.act.changedAutoUpload();

            expect(disk.autoUpload).toBeTruthy();
            expect(notifications.alert).toHaveBeenCalled();
          }

        });

        it('should successfully upload when user toggles auto-upload on and device is online', function () {
          if (disk.server === 'tabsintServer') {
            var r1 = {stuff: 'b'};
            results.save(r1);
            expect(disk.queuedResults.length).toEqual(1);

            expectGet(1);
            expectPost('true');
            expectGet(2);
            expectReportTrigger();
            logSuccess = [true];
            disk.autoUpload = true;
            adminLogic.act.changedAutoUpload();

            expect(disk.autoUpload).toBeTruthy();
            $httpBackend.flush();

            expect(disk.queuedResults.length).toEqual(0);
          }
        });

        it('should alert when it fails to upload when user toggles auto-upload on and device is online', function () {
          if (disk.server === 'tabsintServer') {
            var r1 = {stuff: 'b'};
            results.save(r1);
            expect(disk.queuedResults.length).toEqual(1);

            disk.autoUpload = true;
            adminLogic.act.changedAutoUpload();
            expect(disk.autoUpload).toBeTruthy();

            expectGet(1);
            expectPost('false');
            logSuccess = [true];
            $httpBackend.flush();

            expect(disk.queuedResults.length).toEqual(1);
            expect(notifications.alert).toHaveBeenCalled();
          }
        });

      });

    });


  });
});
