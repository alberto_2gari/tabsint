/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.results', [])

    .factory('results', function (logger, app, disk, $cordovaFile, notifications, paths, config, file, gitlab,
                                  tabsintServer, localServer, $q, networkModel, tasks, logExport, devices, gettextCatalog, csv) {

      var results = {};

      results.dm = {
        queuedResultsWarnLength: 10,
        uploadSummaryLength: 30
      };

      // container to hold the current exam results (previously `results.current`)
      results.current = undefined;

      /**
       * Generate a standardized filename across results
       * @param  {object} [result] - result object. If no result object provided, then it will use the current datetime
       * @param  {object} [suffix] - optional suffix to append
       * @return {string}        string filename
       */
      results.filename = function(result, suffix) {
        var dateTime, filename;

        // set dateTime to testDateTime if result is passed, otherwise set to the current dateTime
        if (result) {
          dateTime = result.testDateTime.replace(':', '-').replace(':', '-').split('.')[0];
        } else {
          dateTime = (new Date()).toJSON().replace(':', '-').replace(':', '-').split('.')[0];
        }

        filename = devices.shortUUID + '_' + dateTime;

        // add any suffix asked for
        if (suffix) {
          filename = filename + suffix;
        }

        return filename;
      };

      /**
       * Method to save a result in preperation for export/upload
       * Update of adminLogic.act.pushResult
       * @param  {object} result - raw result object
       * @return {promise}        [description]
       */
      results.save = function(result) {
        logger.info('Saving result: ' + angular.toJson(result));
        disk.queuedResults.push(result);            // save result

        // exit during testing
        if (app.test) {
          return;
        }

        // back up result object in local file
        return results.backup(result)
          .then(function() {
            // export/upload the result
            var idx = disk.queuedResults.length - 1;

            if (disk.autoUpload) {
              if (disk.server === 'localServer'){
                return results.export(idx);
              } else {
                return results.upload(idx);
              }
            }
          })
          .finally(function() {
            if (disk.exportCSV) {
              results.exportCSV(result);
            }

            // alert if too many results are stored locally
            if (disk.queuedResults.length >= results.dm.queuedResultsWarnLength) {
              results.resultsAlert();
            }

            // upload logs
            if (!disk.disableLogs) {
              return logExport.export();
            }
          });

      };

      /**
       * Back up result object to disk. Backups will be saved in .tabsint-results-backup
       * @param {object} result - result object to backup to file on sd card
       */
      results.backup = function(result){
        var filename, dir;
        filename = results.filename(result, '.json');
        dir = '.tabsint-results-backup/' + result.protocolName;

        return file.createDirRecursively(paths.public(''), dir)
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filename, angular.toJson(result));
          })
          .then(function() {
              logger.info('Successfully exported backup result to file: ' + filename);
            }, function(e) {
              logger.error('Failed to export backup result to file with error: ' + angular.toJson(e));
            });
      };

      /**
       * Pop alert that too many results are being queued.
       * Allow the user to suppress for one day
       */
      results.resultsAlert = function (){
        if (!disk.suppressAlerts) {
          logger.warn('' + disk.queuedResults.length + gettextCatalog.getString(' have accumulated on the tablet'));
          notifications.confirm(disk.queuedResults.length + gettextCatalog.getString(' results are saved on the tablet. Please upload or export and remove results from the tablet to avoid losing data'),
            function(btnIdx) {
              if (btnIdx === 2) {
                results.suppressAlerts();
              }
            }, 'Warning', ['OK', 'Suppress Warnings for 1 Day']
          );
        }
      };


      /**
       * Suppress queued results warning for 1 day
       */
      results.suppressAlerts = function() {
        logger.info('Too many results: Suppress turned on');
          disk.suppressAlerts = true;
          setTimeout(function(){
            disk.suppressAlerts = false;
            logger.info('Too many results: Suppress turned back off by timeout');
          }, 1000*60*60*24);
      };


      /**
       * Method to upload results to an uploadable server type.
       * @return {} [description]
       */
      results.uploadAll = function() {

        var q = $q.defer();
        var promise = q.promise;  // variable to collect promises throughout

        // upload each result individually
        _.forEachRight(disk.queuedResults, function(r, idx) {
          promise = promise.then(function() {
            return results.upload(idx);
          });
        });

        // start the promise chain
        q.resolve();
        return promise;
      };

      /**
       * Method to upload result to an uploadable server type.
       * Errors during the upload process are handled within this method, so rejected promises will be empty.
       * @param {number} [idx] = 0 - index of result to upload
       * @return {promise} Promise to upload single result.
       */
      results.upload = function(idx) {
        var r;
        if (!angular.isDefined(idx)) {
          idx = 0;
        } else if (idx >= disk.queuedResults.length) {
          logger.error('Index '+ idx +' is outside of results length: ' + disk.queuedResults.length);
          notifications.alert(gettextCatalog.getString('Failed to upload result at index: ') + idx +'. '+gettextCatalog.getString('Index is out of range of queued results. Please upload your logs if the issue persists.'));
          return $q.reject();
        }

        r = disk.queuedResults[idx];

        // check network status, set
        if (!networkModel.status) {
          logger.info('Offline while trying to upload a result');
          notifications.alert(gettextCatalog.getString('The device is having difficulty communicating with the network'));
          return $q.reject();
        } else {
          r.testResults.network = networkModel.type;   // put network type for upload on the results structure
        }

        // only upload result if it was sourced from the right place
        if (_.includes(['tabsintServer', 'gitlab'], r.testResults.protocol.server)) {

          // tabsintServer
          if (r.testResults.protocol.server === 'tabsintServer') {
            logger.debug('Uploading result at index ' + idx + ' to tabsintServer with testDateTime: '+angular.toJson(r.testDateTime));
            tasks.register('upload results', 'Uploading result at index: ' + idx + ' to TabSINT Server');
            return tabsintServer.submitResults(r)
              .then(function() {
                results.updateSummary(r);
                results.delete(idx);
              }, function(){
                logger.error('Failure while uploading result: ' + angular.toJson(r)); // put the whole result in the log at this point
                notifications.alert('The device failed to upload a result to the TabSINT server. Please verify your TAbSINT server settings and upload your logs. Please upload your logs if the issue persists.');
                return $q.reject();
              })
              .finally(function() {
                tasks.deregister('upload results');
              });
          }

          // gitlab
          else if (r.testResults.protocol.server === 'gitlab') {
            var host, group, token, repositoryName;

            host = disk.servers.gitlab.host;                  // host is not yet configurable
            token = disk.servers.gitlab.token;                // token is not yet configurable

            // allow user to configure group and repository
            if (disk.gitlab.useSeparateResultsRepo) {
              group = disk.servers.gitlab.resultsGroup;
              repositoryName = disk.servers.gitlab.resultsRepo;
            } else {
              group = disk.servers.gitlab.namespace;
              repositoryName = 'results';
            }

            logger.debug(`Uploading result at index ${idx} to gitlab server with testDateTime: ${angular.toJson(r.testDateTime)}`);
            var toPath = `${r.testResults.protocol.name}/${results.filename(r, '.json')}`;
            tasks.register('upload results', `Uploading result at index: ${idx} to gitlab repository ${group}/${repositoryName}`);
            return gitlab.commit(host, group, repositoryName, token, r, toPath, `Pushing result from tablet ${devices.shortUUID} for protocol ${r.testResults.protocol.name}`)
              .then(function() {
                results.updateSummary(r);
                results.delete(idx);
              }, function(e) {
                logger.error('Failure while uploading result: ' + angular.toJson(r)); // put the whole result in the log at this point
                if (e && e.msg) {
                  notifications.alert(e.msg);
                } else {
                  logger.error(`Unknown failure while commiting gitlab result ${host} ${group} ${repositoryName} ${token} with error: ${angular.toJson(e)}`);
                  notifications.alert(gettextCatalog.getString('TabSINT encountered an issue while pushing result to Gitlab. Please verify the wifi connection and gitlab settings. Please upload your logs if the issue persists.'));
                }

                return $q.reject();
              })
              .finally(function() {
                tasks.deregister('upload results');
              });
          }
        } else {
          notifications.alert(gettextCatalog.getString('Stored result(s) cannot be uploaded.  The protocol used to generate these results was loaded locally from the tablet and is not affiliated with a server, so the results cannot be uploaded to the currently selected server, ') + disk.server + '. '+ gettextCatalog.getString('Please export the result(s) locally to save.'));
          logger.error('Trying to upload a result that has an unknown protocol server: ' + r.testResults.protocol.server);
        }
      };

      /**
       * Method to update the upload summary for uploadable results
       * @param  {object} r - result object
       */
      results.updateSummary = function(r) {
        var meta = {
          siteId : r.siteId,
          protocolName : r.protocolName,
          testDateTime : r.testDateTime,
          nResponses : r.nResponses,
          source: r.testResults.protocol.server,
          output: r.testResults.protocol.server,
          uploadedOn : (new Date()).toJSON()
        };
        var len = disk.uploadSummary.unshift(meta);
        logger.info('Result summary: ' + angular.toJson(meta));

        // remove the last element if the summary is beyond the prescribed length
        if (len > results.dm.uploadSummaryLength) {
          disk.uploadSummary.splice(-1, 1);
        }
      };

      results.exportCSV = function(result) {
        var base, filenameCSV, filenameFlatHeaderCSV, filenameBroadHeaderCSV;
        base = results.filename(result);
        filenameFlatHeaderCSV = base + '_FLAT_HEADER.csv';
        filenameBroadHeaderCSV = base + '_BROAD_HEADER.csv';
        filenameCSV = base + '.csv';

        var dir;
        if (disk.servers.localServer.resultsDir) {
          dir = disk.servers.localServer.resultsDir;
        } else {
          dir = 'tabsint-results';
        }

        // append the protocol name
        dir = dir + '/' + result.protocolName;

        return file.createDirRecursively(paths.public(''), dir)
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filenameCSV, csv.generateFlatCSV(result));
          })
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filenameFlatHeaderCSV, csv.generateFlatHeaderCSV(result));
          })
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filenameBroadHeaderCSV, csv.generateBroadHeaderCSV(result));
          });
      };

      /**
       * Method to export results to local file
       * @param {number} [idx] - index of result to export. If undefined, will export all
       * @return {promise} promise to export result
       */
      results.export = function(idx) {
        var r, filename;
        if (angular.isDefined(idx)) {
          r = disk.queuedResults[idx];
          filename = results.filename(r, '.json');
          logger.info('Exporting result at index ' + idx);
        } else {
          var q = $q.defer();
          var promise = q.promise;

          // export each result using this same method
          _.forEachRight(disk.queuedResults, function(r, idx) {
            promise = promise.then(function() {
              return results.export(idx) ;
            });
          });

          q.resolve();
          return promise;
        }

        //directory handling
        var dir;
        if (disk.servers.localServer.resultsDir) {
          dir = disk.servers.localServer.resultsDir;
        } else {
          dir = 'tabsint-results';
        }

        // append protocol name into path
        dir = dir + '/' + r.protocolName;

        return file.createDirRecursively(paths.public(''), dir)
          .then(function() {
            return $cordovaFile.writeFile(paths.public(dir), filename, angular.toJson(r));
          })
          .then(function() {
            logger.info('Successfully exported results to file: ' + filename);
            results.updateSummary(r);
            results.delete(idx);
            notifications.alert(gettextCatalog.getString('Successfully exported results to file: ') + filename + gettextCatalog.getString(' in directory: ') + dir);
          })
          .catch(function(e) {
            logger.error('Failed to export results to file with error: ' + angular.toJson(e));
            notifications.alert(gettextCatalog.getString('Failed to export results to file. Please file an issue at')+'https://github.com/creare-com/tabsint');
          });

      };



      /**
       * Delete a result from the queued Results
       * @param  {object} result object to delete
       */
      results.delete = function(idx) {
        try {
          var result = disk.queuedResults.splice(idx,1)[0];
          logger.info('Deleted result at testDateTime: ' + angular.toJson(result.testDateTime));
        } catch(e) {
          logger.error('Failed to delete result at index: ' + idx + ' with error: ' + angular.toJson(e));
          notifications.alert(gettextCatalog.getString('Failed to remove result. Please file an issue at') + 'https://github.com/creare-com/tabsint');
        }
      };

      /**
       * Delete all results from the queued Results
       */
      results.deleteAll = function() {
        logger.info('Deleting all queued results');
        disk.queuedResults = [];
      };

      /**
       * Delete all results from the queued Results
       */
      results.removeBackupResults = function() {
        logger.info('Deleting all backup results');
        return $cordovaFile.removeRecursively(paths.public(''), '.tabsint-results-backup')
          .then(function() {
            logger.info('Successfully deleted all backup result files');
          });
      };

      /**
       * Used by results-warnings component
       * @return {[type]} [description]
       */
      results.checkStoredResults = function(){
        var ageLimit = 1000*3600*24; // 1 day, in milliseconds
        var oldExamsStoredLocally = false;
        var now = Date.parse(new Date());
        for (var i=0; i<disk.queuedResults.length; i++) {
          var d = Date.parse(disk.queuedResults[i].testDateTime);
          if ((now-d) > ageLimit){
            oldExamsStoredLocally = true;
          }
        }
        return oldExamsStoredLocally;
      };





      return results;
    });

});
