/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.hardware', [])

    .factory('hardware', function(logger, pm, notifications, disk, devices, gettextCatalog){
      // compare headset and tablet of calibration in protocol with the headset on disk and device
      var hardware = {};

      hardware.checkHeadsetTabletCalib = function() {
        if (pm.root.headset) {
          if (pm.root.headset !== disk.headset) {
            notifications.alert(gettextCatalog.getString('Headset referenced in calibration')+' (' + pm.root.headset + ') '+gettextCatalog.getString('is different than the default saved headset')+' (' + disk.headset + '). '+gettextCatalog.getString('Audio may have been calibrated for a different headset.'));

          }
        }
        if (pm.root.tablet) {
          if (pm.root.tablet !== devices.model) {
            logger.warn('Tablet referenced in calibration (' + pm.root.tablet + ') is different the current tablet (' + devices.model + '). Audio may have been calibrated for a different tablet.');
          }
        }
      };

      return hardware;
    });

});