/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.subject-history', [])

    .factory('subjectHistory', function(app, file, tabsintServer, $q, disk, json, logger){
      var api = {
        load: undefined,
        history: {
          data: undefined,
          dateModified: undefined
        }
      };

      var targetPath = disk.uniqueProtocolName + '/subjecthistory.json';
      var nativeUrl;
      var refreshTime = 24*60*60*1000; // milliseconds in 24 hours

      api.load = function() {
        function getFile() {
          return file.ready()
              .then(function() {
                return file.getFile(targetPath);
              })
              .then(function(fileEntry) {
                logger.debug('Local subject history found');
                nativeUrl = fileEntry.nativeURL;
              }, function() {
                logger.debug('No local subjecthistory.json was found. Will attempt to download');
                return $q.reject();
              });
        }

        function getDateModified() {
          return file.ready()
              .then(function() { return file.getMetadata(targetPath); })
              .then(function(metadata) {
                api.history.dateModified = metadata.modificationTime;
                var currentDate = new Date();
                if ( (currentDate.getTime() - refreshTime) > api.history.dateModified) {
                  logger.debug('Subject history is out of date, attempting to download newer version');
                  return $q.reject();
                }
              }, function() {
                api.history.dateModified = undefined;
                return $q.reject();
              });
        }

        function loadLocal() {
          var deferred = $q.defer();
          api.history.data = json.load(nativeUrl);
          if (api.history.data) {
            deferred.resolve();
          } else {
            logger.error('Failed to load subjecthistory.json from local file');
            return $q.reject();
          }
        }

        if (app.tablet) {
          return getFile()   // will reject if no local file is found
            .then(getDateModified)   // will reject if out of date
            .then(loadLocal, function() {
              if (disk.server === 'tabsintServer') {
                return tabsintServer.downloadSubjectHistory()   // return true if no subject history needs to be downloaded
                  .then(getFile)
                  .then(loadLocal)
                  .catch(function() {logger.debug('No subject history available for download at this site'); });
                } else {
                  logger.debug('This server type does not support subject history');
                  return $q.reject();
                }
            })
            .catch(function() { logger.error('Subject history loading failed during');});
        } else { // browser and testing
          var deferred = $q.defer();

          api.history.data = json.load(disk.protocol.path +'/subject-history.json');
          if (api.history.data) {
            deferred.resolve();
            logger.info('Local subject history found');
          } else {
            logger.error('Failed to load subjecthistory.json from local file');
            deferred.resolve();
          }

          return deferred.promise;
        }


      };

      return api;

    });

});