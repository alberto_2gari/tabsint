/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.json', [])

    .factory('json', function (logger) {
      // Service to load json files synchronously

      var api = {
        load: undefined
      };

      // Load JSON data from file. Synchronous.
      api.load = function(path) {
        // input: path to json
        // return: json object
        var json = {};
        $.ajax({
          'async': false,
          'cache': false,
          'global': false,
          'url': path,
          'dataType': 'json'
        }).done(function (jsonData) {
          json = jsonData;
        }).fail(function() {      // for use in unit testing
          var newPath = 'base/www/'+ path;
          $.ajax({
            'async': false,
            'cache': false,
            'global': false,
            'url': newPath,
            'dataType': 'json'
          }).done(function (jsonData) {
            json = jsonData;
          }).fail(function () {
            logger.error('Failed to load json at path: ' + path);
            json = undefined;
          });
        });
        return json;
      };


      return api;
    });

});
