/* jshint bitwise: false */

/**
 * Created by bpf on 6/16/2017.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cha.media', [])
    .factory('chaMedia', function ($q, disk, logger, gitlab, tasks, notifications, $cordovaFile, paths, app, cha, file, checksum) {
      var api = {
        selected: undefined,
        model: {},
        select: undefined,
        mclass: undefined,
        syncRepoToCha: undefined,
        cancelMediaSync: undefined,
        addRepo: undefined,
        updateRepo: undefined,
        deleteRepo: undefined,
        isAdmin: false,
        syncActive: false
      };

      var protectedRepo = {
        group: 'cha-media',
        name: 'protected-media',
        host: 'https://gitlab.com/',
        token: '9VAqsxxrLTsTsZEmqMqw'
      };

      var fileListEqualOnCha, fileListDeleteFromCha, fileListTransferToCha;

      if (!disk.plugins.cha.mediaRepos) {
        disk.plugins.cha.mediaRepos = [];
      }

      api.select = function(m) {
        if (api.selected === m) {
          api.selected = undefined;
        } else {
          api.selected = m;
        }
      };

      api.mclass = function(m) {
        if (_.isEqual(api.selected, m)) {
          return 'table-selected';
        } else {
          return '';
        }
      };

      api.cancelMediaSync = function() {
        logger.debug('Cancelling Cha Media Repo Sync');
        tasks.register('cancel-media-sync','Cancelling CHA Media Sync.');
        cha.cancelFileOperation();
        api.syncActive = false;
        tasks.deregister('cancel-media-sync');
      };

      api.syncRepoToCha = function() {
        if (!api.selected || !api.selected.path) {
          var msg = 'Cannot sync repo to CHA.  No repo selected.';
          logger.warn(msg);
          notifications.alert(msg);
          return;
        }

        logger.info('Syncing CHA Media Repo ' + api.selected.group + ':' + api.selected.name + ' to headset with adminMode ' + api.selected.admin);
        tasks.register('sync-CHA-media-repo','Syncing CHA Media Repo.  Comparing Media on CHA with Selected Repository.  This comparison may take a few minutes.');
        api.syncActive = true;
        var repoPath = api.selected.path;
        var isAdmin = api.selected.admin;
        var transferList = [], equalDirList = [], equalFileList = [], deleteList = [], deleteDirList = [];

        /**
         * Recursively iterate through all files and folders in the repo path.
         * Compare to files and folders on cha.
         * Delete files and folders on cha that aren't in the repo path (Except the 'user' directory and any folders immediately in that directory.)
         * Create any directories that aren't yet on the cha.
         * Transfer any files in the repo path that aren't on the cha.
         */
        function recurse(currentPath) {
          console.log('--recursing.  current path: ' + currentPath);
          if (!api.syncActive) {
            logger.debug('CHA media sync breaking recursion chain because have been cancelled, probably by the user.');
            return $q.reject('Sync Cancelled.');
          }
          var chaDirectories = [], chaDirCrcs = [], chaCrcs = [];
          var tabletDirectories = [];
          var chaDirectoryName = currentPath.replace(repoPath,''); // remove the tablet portion of the nativeURL
          chaDirectoryName = chaDirectoryName.toUpperCase(); // transition to upper case so everything matches the cha directory structure
          if (!isAdmin) {
            chaDirectoryName = 'user/' + chaDirectoryName;
          }
          var user = isAdmin ? '' : 'USER/'; // prepend 'user' directory to all non-admin media repos so they don't touch the locked portion of the sd card
          console.log('--requestingcrc for path: ' + user + chaDirectoryName);

          return cha.getDirectoryCrc(chaDirectoryName) // get and save cha crcs in this directory
            .then(function(returnedObj) {
              // console.log('-- returnedCrcs: ' + JSON.stringify(returnedObj));
              if (returnedObj.success && returnedObj.result.length > 0) {
                chaCrcs = returnedObj.result.filter(function(entry){return angular.isDefined(entry.Attributes) && !(entry.Attributes & 16)});
                chaDirCrcs = returnedObj.result.filter(function(entry){return angular.isDefined(entry.Attributes) && (entry.Attributes & 16)});
                console.log('chaCrcs: ' + JSON.stringify(chaCrcs));
                console.log('chaDirCrcs: ' + JSON.stringify(chaDirCrcs));
              } else {
                chaCrcs = [];
                // make the directory
                return cha.makeDirectoryRoot(chaDirectoryName);
              }
            })
            // .then(function(){ // preparing for future where we want to delete unused directories...
            //   if (!isAdmin || chaDirectoryName.indexOf('user') >= 0) {
            //     return cha.getDirectory(chaDirectoryName)
            //       .then(function(returnedObj) {
            //         if (returnedObj.success && returnedObj.result.length > 0) {
            //           chaDirectories = returnedObj.result;
            //         } else {
            //           chaDirectories = [];
            //         }
            //       });
            //   }
            // })
            .then(function() { // get tablet-based media repo files in this directory
              return file.getEntries(currentPath);
            })
            .then(function(entries){
              var chain = $q.when();
              entries.forEach(function(entry) { // iterate over media entries
                chain = chain.then(function() {
                  var chaTarget = user + entry.nativeURL.replace(repoPath,''); // establish the cha version of this file location
                  if (entry.isDirectory) {
                    var dirCrc = checksum.crc32ofString(entry.name);
                    // console.log('directory: ' + entry.name + '. dirCrc = ' + dirCrc + '. chaTarget: ' + chaTarget);
                    tabletDirectories.push(entry.nativeURL); // save this for comparison in case we want to delete unused cha directories later
                    var tmpInd = chaDirCrcs.findIndex(function(chaDirCrc) { return chaDirCrc.Path === dirCrc});
                    // console.log('found: ' + tmpInd);
                    if (tmpInd >= 0) {
                      equalDirList.push(chaTarget); // for debugging
                      chaDirCrcs.splice(tmpInd,1); // remove from the chaDirectories list.  any remaining after this entries.forEach loop should be deleted.
                    } else {
                      // console.log('making directoryRoot with: ' + chaTarget);
                      return cha.makeDirectoryRoot(chaTarget); // it's not on the cha. create this directory - fine if it already exists.
                    }
                  } else if (entry.isFile) {
                    var fileSize, fileCrc; // need to use these - can only pass one variable via the promise chain
                    return checksum.crc32(entry.fullPath) // it's a file - get crc, compare with chaCrcs list
                      .then(function(crc) {
                        fileCrc = crc; // store for use later
                      })
                      .then(function() {
                        // get file size.  For transfer time calculations.
                        var deferred = $q.defer();  // return entry.file(function(theFile) fileSize=theFile.fileSize)  doesn't properly hold the promise chain until fileSize is defined.  Have to create a promise manually.
                        entry.file(
                          function(theFile){  fileSize = theFile.size;  deferred.resolve();  },
                          function(err){  fileSize = 0; deferred.resolve(); }  // something went wrong, but don't let this break functionality - a transfer time estimate is just a nice-to-have.
                        );
                        return deferred.promise;
                      })
                      .then(function() {
                        var tmpInd = chaCrcs.findIndex(function(chaCrc) { return chaCrc.Path === fileCrc});
                        if (tmpInd >= 0) {
                          equalFileList.push(chaTarget); // for debugging
                          chaCrcs.splice(tmpInd,1); // remove from the chaCrcs list.  any remaining after this entries.forEach loop should be deleted.
                        } else {
                          transferList.push({targetURL: chaTarget, nativeURL: entry.nativeURL, fileSize: fileSize}); // it's not on the cha - add to transfer list
                        }
                      });
                  }
                });
              });
              return chain;
            })
            .then(function(){
              // any remaining crcs should be deleted
              var userIndex = chaDirectoryName.indexOf('USER');
              var userString = 'USER/';
              if (userIndex >= 0 && userIndex >= chaDirectoryName.length - userString.length) {
                logger.debug('CHA media sync: not deleting - this is a user directory: ' + chaDirectoryName);
                // don't delete files here - they are in the main user directory - things like mediaver.txt and cha_prog.dat, plus users can have more than one media repo, and they'd all go in USER!
              } else {
                chaCrcs.forEach(function(chaCrc) { // all remaining chaCrcs did not match a file in the tablet-based repo.  delete these files
                    deleteList.push(chaDirectoryName + chaCrc.Path);
                });
                chaDirCrcs.forEach(function(chaDirCrc) {
                    deleteDirList.push(chaDirectoryName + chaDirCrc.Path);
                });
                /* We can't delete directories yet.
                chaDirectories.forEach(function(chaDir) { // all remaining chaDirectories did not match a dir in the tablet-based repo.  delete these directories
                  if (chaDir.Path) {
                    deleteDirList.push(chaDirectoryName + chaDir.Path);
                  }
                });
                */
              }
            })
            .then(function(){
              var chain = $q.when();
              tabletDirectories.forEach(function(recurseDir) {
                chain = chain.then(function() {
                  return recurse(recurseDir);
                });
              });
              return chain;
            });
        }

        return cha.abortExams()
          .then(function() {
            return recurse(repoPath);
          })
          .then(function() {
            logger.info('Cha Media Repo File Sync Ready for File Operations!');
            logger.info('Files to Delete from CHA: \n' + JSON.stringify(deleteList));
            logger.info('Directories to Delete from CHA: \n' + JSON.stringify(deleteDirList));
            logger.info('Files to transfer to CHA: \n' + JSON.stringify(transferList));
            logger.info('Files equivalent on CHA: \n' + JSON.stringify(equalFileList));
            logger.info('Directories equivalent on CHA: \n' + JSON.stringify(equalDirList));
          })
          .then(function() {
            tasks.register('sync-CHA-media-repo','Syncing CHA Media Repo.  Deleting unused files from CHA.');
            var deferred = $q.defer();

            // recursive
            function deleteNext() {
              if (!api.syncActive) {
                logger.debug('CHA media sync breaking recursion chain because have been cancelled, probably by the user.');
                deferred.reject('Sync Cancelled.');
                return;
              }
              if (deleteList.length > 0) {
                var fileToDelete = deleteList.shift(); // pull first - the cha returns crc's in order, so this may save a bunch of time finding each file to delete by crc
                cha.deleteFileCrc(fileToDelete)
                   .then(deleteNext);
              } else {
                deferred.resolve();
              }
            }

            deleteNext(); // start the recursion
            return deferred.promise;
          })
          .then(function() {
            console.log('-- FUTURE: Delete directories that are not part of the repo.  Exclude \'user\' and first children of \'user\'.');
            /* we can't delete directorires yet
            task.register('sync-CHA-media-repo','Syncing CHA Media Repo.  Deleting unused folders from CHA.');
            var chain = $q.when();
            deleteDirList.forEach(function(dirToDelete){
              chain = chain.then(function(){
                return cha.deleteDirectoryCrc(dirToDelete)
                  .then(function(){
                    console.log('-- FUTURE: Deleted directory'); // TODO - this may require sorting to delete children before parents...
                  });
              });
            });
            return chain.then(function() {
              console.log('All directories in the delete list have been removed from the cha.')
            });
            */
          })
          .then(function() {
            api.syncActive = false;
            return cha.transferFiles(transferList);
          })
          .then(function() {
            logger.log('Done syncing media repo to CHA.');
          })
          .catch(function(e){
            logger.log('CHA media sync stopped with reason: ' + JSON.stringify(e));
          })
          .finally(function(){
            tasks.deregister('sync-CHA-media-repo');
          });
      };

      api.addRepo = function() {
        var host, group, token, name, version, type = 'CHA media';
        if (api.isAdmin) {
          host = protectedRepo.host;
          group = protectedRepo.group;
          token = protectedRepo.token;
          name = api.model.name; // this could also be protectedRepo.name
          version = api.model.version;
        } else {
          host = disk.servers.gitlab.host;
          group = disk.servers.gitlab.namespace;
          token = disk.servers.gitlab.token;
          name = api.model.name;
          version = api.model.version;
        }

        gitlab.add(host, group, name, token, version, 'CHA media')
          .then(function(repo) {
            // define and store on disk
            repo = gitlab.defineProtocol(repo); // overwrites
            if (api.isAdmin) {
              repo.admin = true;
              api.isAdmin = false;
            }

            var dums = _.filter(disk.plugins.cha.mediaRepos, {name: repo.name, path: repo.path, date: repo.date});

            // if the media does not already exist, then push it onto the stack
            if (dums.length === 0) {
              disk.plugins.cha.mediaRepos.push(repo);
            } else {
              logger.error('Media meta data already in cha mediaRepos');
              notifications.alert('A matching version of the CHA media repo you just tried to add already exists in the stored CHA media repos!  See the Headset Media section to find it.');
            }

            // reset the temporary input
            api.model = {};
          });
      };

      api.updateRepo = function() {
        var deferred = $q.defer();
        if (!api.selected) {
          return;
        }

        notifications.confirm('Update CHA media repo ' + api.selected.name + '?',
          function(buttonIndex) {
            if (buttonIndex === 1){
              tasks.register('updating', 'Updating CHA Media');
              updateMedia()
                .finally(function() {
                  tasks.deregister('updating');
                  deferred.resolve();
                });
            } else {
              deferred.resolve();
            }
          });

        function updateMedia() {
          return gitlab.pull(api.selected.repo)
            .then(function(repo) {
              // update media on disk
              // Hack to keep track of the admin flag (for creare-managed media repo)
              var repoIsAdmin = api.selected.admin;
              var pidx = _.findIndex(disk.plugins.cha.mediaRepos, {path: paths.data(paths.gitlab(repo))});
              if (pidx !== -1) {
                var tmpRepo = gitlab.defineProtocol(repo);
                tmpRepo.admin = repoIsAdmin;
                disk.plugins.cha.mediaRepos[pidx] = tmpRepo; // overwrite
              } else {
                // can't update it - it doesn't exist
                logger.warn('Attempted to update CHA media repo ' + repo.name + ' but the repo does\'t exist');
              }
            });
        }

        return deferred.promise;
      };

      api.deleteRepo = function() {
        var deferred = $q.defer();
        if (!api.selected) {
          return;
        }

        notifications.confirm('Delete media repo ' + api.selected.name + ' and remove media files from disk?',
          function(buttonIndex) {
            if (buttonIndex === 1){
              deleteMediaRepo(api.selected);
              api.selected = undefined;
            }
          });

          function deleteMediaRepo(m) {
            var msg;
            if (!disk.plugins.cha.mediaRepos) {
              msg = 'Attempted to delete CHA media repo ' + m.name + ' but TabSINT currently has no CHA media repos stored on disk.';
              logger.error(msg);
              deferred.reject({code: 2201, msg: msg});
            }

            var idx = _.findIndex(disk.plugins.cha.mediaRepos, m);
            if (idx === -1) {
              msg = 'Trying to delete CHA media repo ' + m.name +', but it does not exist';
              logger.error(msg);
              deferred.reject({code: 2202, msg: msg});
            }

            if (app.tablet) {
              try {
                logger.debug('Removing media files for repo: ' + m.name + ' at path: ' + m.path);
                var root = m.path.split('/').slice(0,-2).join('/');
                var repoDir = m.path.split('/').slice(-2, -1)[0];
                $cordovaFile.removeRecursively(root, repoDir)
                  .then(function() {
                    // remove metadata from disk
                    disk.plugins.cha.mediaRepos.splice(idx, 1);
                    deferred.resolve();
                  })
                  .catch(function(e) {
                    msg = 'Failed to remove media files in directory ' + m.name + ' within root ' + m.path + ' for CHA media repo ' + m.name + ' with error: ' +angular.toJson(e);
                    logger.error(msg);
                    deferred.reject({code: 2203, msg: msg});
                  });
              } catch(e) {
                msg = 'Failed to remove media directory ' + m.name + ' from path ' + m.path + ' for reason: ' + JSON.stringify(e);
                logger.debug(msg);
                deferred.reject({code: 2204, msg: msg});
              }
            }

          }

        return deferred.promise;
      };

      if (window.tabsint) {
        window.tabsint.chaMedia = api;
      }

      return api;
    });
  });
