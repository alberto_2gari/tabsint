/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.noSleep', [])

    .factory('noSleep', function (app, cordova, logger) {
      var noSleep = {
        keepAwake: undefined,
        allowSleepAgain: undefined
      };

      noSleep.keepAwake = function () {
        if (app.tablet) {
          cordova.ready().then(function () {
            if (angular.isUndefined(window.plugins.insomnia)) {
              logger.error('noSleep plugin "insomnia" is undefined');
            } else {
              window.plugins.insomnia.keepAwake();
            }
          });
        } else {
          console.log('DEBUG: Nosleep in the browser');
        }

      };

      noSleep.allowSleepAgain = function () {
        if (app.tablet) {
          cordova.ready().then(function () {
            if (angular.isUndefined(window.plugins.insomnia)) {
              logger.error('noSleep plugin "insomnia" is undefined');
            } else {
              //logger.debug('calling insomnia.allowSleepAgain');
              window.plugins.insomnia.allowSleepAgain();
            }
          });
        } else {
          logger.debug('-- Nosleep in the browser');
        }
      };

      return noSleep;
    });
});
