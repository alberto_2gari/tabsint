/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global Notifications, confirm, alert */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.notifications', [])   

    .factory('notifications', function (app) {
      /*
      Displays an alert screen, with msg and title, and buttonLabels.length buttons.
      callBack takes buttonIndex and acts on user selection.  1-based count.

      If navigator.notification is not available, default confirm alert is displayed, with 'OK' tied to button1 and
      'Cancel' tied to button2
       */

      var notifications = {
        confirm: undefined,
        alert: undefined
      };

      notifications.confirm = function (msg, callBack, title, buttonLabels) {
        if (app.tablet) {
          app.ready()
            .then(function () {
              if (angular.isUndefined(Notifications)) {
                msg += ' Normal confirm notification not available.  Press "OK" to ' + buttonLabels[0] + ' or "Cancel" to ' + buttonLabels[1] + '.';
                if (confirm(msg)) {
                  callBack(1);
                } else {
                  callBack(2);
                }
              } else {
                Notifications.confirm(msg, callBack, title, buttonLabels);
              }
          });
        } else {
          if (confirm(msg)) {
            callBack(1);
          } else {
            callBack(2);
          }
        }
      };

      /*If navigator.notification is not available, default confirm alert is displayed, with 'OK' tied to button1 and
      'Cancel' tied to button2
      */
      notifications.alert = function (msg, callBack, title, buttonLabels) {
        if (app.tablet) {
          app.ready().then( function() {
            if (angular.isUndefined(Notifications)) {
              alert(msg, callBack, title, buttonLabels);
            } else {
              Notifications.alert(msg, callBack, title, buttonLabels);
            }
          });
        } else {
          if (app.test) {   // suppress alerts during testing
            console.log('-- ALERT: ' + msg);   
          } else {
            alert(msg, callBack, title, buttonLabels);
          }
          
        }
      };

      return notifications;
    });
});