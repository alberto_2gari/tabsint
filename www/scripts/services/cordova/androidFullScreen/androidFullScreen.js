/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global AndroidFullScreen */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.androidFullScreen', [])   

    .factory('androidFullScreen', function (app, devices, cordova, logger) {
      /* Wrapper for TabSINTNative to filter ios calls

       Newer cordova versions seem to grab the plugin despite the plugin.xml
       lacking a platform=ios section */

      // set up immersive for android

      return {
        immersiveMode: function () {
          if (app.tablet) {  // this should only run on the tablet
            cordova.ready()
              .then(function () {
                if (devices.platform.toLowerCase() === 'android') {
                  if (angular.isDefined(AndroidFullScreen)) {
                    AndroidFullScreen.immersiveMode(function(){}, function(e) {
                      logger.error('Failed to set android into immerive mode with error: ' + angular.toJson(e));
                    });
                  } else {
                    logger.error('"AndroidFullScreen" is undefined. Cordova probably not loaded.');
                  }
                } else if (devices.platform.toLowerCase() === 'ios') {
                  // iOS - do nothing
                } else {
                  logger.error('AndroidFullScreen failed to load. devices.platform: '+devices.platform);
                }
              });
          } else {
            // browser testing will do nothing
          }
        }
      };
    });
});