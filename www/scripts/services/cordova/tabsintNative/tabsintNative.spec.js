define(['test-dep','app'], function () {
  'use strict';

  beforeEach(module('tabsint'));

  // beforeEach(module('tabsintNative'));

  describe('tabsintNative wrapper tests', function () {
    var tabsintNative;

    beforeEach(
      inject(function ($injector, _tabsintNative_){
        tabsintNative = _tabsintNative_;
      })
    );

    it("should define 'onVolumeError'", function() {
      expect( tabsintNative.onVolumeError ).toBeDefined();
    });

    it("should provide a 'onVolumeError' method", function() {
      expect( tabsintNative.onVolumeError ).toEqual(jasmine.any(Function));
    });

  });
});
