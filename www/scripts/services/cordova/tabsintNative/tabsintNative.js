/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global TabSINTNative */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.tabsintNative', [])

    .factory('tabsintNative', function (app, devices, cordova, logger, notifications, disk, gettextCatalog) {
      /* Wrapper for TabSINTNative to filter ios calls

       Newer cordova versions seem to grab the plugin despite the plugin.xml
       lacking a platform=ios section */

       var tabsintNative = {
         initialize: undefined,
         resetAudio: undefined,
         getAudioVolume: undefined,
         sendExternalData: undefined,
         getExternalData: undefined,
         setExternalDataHandler: undefined,
         onVolumeError: undefined
       };

       tabsintNative.initialize = function(successCallback, errorCallback) {
         cordova.ready()
                .then(function() {
                  if (devices.platform.toLowerCase() === 'android') {
                    TabSINTNative.initialize(successCallback, errorCallback);
                  } else if (devices.platform.toLowerCase() === 'ios') {
                    // iOS - do nothing
                  } else if (devices.platform.toLowerCase() === 'browser') {
                    // browser - do nothing
                  } else {
                    notifications.alert(gettextCatalog.getString('ERROR: TabSINT Native Failed to Initialize'));
                    logger.error('TabSINT Native failed to initialize. devices.platform: '+devices.platform);
                  }
                });
       };

       tabsintNative.resetAudio = function(successCallback, errorCallback) {
         cordova.ready()
                .then(function() {
                  if (devices.platform.toLowerCase() === 'android') {
                    // override the resetAudio command is we have disabled automatic volume control
                    if (disk.disableVolume) {
                      if (successCallback && typeof(successCallback) === 'function') { successCallback(); }  // call success callback
                    } else {
                      logger.debug("Reset audio.");
                      TabSINTNative.resetAudio(successCallback, errorCallback);
                    }
                  } else if (devices.platform.toLowerCase() === 'ios') {
                    // iOS - do nothing
                  } else if (devices.platform.toLowerCase() === 'browser') {
                    // browser - do nothing
                  } else {
                    notifications.alert(gettextCatalog.getString('ERROR: TabSINT Native Failed to Reset Audio'));
                    logger.error('TabSINT Native failed to Reset Audio. devices.platform: '+devices.platform);
                  }
                });
       };

       tabsintNative.getAudioVolume = function(successCallback, errorCallback) {
         cordova.ready()
                .then(function() {
                  if (devices.platform.toLowerCase() === 'android') {
                    TabSINTNative.getAudioVolume(successCallback, errorCallback);
                  } else if (devices.platform.toLowerCase() === 'ios') {
                    // iOS - do nothing
                  } else if (devices.platform.toLowerCase() === 'browser') {
                    // browser - do nothing
                  } else {
                    notifications.alert(gettextCatalog.getString('ERROR: TabSINT Native Failed to Get Audio Volume'));
                    logger.error('TabSINT Native failed to getAudioVolume. devices.platform: '+devices.platform);
                  }
                });
       };

       tabsintNative.sendExternalData = function(successCallback, errorCallback, externalAppName, data) {
         cordova.ready()
                .then(function() {
                  if (devices.platform.toLowerCase() === 'android') {
                    var dataString = JSON.stringify(data);
                    TabSINTNative.sendExternalData(successCallback, errorCallback, externalAppName, dataString);
                  } else if (devices.platform.toLowerCase() === 'ios') {
                    // iOS - do nothing
                  } else if (devices.platform.toLowerCase() === 'browser') {
                    // browser - do nothing
                  } else {
                    notifications.alert(gettextCatalog.getString('ERROR: TabSINT Native Failed to send external data.'));
                    logger.error('TabSINT Native failed to send external data. devices.platform: '+devices.platform);
                  }
                });
       };

       tabsintNative.getExternalData = function(successCallback, errorCallback) {
         // This will probably never be needed - getExternalDataIntent is only required if the app will be started from external data.
         // The app is triggered by external data, and the listener is not established yet, so have to use getExternalDataIntent to
         // get that first message...
         cordova.ready()
                .then(function() {
                  function handleData(d) {
                    try {
                      var dataObj = JSON.parse(d.data);
                      successCallback(dataObj);
                    } catch (e) {
                      logger.error('Could not parse incoming data object.  Error: ' + JSON.stringify(e) + '.  The incoming data object should be a JSON.stringified object.  Received: ' + d);
                    }
                  }

                  if (devices.platform.toLowerCase() === 'android') {
                    TabSINTNative.getExternalDataIntent(handleData, errorCallback);
                  } else if (devices.platform.toLowerCase() === 'ios') {
                    // iOS - do nothing
                  } else if (devices.platform.toLowerCase() === 'browser') {
                    // browser - do nothing
                  } else {
                    notifications.alert(gettextCatalog.getString('ERROR: TabSINT Native Failed to Get External Data.'));
                    logger.error('TabSINT Native failed to getExternalData. devices.platform: '+devices.platform);
                  }
                });
       };

       tabsintNative.setExternalDataHandler = function(successCallback, errorCallback) {
         cordova.ready()
                .then(function() {
                  function handleData(d) {
                    if (d.type && d.type === 'text/plain' && d.data) {
                      try {
                        var dataObj = JSON.parse(d.data);
                        successCallback(dataObj);
                      } catch (e) {
                        logger.error('Could not parse incoming data object.  Error: ' + JSON.stringify(e) + '.  The incoming data object should be a JSON.stringified object.  Received: ' + JSON.stringify(d));
                      }
                    }
                  }
                  if (devices.platform.toLowerCase() === 'android') {
                    TabSINTNative.setExternalDataIntentHandler(handleData, errorCallback);
                  } else if (devices.platform.toLowerCase() === 'ios') {
                    // iOS - do nothing
                  } else if (devices.platform.toLowerCase() === 'browser') {
                    // browser - do nothing
                  } else {
                    notifications.alert(gettextCatalog.getString('ERROR: TabSINT Native Failed to set external data handler.'));
                    logger.error('TabSINT Native failed to setExternalDataHandler. devices.platform: '+devices.platform);
                  }
                });
       };

       tabsintNative.onVolumeError = function(e){       //for internal use
         if (e.code === 99) {
           logger.error('Failed to set volume while running tabsintNative.resetAudio() with error: ' + angular.toJson(e));
           logger.info('Volume error. Volume set to: ' + e.volume);
           var msg = gettextCatalog.getString("TabSINT was unable to set the volume to 100%. This is required for playing calibrated audio through the tablet.")+'\n\n' +
             gettextCatalog.getString("Please set the volume to 100% manually to avoid seeing this error again.")+"\n\n" +
             gettextCatalog.getString('See the documentation for more information.');
           notifications.alert(msg);
         }
       };

      return tabsintNative;
    });
});
