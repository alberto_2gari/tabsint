/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

 /*global SLM */

 define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.slm', [])

  .factory('slm', function ($timeout, tabsintNative, logger, notifications, hardware, disk, $q) {

    var slm = {};
    
    /**
     * Recording API point. 
     * Will be set true when the SLM is recording
     * @type {Boolean}
     */
    slm.recording = false;

    /**
     * Init method for each page
     * @param  {object} page - page object from the protocol page
     */
    slm.init = function(page) {
      if (!installed()) {
        logger.error('Attempting to start slm on page, but SLM cordova plugin is not available');
        notifications.alert('This page is requesting the sound level meter, but the SLM plugin is not included in this build of TabSINT. Please hand the device back to an Administrator.');
        return;
      }

      if (page.slm.microphone && page.slm.microphone !== 'internal') {
        if (page.slm.microphone === 'dayton') {

          SLM.initDaytonAudioMic(function() {
            logger.debug('Successfully init the dayton mic');
            record();
          }, function(e) {
            logger.error(`Failed to init dayton mic with error: ${JSON.stringify(e)}`);
            notifications.alert('This page is requesting the sound level meter using the Dayton microphone, but TabSINT failed to initialize the microphone. Please hand the device back to an Administrator.');
          });

        } else if (page.slm.microphone === 'studio6') {

          SLM.initUSBprecisionMicStudioSixDigital(function() {
            logger.debug('Successfully init the studio6 mic');
            record();
          }, function(e) {
            logger.error(`Failed to init studio6 mic with error: ${JSON.stringify(e)}`);
            notifications.alert('This page is requesting the sound level meter using the Studio 6 microphone, but TabSINT failed to initialize the microphone. Please hand the device back to an Administrator.');
          });

        }
      } else {

        SLM.initInternalMic(function() {
          logger.debug('Successfully init the internal mic');
          record();
        }, function(e) {
          logger.error('Failed to init internal mic');
          notifications.alert('This page is requesting the sound level meter using the internal microphone, but TabSINT failed to initialize the microphone. Please hand the device back to an Administrator.');
        });

      }
    };

    /**
     * Stop recording
     * @return {promise} 
     */
    slm.stop = function() {
      if (!installed()) { return $q.reject(); }

      var q = $q.defer();

      SLM.stop(function() {
        logger.info('Successfully stopped recording with SLM');
        slm.recording = false;
        q.resolve();
      }, function() {
        logger.error('Failed to stop recording with SLM');
        q.reject({msg: 'TabSINT encountered an issue while trying to stop the sound level meter recording. Please hand the device to an Administrator'});
      });

      return q.promise;
    };


    /**
     * Return parameters from sound level meter for a given sound
     * @param  {object} page - page object defining slm parameters
     * @return {promise}     - promise that resolves with the result object
     */
    slm.getSLM = function(page) {
      if (!installed()) { return $q.reject(); }

      var params = page.slm.parameters || ['recordingStartTime', 'recordingDuration', 'numberOfReports', 'timePoints', 'SPL_A_mean'];
      var presentationId = page.id;
      var result = {
        presentationId: page.id
      };

      return slm.stop()
        .then(function() { return getParams(params, result); })
        .catch(function(e) {
          if (e && e.msg) {
            notifications.alert(e.msg);
          }
        })
        .finally(function() {
          // results will pass through close
          slm.close();
        });

    };

    /**
     * Close an open slm connection
     */
    slm.close = function() {
      if (!installed()) { return; }

      SLM.close(function() {
        logger.info('Successfully closed SLM connection');
        slm.recording = false;
      }, function(e) {
        logger.error('Failed to close SLM connection properly');
      });
    };


    /**
     * Convience function to check if SLM is installed
     * @return {boolean} 
     */
    function installed() {
      return (typeof(SLM) !== 'undefined');
    }

    /**
     * Start recording with the previously init SLM
     */
    function record() {
      SLM.record(function() {
        logger.info('Successfully started recording with SLM');
        slm.recording = true;
      }, function() {
        logger.error('Failed to start recording with SLM');
        slm.recording = false;
      });
    }



    /**
     * Get parameters from SLM
     * @param  {object} params - parameters to get
     * @param  {object} result - result object to append to
     * @return {promise} - promise that resolves with object containing all the parameters
     */
    function getParams(params, result) {
      var promises = [];

      _.forEach(params, function(param) {
        if (typeof(SLM[param]) === 'function') {
          var sq = $q.defer();
          SLM[param](function(values) {
            result[param] = values;
            sq.resolve();
          }, function(e) {
            logger.error(`Failed to get slm parameter ${param} with error ${JSON.stringify(e)}`);
            result[param] = null;
            sq.resolve();
          });

          promises.push(sq.promise);
        }
      });

      // wait for all the promises to resolve, then return the result
      return $q.all(promises)
        .then(function() {
          return result;
        });
    }

    return slm;

  });
});
