/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global navigator, Media, LocalFileSystem */

define(['angular',
  './file/file',
  './network/network',
  './media/media',
  './devices/devices',
  './splashscreen/splashscreen',
  './androidFullScreen/androidFullScreen',
  './tabsintNative/tabsintNative',
  './notifications/notifications',
  './tabletLocation/tabletLocation',
  './noSleep/noSleep',
  './sqLite/sqLite',
  './bluetoothStatus/bluetoothStatus',
  './slm/slm',  
  ], function (angular) {
  'use strict';

  /* Angular wrappers for Cordova services.

   Purpose: To make it easier to mock/test these services, and
   deal with queueing functions at startup before deviceReady().

   */

  angular.module('tabsint.services.cordova', [
    'tabsint.services.cordova.file',
    'tabsint.services.cordova.network',
    'tabsint.services.cordova.media',
    'tabsint.services.cordova.devices',
    'tabsint.services.cordova.splashscreen',
    'tabsint.services.cordova.androidFullScreen',
    'tabsint.services.cordova.tabsintNative',
    'tabsint.services.cordova.notifications',
    'tabsint.services.cordova.tabletLocation',
    'tabsint.services.cordova.noSleep',
    'tabsint.services.cordova.sqLite',
    'tabsint.services.cordova.bluetoothStatus',
    'tabsint.services.cordova.slm'])

    .factory('cordova', function ($q, app) {

      var cordova = {};

      /**
       * Empty object to hold the window variable `window.cordova.plugins`
       * @type {Object}
       */
      cordova.plugins = {};


      // deferred promise to the "deviceready" event
      var deferred = $q.defer();
      
      /**
       * Returns the promise to the "deviceready" event
       * @return {promise} - promise to resolve with "deviceready" fires
       */
      cordova.ready = function() {
        return deferred.promise;
      };

      /**
       * Function to run when the device fires the "deviceready" event
       */
      function onDeviceReady() {
        console.log('-- Cordova ready');

        // disable back button on device
        document.addEventListener("backbutton", function (e) {
          e.preventDefault();
        }, false );

        deferred.resolve();
      }

      /**
       * Methods to run on load
       */
      //only run on tablet
      if (app.tablet) {
        document.addEventListener("deviceready", onDeviceReady, false);
        cordova.plugins = window.cordova.plugins;
      } else {  // Browser testing
        deferred.resolve();
      }


      return cordova;
    });

});
