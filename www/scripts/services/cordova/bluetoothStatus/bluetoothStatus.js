/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.bluetoothStatus', [])   

    .factory('bluetoothStatus', function (app, cordova, logger) {
      var api = {
        init: undefined,
        update: undefined,
        state: undefined,
        type: undefined
      };



      api.update = function() {
        if (app.tablet) {
          api.state = cordova.plugins.BluetoothStatus.BTenabled;
        } else {
          api.state = true;
        }

        if (api.state) {
          logger.info('Bluetooth enabled on the device');
        } else{
          logger.info('Bluetooth disabled on the device');
        }
        
      };

      // user cordova-plugin-bluetooth-status
      api.init = function() {
        if (app.tablet) {

          // add event listeners
          window.addEventListener('BluetoothStatus.enabled', function() {
            api.update();
          });

          window.addEventListener('BluetoothStatus.disabled', function() {
            api.update();
          });

          console.log('-- Initiating cordova bluetooth plugin on the tablet');
          cordova.ready()
            .then(cordova.plugins.BluetoothStatus.initPlugin)
            .catch(function(err) {
              logger.error('Bluetooth service failed while trying to set the initiate');
              api.state = undefined;
            });
        } else {
          cordova.ready()
            .then(function() {
              console.log('-- Initiating mock bluetooth plugin here');
              api.state = true;
            });
          
        }
      };

      api.init();

      return api;
    });
});