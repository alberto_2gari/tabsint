/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global alert */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cordova.sqLite', [])   

    .factory('sqLite', function (app, cordova, $q) {
      var sqLite = {
        ready: undefined,
        open: undefined,
        drop: undefined,
        store: undefined,
        prepareForUpload: undefined,
        getLogs: undefined,
        get: undefined,
        count: undefined,
        numLogs: {
          logs: undefined,
          results: undefined
        },
        delete: undefined
      };
      var db;
      var uploadStartIndex, uploadStopIndex;

      sqLite.open = function(){
        var deferred = $q.defer();
        
        // define ready by this promise
        sqLite.ready = function() {
          return deferred.promise;
        };
        
        if (app.tablet) {  // Tablet
          cordova.ready()
              .then(function() {
                if (angular.isUndefined(window.sqlitePlugin)) {
                  alert('Cordova plugin "sqlitePlugin" is undefined');
                  deferred.reject('WARNING: Sql plugin is undefined');
                }
              })
              .then(function() {
                if (db === angular.undefined) {
                  db = window.sqlitePlugin.openDatabase({name: 'test.db', location: 1},
                      function() {
                        sqLite.numLogs.logs = 0;
                        console.log('-- Log Database ready.');
                        deferred.resolve();
                      },
                      function(e) {deferred.reject(e)}
                  );
                } else {
                  console.log('-- DB already defined');
                  deferred.resolve();
                }
              });
        } else {  // Browser
          db = {
            newlogs: [],
            results: [],
            ids: {newlogs: 0, results: 0}
          };
          console.log('-- Browser logs ready');
          deferred.resolve();
        }

        return deferred.promise;
      };

      // run open at launch
      sqLite.open();

      sqLite.drop = function(tableName){
        var deferred = $q.defer();

        if (app.tablet) { // tablet
          sqLite.ready()
            .then(function() {
              db.transaction(function (tx) {
                if (tableName === 'newlogs') {
                  tx.executeSql('DROP TABLE IF EXISTS newlogs', [],
                    function () {
                      sqLite.numLogs.logs = 0;
                      deferred.resolve();
                    },
                    function (e) { deferred.reject(e); }
                  );
                }
              });
            });
        } else { // browser
          if (db && db.hasOwnProperty(tableName)) {
            db[tableName] = [];
            db.ids[tableName] = 0;
          }
          deferred.resolve();
        }

        return deferred.promise;
      };

      sqLite.store = function (tableName, date, type, logMsg, param) {
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              if (tableName === 'results') {
                // future results structure
              } else if (tableName === 'newlogs') {
                db.transaction(function (tx) {
                  tx.executeSql('CREATE TABLE IF NOT EXISTS newlogs (msgID INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, data TEXT, type TEXT, uuid TEXT, siteID TEXT, build TEXT, version TEXT, platform TEXT, model TEXT, os TEXT, other TEXT)');
                  tx.executeSql('INSERT INTO newlogs (date, data, type, uuid, siteID, build, version, platform, model, os, other) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [date, logMsg, type, param.uuid, param.siteId, param.build, param.version, param.platform, param.model, param.os, param.other], null, null);
                  sqLite.numLogs.logs +=1;
                  deferred.resolve();
                });
              } else {
                deferred.reject('ERROR: Unknown table name');
              }
            });
        } else {
          if (db.hasOwnProperty(tableName)) {
            if (db[tableName] === angular.undefined){db[tableName] = [];}

            db[tableName].push({
              msgID: db.ids[tableName]++,
              type: type,
              date: date,
              data: logMsg,
              uuid: param.uuid,
              siteID: param.siteID,
              build: param.build,
              version: param.version,
              model: param.model,
              os: param.os,
              other: param.other
            });
            sqLite.numLogs.logs +=1;
            deferred.resolve();
          } else {
            deferred.reject('ERROR: Unknown table name');
          }
        }

        return deferred.promise;
      };

      sqLite.prepareForUpload = function(){
        var deferred = $q.defer();

        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM newlogs ORDER BY msgID ASC LIMIT 1;', [], function (tx, res) {
                  uploadStartIndex = res.rows.item(0).msgID;
                  tx.executeSql('SELECT * FROM newlogs ORDER BY msgID DESC LIMIT 1;', [], function (tx, res) {
                    uploadStopIndex = res.rows.item(0).msgID;
                    console.log('-- startInd: '+uploadStartIndex+', stopInd: '+uploadStopIndex);
                    deferred.resolve();
                  }, function (e) { deferred.reject(e);
                  });
                }, function (e) { deferred.reject(e);
                });
              });
            });
        } else {
          if (db && db.newlogs === angular.undefined){ db.newlogs = []; }

          uploadStopIndex = 0;
          db.newlogs.forEach(function(item){
            uploadStopIndex = Math.max(uploadStopIndex,item.msgID);
          });
          uploadStartIndex = uploadStopIndex;
          db.newlogs.forEach(function(item){
            uploadStartIndex = Math.min(uploadStartIndex,item.msgID);
          });
          console.log('-- startInd: '+uploadStartIndex+', stopInd: '+uploadStopIndex);
          deferred.resolve();
        }

        return deferred.promise;
      };

      sqLite.getLogs = function(){
        var deferred = $q.defer();

        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM newlogs WHERE msgID >= ? AND msgID <= ? ORDER BY msgID ASC LIMIT 50;', [uploadStartIndex, uploadStopIndex],
                  function (tx, res) {
                    var ret = [];
                    for (var i = 0; i < res.rows.length; i++) {
                      ret.push(res.rows.item(i));
                    }
                    deferred.resolve(ret);
                  },
                  function (e) { deferred.reject(e); }
                );
              });
            });
        } else {
          if (db && db.newlogs === angular.undefined){db.newlogs = [];}
          var tmp = [];
          db.newlogs.forEach(function(item){
            if (item.msgID >= uploadStartIndex && item.msgID <= uploadStopIndex){
              if (tmp.length < 20){
                tmp.push(item);
              }
            }
          });
          deferred.resolve(tmp);
        }
        return deferred.promise;
      };

      sqLite.get = function (tableName) {
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                if (tableName === 'results') {
                  // get results
                } else if (tableName === 'newlogs') {
                  tx.executeSql('SELECT * FROM newlogs;', [],
                    function (tx, res) {
                      //window.tmp = res;
                      //console.log(res);
                      var retLogs = [];
                      for (var i = 0; i < res.rows.length; i++) {
                        retLogs.push(res.rows.item(i));
                      }
                      //console.log(retLogs);
                      deferred.resolve(retLogs);
                    },
                    function (e) { deferred.reject(e); }
                  );
                } else {
                  deferred.reject('ERROR: Unknown table name');
                }
              });
            });
        } else {
          if (db && db[tableName] === angular.undefined){db[tableName] = [];}
          var tmp = db[tableName];
          deferred.resolve(tmp);
        }

        return deferred.promise;
      };

      sqLite.count = function(tableName){
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                if (tableName === 'newlogs') {
                  tx.executeSql('SELECT COUNT(*) AS c FROM newlogs', [],
                    function (tx, res) {
                      console.log('-- Log count: ' + res.rows.item(0).c);
                      sqLite.numLogs.logs = res.rows.item(0).c;
                      deferred.resolve(res.rows.item(0).c);
                    },
                    function (e) { deferred.reject(e); }
                  );
                } else {
                  deferred.reject('ERROR: Unknown table');
                }
              });
            });
        } else {
          if (db && db[tableName] === angular.undefined){db[tableName] = [];}
          console.log('-- Log count: '+db[tableName].length);
          sqLite.numLogs.logs = db[tableName].length;
          deferred.resolve(db[tableName].length);
        }

        return deferred.promise;
      };

      sqLite.delete = function (tableName, id) {
        var deferred = $q.defer();
        if (app.tablet) {
          sqLite.ready()
            .then(function () {
              db.transaction(function (tx) {
                if (tableName === 'results') {
                  // delete results rows
                } else if (tableName === 'newlogs') {
                  tx.executeSql('DELETE FROM newlogs WHERE msgID = ?', [id],
                    function () {
                      sqLite.numLogs.logs -= 1;
                      deferred.resolve();
                    },
                    function (e) { deferred.reject(e); }
                  );
                } else {
                  deferred.reject('ERROR: Unknown table');
                }
              });
          });
        } else {
          if (db && db[tableName] === angular.undefined){ db[tableName] = []; }
          var rmID;
          for (var i = 0; i < db[tableName].length; i++){
            var item = db[tableName][i];
            if (item.msgID === id){
              rmID = i;
            }
          }
          if (rmID !== angular.undefined){
            db[tableName].splice(rmID,1);
            sqLite.numLogs.logs -= 1;
          }
          deferred.resolve();
        }

        return deferred.promise;
      };

      return sqLite;
    });
});