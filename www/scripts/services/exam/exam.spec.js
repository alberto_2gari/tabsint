/**
 * Created by rxc on 12/5/13.
 */
/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['tv4','test-dep', 'app'], function (tv4) {
  'use strict';

  beforeEach(module('tabsint'));

  describe('ExamLogic', function () {

    var $rootScope, examLogic, page, results, disk, protocol, timeout, notifications, json, pm;

    beforeEach(module(function ($provide) {
      $provide.value('authorize', {
        modalAuthorize: function (targetFn) {
          targetFn();
        } // assume authorized.
      });
    }));
    beforeEach(module(function ($provide) {
      $provide.value('chooseCha', {
        discover: function () {
          console.log('Would have discovered CHA here.');
        } // assume authorized.
      });
    }));
    beforeEach(inject(function ($injector, _$rootScope_, _examLogic_, _page_, _results_, _disk_,_protocol_, _notifications_,  _json_, _pm_, _$httpBackend_) {
      examLogic = _examLogic_;
      page = _page_;
      results = _results_;
      pm = _pm_;
      disk = _disk_;
      protocol = _protocol_;
      timeout = $injector.get('$timeout');
      notifications = _notifications_;
      json = _json_;
      $rootScope = _$rootScope_;
      _$httpBackend_.whenGET('res/translations/translations.json')
        .respond(200, 'a string');
    }));

    beforeEach(function() {
      $rootScope.$digest();
      protocol.override('res/protocol/mock', false, false)
        .then(examLogic.act.reset);
      $rootScope.$digest();
    });

    function clearResults(){
      delete disk.queuedResults;
      disk.queuedResults = [];
    }

    function flushSubmitTimeout(){
      timeout.flush(5001);
    }

    function flushActivateTimeout(){
      timeout.flush(101);
    }

    it('should have some expected fields', function () {
      expect(page.dm).toBeDefined();
      //expect(page.result).toBeDefined(); // intentionally undefined in examLogic.js during act.reset!
      expect(examLogic.act).toBeDefined();
      expect(examLogic.testing).toBeDefined();
    });

    it('should be READY.', function () {
      expect(examLogic.dm.state.mode).toEqual('READY');
    });

    it('should have filled up its anticipated protocols queue.', function () {
      expect(examLogic.dm.state.progress.anticipatedProtocols.length).toBeGreaterThan(0);
    });

    it('should have 0 pct progress', function () {
      expect(examLogic.dm.state.progress.pctProgress).toEqual(0);
    });

    describe('goToIdx', function () {
      it('should fall back on global properties', function () {
        examLogic.testing.goToId('ref1'); // go to sub-protocol
        flushActivateTimeout();
        examLogic.testing.goToId('mrt003');
        flushActivateTimeout();
        console.log(angular.toJson(page.dm));
        expect(page.dm.helpText).toEqual(pm.root.helpText);
      });

      it('should initialize a result.', function () {
        examLogic.testing.goToId('multichoice001');
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual(pm.root.pages[1].id);
      });
    });

    describe('begin', function () {
      it('should switch mode.', function () {
        examLogic.act.begin();
        flushSubmitTimeout();
        expect(examLogic.dm.state.mode).toEqual('TESTING');
      });

      it('should initialize results', function () {
        examLogic.act.begin();
        flushSubmitTimeout();
        expect(results.current).toBeDefined();
        expect(results.current.testResults.responses.length).toEqual(0);
      });

      it('should go to the first page', function () {
        examLogic.act.begin();
        flushSubmitTimeout();
        expect(page.dm.id).toEqual(pm.root.pages[0].id);
      });
    });

    it('should initially have undefined submit logic', function () {
      expect(page.dm.isSubmittable).toBeUndefined();
    });

    describe('submission', function () {
      beforeEach(function () {
        examLogic.act.begin();
        flushSubmitTimeout();
        examLogic.testing.goToId('ref1');
        flushActivateTimeout();
        //examLogic.testing.goToId('mrt001');
      });
      it('should not let us submit mrt initially', function () {
        expect(page.dm.isSubmittable()).toBeFalsy();
      });
      //      it('should let us submit after we choose', function () {
      //        examLogic.testing.chooseCorrect();
      //        expect(page.dm.isSubmittable()).toBeTruthy();
      //        examLogic.testing.chooseIncorrect();
      //        expect(page.dm.isSubmittable()).toBeTruthy();
      //      });
      //      it('should throw an error if there is no response.', function () {
      //        spyOn(window, 'alert');
      //        examLogic.act.submit();
      //        expect(notifications.alert).toHaveBeenCalled();
      //      });


      describe('After a correct MRT submission', function () {
        var responses, lastResponse;

        beforeEach(function () {
          examLogic.testing.chooseCorrect();
          flushSubmitTimeout();
          flushActivateTimeout();
          spyOn(notifications, 'alert');
          //examLogic.act.submit();
          responses = results.current.testResults.responses;
          lastResponse = responses[responses.length - 1];
        });
        it('should now have pct progress > 0.', function () {
          expect(examLogic.dm.state.progress.pctProgress).toBeGreaterThan(0);
        });
        it('responses should not be empty.', function () {
          expect(responses.length).toBeGreaterThan(0);
        });
        it('should not throw an alert', function () {
          expect(notifications.alert).not.toHaveBeenCalled();
        });
        it('should have a responseStartTime.', function () {
          expect(lastResponse.responseStartTime.toJSON().length).toBeGreaterThan(10);
        });
        it('should have a responseElapTimeMS.', function () {
          expect(lastResponse.responseElapTimeMS).toEqual(jasmine.any(Number));
        });
        it('should be marked correct.', function () {
          expect(lastResponse.correct).toBeTruthy();
        });
        it('should have a presentationId and response defined.', function () {
          expect(lastResponse.presentationId.length).toBeGreaterThan(0);
          expect(lastResponse.response.length).toBeGreaterThan(0);
        });
        it('should be on a different page.', function () {
          expect(page.dm.id).not.toEqual('mrt001');
        });
      });

      describe('After an incorrect MRT submission', function () {
        var responses, lastResponse;

        beforeEach(function () {
          examLogic.testing.chooseIncorrect();
          examLogic.act.submit();
          flushSubmitTimeout();
          flushActivateTimeout();
          responses = results.current.testResults.responses;
          lastResponse = responses[responses.length - 1];
        });
        it('should be marked incorrect.', function () {
          expect(lastResponse.correct).toBeFalsy();
        });
      });


      it('should have relevant fields defined...', function () {
        examLogic.testing.chooseCorrect();
        examLogic.act.submit();
      });
    });

    describe('When it begins a page with wav files', function () {
      var playWav;
      beforeEach(function () {
        playWav = spyOn(examLogic.testing, 'playWav');
        examLogic.act.begin();
        flushActivateTimeout();
        examLogic.testing.goToId('ref1'); // go to sub-protocol
        flushActivateTimeout();
      });

    });

    describe('After an exam', function () {
      var nResultsInitial, nResultsAfter;

      beforeEach(function () {
        clearResults();
        disk.autoUpload = false;
        nResultsInitial = disk.queuedResults.length;
        examLogic.act.begin();
        flushSubmitTimeout();
        examLogic.testing.goToId('ref1');
        flushActivateTimeout();
        //examLogic.testing.goToId('mrt001');
        examLogic.testing.chooseIncorrect();
        flushSubmitTimeout();
        flushActivateTimeout();
        //        examLogic.act.submit();
        examLogic.testing.chooseCorrect();
        flushSubmitTimeout();
        flushActivateTimeout();
        //        examLogic.act.submit();
        examLogic.testing.chooseCorrect();
        flushSubmitTimeout();
        flushActivateTimeout();
        //        examLogic.act.submit();
        examLogic.act.submit();
        flushSubmitTimeout();
        flushActivateTimeout();
      });

      afterEach(function () {
        clearResults();
      });

      it('should have been finalized', function () {
        expect(examLogic.dm.state.mode).toEqual('FINALIZED');
      });
      it('should tabulate answers correctly.', function () {
        expect(results.current.nCorrect).toEqual(2);
        expect(results.current.nIncorrect).toEqual(1);
        expect(results.current.nResponses).toEqual(4);
        expect(results.current.testResults.responses.length).toEqual(4);
      });
      it('should have appended to the results queue.', function () {
        if (disk.server === 'tabsintServer') {
          nResultsAfter = disk.queuedResults.length;
          expect(nResultsAfter).toEqual(nResultsInitial + 1);
        }
      });
      it('should have a results schema that passes validation.', function () {
        var resultsAfter = _.last(disk.queuedResults);
        var jsonSchema;

        // Load results schema (sync).
        $.ajax({
          'async': false,
          'global': false,
          'url': 'base/test/spec/results_schema.json',
          'dataType': 'json'
        }).done(function (data) {
          jsonSchema = data;
        }).fail(function (e) {
          console.log('Error!');
          console.log(e.toString());
        });

        var valid = tv4.validate(angular.toJson(resultsAfter), jsonSchema);
        if (!valid) {
          console.log(tv4.error.toString());
        }
        expect(valid).toBeTruthy();

      });
    });

    describe('Timeouts', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
        examLogic.testing.goToId('ref_timeout');
        flushActivateTimeout();
      });

      it('should timeout (pages) and return to the parent protocol.', function () {
        for (var i = 0; i < 3; i++) {
          examLogic.testing.choose('y');
          flushSubmitTimeout();
          flushActivateTimeout();
          //examLogic.act.submit();
        }
        expect(page.result.presentationId).toEqual('yn005');
      });
      it('should not timeout early (pages).', function () {
        for (var i = 0; i < 2; i++) {
          examLogic.testing.choose('y');
          flushSubmitTimeout();
          flushActivateTimeout();
          //examLogic.act.submit();
        }
        expect(page.result.presentationId).toEqual('yn003');
      });
      it('should timeout (time) and return to the parent protocol.', function () {
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        // wind back the clock 700 seconds:
        _.last(examLogic.testing.protocolStack).startTime = new Date((new Date()).valueOf() - 700e3);
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        // should have timed out - ended subprotocol, moved on to yn005.
        expect(page.result.presentationId).toEqual('yn005');
      });
      it('should not timeout early (time).', function () {
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        // wind back the clock 300 seconds:
        _.last(examLogic.testing.protocolStack).startTime = new Date((new Date()).valueOf() - 300e3);
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        // should not have timed out yet - yn003 is next.
        expect(page.result.presentationId).toEqual('yn003');
      });
    });

    describe('Special References', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
        disk.autoUpload = false;

      });

      it('should end subprotocol properly on @END_SUBPROTOCOL.', function () {
        examLogic.testing.goToId('ref_special_end_subprot');
        flushActivateTimeout();
        for (var i = 0; i < 2; i++) {
          examLogic.testing.choose('y');
          flushSubmitTimeout();
          flushActivateTimeout();
          //examLogic.act.submit();
        }
        expect(page.result.presentationId).toEqual('yn005');
      });
      it('should end properly on @END_ALL.', function () {
        examLogic.testing.goToId('ref_special_end_all');
        flushActivateTimeout();
        for (var i = 0; i < 2; i++) {
          examLogic.testing.choose('y');
          flushSubmitTimeout();
          flushActivateTimeout();
          //examLogic.act.submit();
        }
        expect(examLogic.dm.state.mode).toEqual('FINALIZED');
      });

    });

    describe('Skip a presentation', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
        examLogic.testing.goToId('ref_skip_page');
        flushActivateTimeout();
      });

      it('should not skip it if the flag isn\'t set.', function () {
        expect(page.result.presentationId).toEqual('yn001');
        examLogic.testing.choose('no');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('yn002');
        examLogic.testing.choose('no');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('yn003');
      });

      it('should skip if the flag is set.', function () {
        expect(page.result.presentationId).toEqual('yn001');
        examLogic.testing.choose('yes');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('yn003');
      });
    });

    describe('Flags and Follow-Ons.', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
        examLogic.testing.goToId('ref_followOns');
        flushActivateTimeout();
      });

      it('should set flags and have them visible in the dm object', function () {
        expect(page.result.presentationId).toEqual('yn011');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(examLogic.dm.state.flags).toBeDefined();
        expect(examLogic.dm.state.flags.DO_FOLLOW_ON).toBeDefined();
      });

      it('should follow-on based on a flag.', function () {
        expect(page.result.presentationId).toEqual('yn011');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(page.result.presentationId).toEqual('yn012');
      });

      it('should not follow-on if the flag is not set.', function () {
        expect(page.result.presentationId).toEqual('yn011');
        examLogic.testing.choose('n');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(page.result.presentationId).toEqual('yn013');
      });

      it('should follow-on (to a reference) based on a conditional.', function () {
        expect(page.result.presentationId).toEqual('yn011');
        examLogic.testing.choose('n');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(page.result.presentationId).toEqual('yn013');
        examLogic.testing.choose('n');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(page.result.presentationId).toEqual('mrt001');
      });

      it('should follow-on based on an earlier flag.', function () {
        expect(page.result.presentationId).toEqual('yn011');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(page.result.presentationId).toEqual('yn012');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(page.result.presentationId).toEqual('yn013');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        //examLogic.act.submit();
        expect(page.result.presentationId).toEqual('yn015');
      });

      it('should still find the .wav file calibration even in a followon.', function () {
        expect(page.result.presentationId).toEqual('yn011');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('yn012');
        expect(page.dm.wavfiles[0].cal).toBeDefined();
      });

    });

    describe('Conditionals and followons', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
      });

      it('should have a working arrayContains function (positive test).', function () {
        examLogic.testing.goToId('ref2');
        flushActivateTimeout();
        examLogic.testing.goToId('cb001');
        flushActivateTimeout();
        examLogic.testing.choose('["Cyan","Blue"]');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('yn021');
      });

      it('should have a working arrayContains function (negative test).', function () {
        examLogic.testing.goToId('ref2');
        flushActivateTimeout();
        examLogic.testing.goToId('cb001');
        flushActivateTimeout();
        examLogic.testing.choose('["Cyan","Purple"]');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).not.toEqual('yn021');
      });
    });

    describe('Timeouts with subprotocols', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
      });

      it('should return properly', function () {
        examLogic.testing.goToId('prot_subprot_1pg_timeout');
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('yn001');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('yn002');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('prot_subprot_1pg_timeout_is_over');
      });
    });

    describe('Partial exam', function () {

      var nResultsInitial, nResultsAfter;

      it('should go to the Partial protocol, submit when its over, and mark the exam as partial.', function () {
        disk.autoUpload = false;
        nResultsInitial = disk.queuedResults.length;
        // begin a normal exam
        examLogic.act.begin();
        flushActivateTimeout();
        examLogic.testing.goToId('ref1');
        flushActivateTimeout();
        //examLogic.testing.goToId('mrt001');
        examLogic.testing.chooseIncorrect();
        flushSubmitTimeout();
        flushActivateTimeout();

        // submit partial
        examLogic.act.submitPartial();
        flushSubmitTimeout();
        flushActivateTimeout();

        // run through the partial subprotocol
        expect(page.dm.id).toEqual('partial001');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.dm.id).toEqual('partial002');
        examLogic.testing.choose('y');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(examLogic.dm.state.mode).toEqual('FINALIZED');

        // make sure it was submitted
        if (disk.server === 'tabsintServer') {
          nResultsAfter = disk.queuedResults.length;
          expect(nResultsAfter).toEqual(nResultsInitial + 1);

          // make sure it was marked partial
          var result = disk.queuedResults[disk.queuedResults.length-1];
          expect(result.testResults.partialResults).toBeTruthy();
        }
      });

    });

    describe('Repeats', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
      });

      it('should repeat if conditional not met, up to set maximmum.', function () {
        examLogic.testing.goToId('ref_repeat');
        flushActivateTimeout();
        examLogic.testing.choose('A');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat01');
        expect(results.current.testResults.responses[0].presentationId).toEqual('repeat01');
        examLogic.testing.choose('A');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat01');
        expect(results.current.testResults.responses[1].presentationId).toEqual('repeat01');
        examLogic.testing.choose('A');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat02');
        expect(results.current.testResults.responses[2].presentationId).toEqual('repeat01');
      });

      it('should stop repeating if conditional is met.', function () {
        examLogic.testing.goToId('ref_repeat');
        flushActivateTimeout();
        examLogic.testing.choose('A');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat01');
        expect(results.current.testResults.responses[0].presentationId).toEqual('repeat01');
        examLogic.testing.choose('B');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat02');
        expect(results.current.testResults.responses[1].presentationId).toEqual('repeat01');
      });

      it('should show n additional times if no conditional set.', function () {
        examLogic.testing.goToId('ref_repeat');
        flushActivateTimeout();
        examLogic.testing.goToId('repeat02');
        flushActivateTimeout();
        examLogic.testing.choose('1');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat02');
        expect(results.current.testResults.responses[0].presentationId).toEqual('repeat02');
        examLogic.testing.choose('3');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat02');
        expect(results.current.testResults.responses[1].presentationId).toEqual('repeat02');
        examLogic.testing.choose('1');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat02');
        expect(results.current.testResults.responses[2].presentationId).toEqual('repeat02');
        examLogic.testing.choose('1');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat03');
        expect(results.current.testResults.responses[3].presentationId).toEqual('repeat02');
      });

      it('should repeat 2 times if no nRepeat set.', function () {
        examLogic.testing.goToId('ref_repeat');
        flushActivateTimeout();
        examLogic.testing.goToId('repeat03');
        flushActivateTimeout();
        examLogic.testing.choose('x');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat03');
        expect(results.current.testResults.responses[0].presentationId).toEqual('repeat03');
        examLogic.testing.choose('x');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat03');
        expect(results.current.testResults.responses[1].presentationId).toEqual('repeat03');
        examLogic.testing.choose('x');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('repeat04');
        expect(results.current.testResults.responses[2].presentationId).toEqual('repeat03');
      });

      it('should not repeat if the protocol timed out', function () {
        examLogic.testing.goToId('ref_repeat');
        flushActivateTimeout();
        examLogic.testing.goToId('repeat05');
        flushActivateTimeout();
        for (var i = 0; i < 4; i++) {
          examLogic.testing.choose('6');
          flushSubmitTimeout();
          flushActivateTimeout();
          expect(page.result.presentationId).toEqual('repeat05');
          expect(results.current.testResults.responses[i].presentationId).toEqual('repeat05');
        }
        examLogic.testing.choose('6');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('choose');
        expect(results.current.testResults.responses[4].presentationId).toEqual('repeat05');
      });

    });

    describe('The back button', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
        examLogic.testing.goToId('ref_navigation');
        flushActivateTimeout();
      });

      it('should work and should blow away previous response.', function () {
        examLogic.testing.choose('Survey A: Youth Wearers');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(results.current.testResults.responses.length).toEqual(1);
        expect(page.result.presentationId).toEqual('infoSurveyA');
        examLogic.act.submit();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(results.current.testResults.responses.length).toEqual(2);
        expect(page.result.presentationId).toEqual('questionA1');
        examLogic.act.back();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyA');
        expect(results.current.testResults.responses.length).toEqual(1);
      });

      it('should inherit from parent protocols.', function () {
        examLogic.testing.choose('Survey B: Adult Wearers');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyB');
        examLogic.act.submit();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('questionB1');
        examLogic.act.back();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyB');
      });

      it('should NOT inherit from parent protocols if already set to false.', function () {
        examLogic.testing.choose('Survey C: Kangaroo Wearers');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyC');
        expect(page.dm.enableBackButton).toEqual(false);
        examLogic.act.submit();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyC2');
        expect(page.dm.enableBackButton).toEqual(false);
      });

      it('should not leave the current subprotocol.', function () {
        examLogic.testing.choose('Survey B: Adult Wearers');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyB');
        examLogic.act.back();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyB');
      });

    });

    describe('Navigation', function () {

      beforeEach(function () {
        examLogic.act.begin();
        flushActivateTimeout();
        examLogic.testing.goToId('ref_navigation');
        flushActivateTimeout();
      });

      it('should work.', function () {
        examLogic.testing.choose('Survey A: Youth Wearers');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyA');
        examLogic.act.submit();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('questionA1');
        expect(examLogic.testing.protocolStack.length).toEqual(4);
        expect(examLogic.testing.protocolStack[3].protocol.protocolId).toEqual('SurveyA');
        examLogic.act.navigateToTarget(page.dm.navMenu[0]);
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('choose');
        expect(examLogic.testing.protocolStack.length).toEqual(2);
        expect(examLogic.testing.protocolStack[1].protocol.protocolId).toEqual('MainMenu');
      });

      it('should goto and return to the previous protocol if return is true.', function () {
        examLogic.testing.choose('Survey A: Youth Wearers');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyA');
        expect(examLogic.testing.protocolStack.length).toEqual(4);
        expect(examLogic.testing.protocolStack[3].protocol.protocolId).toEqual('SurveyA');
        examLogic.act.navigateToTarget(page.dm.navMenu[3]);
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyB');
        expect(examLogic.testing.protocolStack.length).toEqual(5);
        expect(examLogic.testing.protocolStack[4].protocol.protocolId).toEqual('SurveyB');
        examLogic.act.submit();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('questionB1');
        expect(examLogic.testing.protocolStack.length).toEqual(5);
        expect(examLogic.testing.protocolStack[4].protocol.protocolId).toEqual('SurveyB');
        examLogic.testing.choose('2');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('questionB2');
        expect(examLogic.testing.protocolStack.length).toEqual(5);
        expect(examLogic.testing.protocolStack[4].protocol.protocolId).toEqual('SurveyB');
        examLogic.testing.choose('2');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyA');
        expect(examLogic.testing.protocolStack.length).toEqual(4);
        expect(examLogic.testing.protocolStack[3].protocol.protocolId).toEqual('SurveyA');
      });

      it('should goto and end after the goto protocol if return is not true.', function () {
        examLogic.testing.choose('Survey A: Youth Wearers');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyA');
        expect(examLogic.testing.protocolStack.length).toEqual(4);
        expect(examLogic.testing.protocolStack[3].protocol.protocolId).toEqual('SurveyA');
        examLogic.act.navigateToTarget(page.dm.navMenu[2]); // that's the adults, with no return
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('infoSurveyB');
        expect(examLogic.testing.protocolStack.length).toEqual(2);
        expect(examLogic.testing.protocolStack[1].protocol.protocolId).toEqual('SurveyB');
        examLogic.act.submit();
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('questionB1');
        expect(examLogic.testing.protocolStack.length).toEqual(2);
        expect(examLogic.testing.protocolStack[1].protocol.protocolId).toEqual('SurveyB');
        examLogic.testing.choose('2');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(page.result.presentationId).toEqual('questionB2');
        expect(examLogic.testing.protocolStack.length).toEqual(2);
        expect(examLogic.testing.protocolStack[1].protocol.protocolId).toEqual('SurveyB');
        examLogic.testing.choose('2');
        flushSubmitTimeout();
        flushActivateTimeout();
        expect(examLogic.dm.state.mode).toEqual('FINALIZED');
      });

    });


  });
});
