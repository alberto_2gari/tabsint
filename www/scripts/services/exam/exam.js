/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.exam', [])

    .factory('examLogic', function ($rootScope, $q, plugins, router, app, protocol, pm, media, disk, $timeout, version,
                                    config, androidFullScreen, devices, tabletLocation, results, noSleep, subjectHistory, page,
                                    notifications, advancedProtocol, tabsintNative, logger, hardware, slm, gettextCatalog, cha, chaExams) {

      var exam = {
        dm: {             // data model
          state: {
            iQuestion: 1,
            mode: undefined, // NOT-READY, READY, TESTING, FINALIZED, ADMIN
            displayMode: 'ENABLED', // ENABLED, DISABLED, TRANSITIONING
            nRepeats: 0
          }
        },
        act: {}          // API methods
      };

      var APPROX_TIME_PER_PAGE = 30;  // seconds

      /* Ordering in the internal model:

       * The exam protocol has a default ("convenience") ordering which is
       that, unless otherwise specified, we proceed sequentially from one
       protocol page to the next. But, in the future, we will expect to
       have exam logic that disrupts this order, so we can't count on it.
       In the actual exam, steps may be repeated, skipped, or out of order.

       * The results structure is strictly sequential. We might eventually move
       back one result to re-do something, but that won't disrupt the order.
       The results array tells us the order that results were given (implicitly),
       as well as the protocol page that was used for each presentation
       (explicitly, via the presentationId field).
       */

      // protocol stack records any paused protocols as we recurse into
      // the nested protocols. We should always be dealing with the last
      // protocol on the stack. When we push an exam onto the stack, we
      // always add a placeholder to the previous page's index in the
      // pageIdxStack.
      var protocolStack = [];


      /* Convenience fxn to get current protocol. Undefined if stack is empty. */
      function currProtocol() {
        return (protocolStack.length)? _.last(protocolStack).protocol:undefined;
      }
      function currPageQueue() {
        return (protocolStack.length)? _.last(protocolStack).pageQueue:undefined;
      }
      function getCurrPageIndex() {
        return (protocolStack.length)? _.last(protocolStack).pageIndex:undefined;
      }
      function setCurrPageIndex(newInd) {
        if (protocolStack.length > 0) {
          _.last(protocolStack).pageIndex = newInd;
        }
      }
      function getNextPage(nPages){
        // if out of pages in this protocol before incrementing nPages, remove the protocol, but do not keep incrementing in the parent protocol

        if (nPages === angular.undefined){nPages = 1;}

        if (currPageQueue() && (getCurrPageIndex() + nPages < currPageQueue().length) ) {
          _.last(protocolStack).pageIndex += nPages;
          return getCurrentPage();
        } else {
          removeCurrentProtocol(); // recurses until finding a parent protocol that isn't timed out and has pages left

          // if anything is left, return that page here
          if (protocolStack.length > 0) {
            return getNextPage();
          }
        }
        return false;
      }
      function getCurrentPage(){
        if (currPageQueue() === angular.undefined || getCurrPageIndex() === angular.undefined){
          return false;
        } else {
          return currPageQueue()[getCurrPageIndex()];
        }
      }
      function decrementPage(nPages){
        nPages = nPages || 1;
        // if we run out of pages in this protocol before incrementing nPages, remove the protocol, but do not keep incrementing in the parent protocol
        var tmpInd = _.last(protocolStack).pageIndex;
        _.last(protocolStack).pageIndex = (tmpInd - nPages) > -1?(tmpInd - nPages):-1;
        _.last(protocolStack).nPagesDone = Math.max(_.last(protocolStack).nPagesDone - nPages, 0);
        return _.last(protocolStack).pageIndex < tmpInd;
      }

      /*
       * Mark the current protocol as done.
       * Remove that protocol from the stack, then make sure there are pages left.  If not, remove that one too.
       * If pages left, check timeouts.
       */
      function removeCurrentProtocol(nRemove) {
        nRemove = (nRemove !== angular.undefined)? nRemove : 1;
        //var state = protocolStack.pop();  //BPF commented - state was overwriting parent var!
        for (var i = 0; i < nRemove; i++) {
          protocolStack.pop();
        }
        // We are done; we don't expect any more questions.
        //state.nPagesExpected = state.nPagesDone; //TODO follow this logic - what does this change?

        // if any protocols left
        if (protocolStack.length > 0) {
        // if no pages left in this protocol, remove this one too
          if (getCurrPageIndex() + 1 > currPageQueue().length) {
            removeCurrentProtocol();
          } else {
            // increment parent protocol pagesDone counter, check timeouts
            protocolStack[protocolStack.length - 1].nPagesDone += 1;
            checkNPagesBasedTimeouts();
          }
        }
      }

      /*  Convenience function for checking protocol timeouts
       *
       *  called from submit logic and when incrementing out of a subprotocol
       */
      function checkNPagesBasedTimeouts() {
        // Check for Npages-based timeouts (only last protocol in stack has
        // changing # of pages)
        if (currProtocol().timeout) {
          if (currProtocol().timeout.nMaxPages) {
            var nPagesDone = protocolStack[protocolStack.length - 1].nPagesDone;
            if (nPagesDone >= currProtocol().timeout.nMaxPages) {
              // Remove timed out protocol and all child protocols.
              logger.info('Timed out after ' + nPagesDone.toString() + ' pages.');
              if (currProtocol().timeout.alert) {
                notifications.alert(gettextCatalog.getString('This (sub)exam has timed out after ') + nPagesDone.toString() + gettextCatalog.getString(' pages.'));
              }
              removeCurrentProtocol();
            }
          }
        }
      }

      function checkTimeBasedTimeouts(){
        // Check for a timeout (in every protocol on the stack).
        for (var i = 0; i < protocolStack.length; i++) {
          var elapsedSeconds;
          if (protocolStack[i].protocol.timeout) {
            if (protocolStack[i].protocol.timeout.nMaxSeconds) {
              elapsedSeconds = ((new Date()) - protocolStack[i].startTime) / 1000;
              if (elapsedSeconds > protocolStack[i].protocol.timeout.nMaxSeconds) {
                if (protocolStack[i].protocol.timeout.alert) {
                  notifications.alert(gettextCatalog.getString('This (sub)exam has timed out after ') + elapsedSeconds.toString() + gettextCatalog.getString(' seconds.'));
                }
                // Remove timed out protocol and all child protocols.
                removeCurrentProtocol(protocolStack.length - i);
                break;
              }
            }
          }
        }
      }

      function checkTimeouts(){
        checkNPagesBasedTimeouts();
        checkTimeBasedTimeouts();
      }

      /*
       Convenience functions for the preProcessFunction
       */
      function isObject(obj){
        return (_.isObject(obj) && !_.isArray(obj) && !_.isFunction(obj));
      }

      function applyDiff(origin, diff){
        _.forEach(diff, function(value,key){
          // find this key in obj
          if (_.has(origin, key) && isObject(value)) {
            // recurse
            applyDiff(origin[key],value);
          } else {
            // found a matching key - overwrite the value
            origin[key] = value;
          }

        });
      }



      /**
       * Fill or re-fill 'anticipated protocols' with all sub-protocols of this
       * protocol.
       */
      function fillAnticipatedProtocols(protocol) {
        // clear the list.
        exam.dm.state.progress.anticipatedProtocols = [];

        // Recurse through the whole structure and add each
        // protocol to the anticipated list.
        var container, applicablePageLimits;
        function _populateList ( protocol ) {

          // ignore anything without a protocol ID:
          if ( protocol.protocolId ) {
            applicablePageLimits = [protocol.pages.length];
            if ( protocol.timeout ) {
              if ( protocol.timeout.nMaxSeconds ) {
                applicablePageLimits.push(protocol.timeout.nMaxSeconds / APPROX_TIME_PER_PAGE);
              }
              if ( protocol.timeout.nMaxPages ) {
                applicablePageLimits.push(protocol.timeout.nMaxPages);
              }
            }

            container = {
              nPagesExpected : _.min(applicablePageLimits),
              protocolId: protocol.protocolId
            };

            exam.dm.state.progress.anticipatedProtocols.push(container);
          }

          if (_.has(protocol, 'subProtocols')) {
            _.forEach(protocol.subProtocols, function (obj) {
              _populateList(obj);
            });
          }

          if (_.has(protocol, 'pages')) {
            _.forEach(protocol.pages, function (obj) {
              if (_.has(obj, 'protocolId')) {
                _populateList(obj);
              }
            });
          }
        }
        _populateList(protocol);
      }

      /**
       * Add a new protocol to the stack, and initialize a structure that keeps track of its state.
       */
      function activateNewProtocol(thisProtocol) {
        var state = {
          protocol: thisProtocol,
          startTime: (new Date()).toJSON(),
          nPagesDone: 0,
          nPagesExpected:0,
          pageQueue: [],
          pageIndex: 0
        };

        if ( thisProtocol.exclusiveTimingMode ) {
          fillAnticipatedProtocols(thisProtocol);
        }

        switch (thisProtocol.randomization) {
          case undefined:
            state.pageQueue = _.clone(thisProtocol.pages);
            break;
          case 'WithoutReplacement':
            if ( (thisProtocol.pages[0].questionMainText) && _.isUndefined(thisProtocol.pages[0].responseArea) ) {
              // The first page appears to be instructions,
              // so we keep it first in the exam page order,
              // even though the other pages will be randomized.
              var pages = angular.copy(thisProtocol.pages);
              var tempPage = pages.shift();
              state.pageQueue = _.shuffle(pages);
              state.pageQueue.unshift(tempPage);
            } else {
              state.pageQueue = _.shuffle(thisProtocol.pages);
            }
            break;
          default:
            logger.error('Undefined randomization type: ' + angular.toJson(thisProtocol.randomization));
        }

        // push onto protocol stack.
        protocolStack.push(state);

        // also, for progress calculation, add to activated protocols list and
        // remove from anticipated protocols list.
        exam.dm.state.progress.activatedProtocols.push(state);
        if ( thisProtocol.protocolId ) {
          exam.dm.state.progress.anticipatedProtocols = _.reject(
            exam.dm.state.progress.anticipatedProtocols,
            function ( container ) {
              return container.protocolId === thisProtocol.protocolId;
            }
          );
        }

      }

      /**
       * Returns a page, resolving references as necessary.
       * If *page* is a reference, the reference protocolis added to the stack, and the
       * first page of that protocol is returned.
       * @param pg
       * @private
       */
      function _resolvePage ( pg ) {
        if (!pg) { return false; }
        var nextProtocol, referencedPage;
        if (_.has(pg, 'skipIf') && evalConditional(pg.skipIf)) {
          // Skip the pg...
          referencedPage= getNextPage();
          return _resolvePage(referencedPage);
        }
        if (_.has(pg, 'reference')) {
          // it's a reference...

          // is it a special command?
          if (pg.reference === '@END_ALL') {
            removeCurrentProtocol(protocolStack.length);
            //finalize();
            return false;
          } else if (pg.reference === '@END_SUBPROTOCOL') {
            removeCurrentProtocol();
            referencedPage = getNextPage(); // need to increment parent pg!
            return _resolvePage(referencedPage);
          } else if (pg.reference === '@PARTIAL') {
            nextProtocol = pm.root._protocolIdDict[pg.reference];
            if (_.isUndefined(nextProtocol)) {
              logger.info('No @PARTIAL reference in protocol.');
              return false;
            } else {
              activateNewProtocol(nextProtocol);
              referencedPage = getCurrentPage();
              return _resolvePage(referencedPage);
            }
          } else {
            // Must be a normal reference.
            nextProtocol = pm.root._protocolIdDict[pg.reference];
            if (_.isUndefined(nextProtocol)) {
              notifications.alert('Unknown reference in protocol: '+ pg.reference);
              logger.error('Unknown reference in protocol: '+ pg.reference);
            }
            activateNewProtocol(nextProtocol);
            referencedPage = getCurrentPage();
            return _resolvePage(referencedPage);
          }
        } else if (_.has(pg, 'pages')) {
          // it's an in-line protocol...
          nextProtocol = pg;
          activateNewProtocol(nextProtocol);
          referencedPage= getCurrentPage();
          return _resolvePage(referencedPage);
        } else {
          // it must just be a page...
          return pg;
        }
      }

      exam.dm.startPageForExternalControl = {
        title: 'Matlab Interface',
        subtitle: 'This protocol will be controlled externally from Matlab',
        instructionText: 'Press begin to start the exam',
        helpText: 'none'
      };
      page.dm1 = {
        'id':'question1',
        'questionMainText':'How many years of service do you have?',
        'responseArea': {
          'type': 'integerResponseArea'
        }
      };
      page.dm2 = {
        'id': 'multichoice001',
        'title': 'Multiple Choice 1',
        'instructionText': 'Respond as appropriate. There is no wrong answer.',
        'responseArea': {
          'type': 'multipleChoiceResponseArea',
          'choices': [
            {
              'id': 'A',
              'text': 'Choice A'
            },
            {
              'id': 'B',
              'text': 'Choice B'
            }
          ]
        }
      };

      function getSubmittableLogic(pg) {
        // Determine page submission logic.
        // this might get more complex as we get different
        // input area types.
        if ( _.isUndefined(pg.responseArea)) {
          pg.isSubmittable = function () {
            return true;
          };
        } else {
          pg.isSubmittable = function () {
            if (angular.isUndefined(pg.responseArea) || pg.responseArea.responseRequired === false) {  // specifically equated to false because the default is "true"
              return true;
            } else {
              return (page.result.response !== undefined);
            }
          };
        }
        return pg;
      }

      exam.act.activateExternalPage = function(pg){
        exam.act.submit = exam.act.submitDefault;
        page.dm = undefined;
        $rootScope.$apply(); // TODO figure out how not to need this
        page.result = undefined;
        pg.canGoBack =  pg.enableBackButton || false;
        // initialize callback
        pg.showFeedback = undefined;

        // Determine page submission logic.
        pg = getSubmittableLogic(pg);

        // Create new result.
        var result = {
          presentationId: pg.id,
          response: undefined,
          correct: undefined
        };


        $timeout(function() {
          finishActivate(pg, result);
          exam.dm.state.displayMode = 'ENABLED';
        },20);
      };

      function getExternalPage(){
        console.log('EXTERNAL COMMAND:  this is where we ask Matlab to serve up the next page');
      }

      /**
       * Change to the given page. *page* can be a reference, a page, or an
       * in-line protocol.
       *
       * Returns true if the page was activated, and false if not.
       */
      function activatePage(pg) {
        exam.act.submit = exam.act.submitDefault;
        console.log('activating page');
        exam.act.gradeResponse = exam.act.gradeResponseDefault;

        var changedFields;
        pg = _resolvePage(pg);
        if (!pg) { return false; }
        pg = angular.copy(pg); // copy so we can add properties safely.

        // reset immersive mode
        androidFullScreen.immersiveMode();

        // Make sure Audio is stopped.
        media.stopAudio();

        // Apply protocol-wide properties.
        var INHERITABLE_PROPERTIES = ['title', 'subtitle', 'helpText', 'hideProgressBar', 'enableBackButton', 'navMenu', 'submitText', 'lookUpTables', 'slm'];

        // Iterate from child to root protocols, applying any missing fields.
        for(var i = protocolStack.length-1; i >= 0; i--) {
          _.defaults(pg, _.pick(protocolStack[i].protocol, INHERITABLE_PROPERTIES));
        }

        // run preprocessing function if available, before all the updates to canGoBack, isSubmittable, progress, etc.
        if (pg.preProcessFunction){
          var passIn = {
            _: _,
            result: angular.copy(_.last(results.current.testResults.responses)),
            examResults: angular.copy(results.current),
            page: angular.copy(pg),
            flags: exam.dm.state.flags
          };

          if (_.has(advancedProtocol.registry, pg.preProcessFunction)) {
            changedFields = advancedProtocol.run(pg.preProcessFunction, passIn);
          }  else {
            logger.error('The protocol referenced function '+pg.preProcessFunction+' but this function does not exist in the functionRegistry.');
            notifications.alert(gettextCatalog.getString('The protocol referenced pre-processing function ') +pg.preProcessFunction + gettextCatalog.getString(', but this function was not found in the registry. Please check the TabSINT documentation to confirm the function is properly defined'));
          }

          if (changedFields) {
            // hack to show no page based on preprocess function, and instead move on to next page in protocol
            if (changedFields === '@SKIP'){
              return activatePage(getNextPage());
            }
            applyDiff(pg, changedFields);
          }
        }

        // integrate SLM into page logic. If the SLM field is on page, we pass that the slm service to handle.
        // SLM is closed in `finishPage()`
        if (pg.slm) {
          slm.init(pg);
        }

        // auto-submit pages after a delay
        if(angular.isDefined(pg.autoSubmitDelay) && pg.autoSubmitDelay >= 50){
          $timeout(function(){
            pg.isSubmittable = function () {
              return true;
            };
            exam.act.submit();
          }, pg.autoSubmitDelay);
        }

        // Determine page back button logic
        pg.enableBackButton = pg.enableBackButton || false;
        var notTheFirstPage = _.last(protocolStack).pageIndex > 0;
        // Detect inline page follow-on's by noting when the current page does not match what the protocol stack expects
        //   it to be.
        var notInASinglePageFollowOn = (_.last(protocolStack).pageQueue[_.last(protocolStack).pageIndex].id === pg.id);
        // Detect when the previous page isn't what we expect it to be.
        var previousPageMatches = false;
        if ( results.current ) {
          var previousResult = _.last(results.current.testResults.responses);
          var expectedPreviousPage = _.last(protocolStack).pageQueue[_.last(protocolStack).pageIndex-1];
          previousPageMatches = previousResult && expectedPreviousPage && ( previousResult.presentationId === expectedPreviousPage.id);
        }
        if (pg.enableBackButton && notTheFirstPage && previousPageMatches && notInASinglePageFollowOn ){
          // check subprotocol index > 0 OR in a followOn TODO - check if we are in a followOn
          pg.canGoBack = function(){return true;};
        } else {
          pg.canGoBack = function(){return false;};
        }

        // Determine page submission logic.
        pg = getSubmittableLogic(pg);

        // initialize callback
        pg.showFeedback = undefined;

        // Update expected # of pages for every exam.
        var nPagesDone = 0,
          nPagesTotal = 0;
        _.forEach(exam.dm.state.progress.anticipatedProtocols, function ( container ) {
          nPagesTotal += container.nPagesExpected;
        });
        _.forEach(exam.dm.state.progress.activatedProtocols, function ( state ) {
          var applicablePageLimits = [state.pageQueue.length];
          if ( state.protocol.timeout ) {
            if ( state.protocol.timeout.nMaxSeconds ) {
              var remainingSeconds = state.protocol.timeout.nMaxSeconds - ((new Date()) - state.startTime)/1000;
              remainingSeconds = _.max([0, remainingSeconds]);
              applicablePageLimits.push(
                Math.ceil(remainingSeconds / APPROX_TIME_PER_PAGE)
              );
            }
            if ( state.protocol.timeout.nMaxPages ) {
              applicablePageLimits.push(state.protocol.timeout.nMaxPages - state.nPagesDone);
            }
          }
          state.nPagesExpected = state.nPagesDone + _.min(applicablePageLimits);
          nPagesDone += state.nPagesDone;
          nPagesTotal += state.nPagesExpected;
        });


        // Re-calculate pct progress.
        var newProgressEstimate = 100 * nPagesDone / (nPagesTotal+1);
        // must be between 0 and 100.
        newProgressEstimate = _.min([100, _.max([0, newProgressEstimate])]);
        // Never let it go down (for psychological reasons).
        //newProgressEstimate = _.max([exam.dm.state.progress.pctProgress, newProgressEstimate]);
        //exam.dm.state.progress.pctProgress = newProgressEstimate;

        exam.dm.state.progress.pctProgress = (pg.progressBarVal !== angular.undefined)?pg.progressBarVal:newProgressEstimate;

        // Create new result.
        var result = {
          presentationId: pg.id,
          response: undefined,
          correct: undefined,
          isSkipped: false,
          responseArea: pg.responseArea ? pg.responseArea.type : undefined,
          page: {
            wavfiles: pg.wavfiles,
            chaWavFiles: pg.chaWavFiles,
            image: pg.image,
            video: pg.video,
            responseArea: pg.responseArea
          }
        };

        if (changedFields){
          result.changedFields = changedFields;
        }

        page.dm.responseArea = undefined;

        $timeout(function() {finishActivate(pg, result);},20);

        return true;
      }

      function finishActivate(pg, result){
        page.dm = pg;

        exam.dm.state.mode = 'TESTING';
        page.result = result;

        // pass in disk and page so they are available to the run functions
        plugins.runEvent('pageStart', {disk: disk, page: page.dm})
          .then(function() {
            try {
              if ((disk.headset === 'Creare Headset' || disk.headset === 'WAHTS') && (page.dm.video || page.dm.wavfiles)) {
                if (!disk.disableAudioStreaming) {
                  if (!cha.streaming) {
                    return chaExams.startTalkThrough();
                  }
                } else {
                  notifications.alert('Audio streaming is currently disabled. Streaming can be enabled in the WAHTS preferences on the Admin page.');
                }
              } else {
                if (disk.disableAudioStreaming) {
                  logger.debug('Audio Streaming Disabled - Cannot Start TalkThrough');
                }
                return chaExams.reset()
                  .then(function() {
                    if (page.dm.chaWavFiles) {
                      chaExams.playWavs(page.dm.chaWavFiles);
                    }
                  });
              }
            } catch (e) { logger.debug('CHA - error on page start event: '+JSON.stringify(e)); }
          })
          .then(function() {
            // Start audio files(s) (if applicable):
            media.stopAudio();
            if (angular.isDefined(page.dm.wavfiles)) {
              var startDelayTime = angular.isDefined(page.dm.wavfileStartDelayTime) ? page.dm.wavfileStartDelayTime : 1000; // use default delay of 1000ms if no time specified
              media.playWav(page.dm.wavfiles, startDelayTime);
            }

            // Video handling
            if (angular.isDefined(page.dm.video)){
              tabsintNative.resetAudio(null,  tabsintNative.onVolumeErr);

              // video object to hold important local information
              var video = {
                submittable: true,
                elem: undefined
              };

              // submittable logic only set once here
              page.dm.isSubmittable = function() {
                return video.submittable;
              };

              $timeout(function() {
                video.elem = document.getElementById('video1'); // mobile browsers often disable allowing auto-play.  Autoplay must be set in the html AND play must be called here
                if (page.dm.video.autoplay) {
                  video.elem.play();
                }

                if(page.dm.video.noSkip){   // if skipVideo is false
                  video.submittable = false;
                  video.elem.addEventListener('ended', function() {    // wait until the video has ended to make our local handle false
                    video.submittable = true;
                    $rootScope.$apply();
                  });
                }
              }, 250);
            }

            // Start response timer
            page.result.responseStartTime = new Date();

            // If previous page was scrolled down this page will be too, scroll back to top - starting at the bottom is annoying!
            window.scrollTo(0,0);
          });
      }

      function _idxOfId(id) {
        var pg = _.find(currPageQueue(),{id:id});
        if (_.isUndefined(pg)) {
          notifications.alert(gettextCatalog.getString('Protocol Error')+': \n\n'+gettextCatalog.getString('No such page id remaining in protocol queue: ') + id.toString());
          logger.error('No such page id remaining in protocol queue: ' + id.toString());
        }
        // Get page index
        return currPageQueue().indexOf(pg);
      }

      /**
       * Change page by id, only within a subprotocol. Figures out index, then applies it
       * */
      function goToId(id) {
        var idx = _idxOfId(id);
        setCurrPageIndex(idx);
        return activatePage(getCurrentPage());
      }

      /**
       * Finalize the exam, end testing,
       * and enter a display mode. */

      exam.act.finalize = function () {
        media.stopAudio();  // Make sure Audio is stopped.

        // Calculating the exam elapsed time.  Could probably be simpler!
        var stopTime = new Date();
        var startTime = new Date(JSON.parse('"'+results.current.testDateTime+'"'));
        var diff = Math.abs(stopTime - startTime); // in ms
        diff /= 1000; //throw away ms
        var dDays = Math.floor(diff/(24*60*60));
        diff = diff%(24*60*60);
        var dHours = Math.floor(diff/(60*60));
        diff = diff%(60*60);
        var dMinutes = Math.floor(diff/60);
        diff = diff%(60);
        var dSeconds = Math.floor(diff);

        var sDays = dDays < 10? '0'+dDays: ''+dDays;
        var sHours = dHours < 10? '0'+dHours: ''+dHours;
        var sMinutes = dMinutes < 10? '0'+dMinutes: ''+dMinutes;
        var sSeconds = dSeconds < 10? '0'+dSeconds: ''+dSeconds;

        results.current.elapsedTime = sHours+':'+sMinutes+':'+sSeconds;


        // tabulate
        results.current.nCorrect = 0;
        results.current.nIncorrect = 0;
        results.current.nResponses = 0;

        for (var i = 0; i < results.current.testResults.responses.length; i++) {
          var response = results.current.testResults.responses[i];
          results.current.nResponses += 1;

          if (typeof(response.correct) === 'string') {
            if (_.includes(angular.fromJson(response.correct), false)) {
              results.current.nIncorrect +=1;
            } else {
              results.current.nCorrect +=1;
            }
          } else if (response.correct === true) {
            results.current.nCorrect += 1;
          } else if (response.correct === false) {
            results.current.nIncorrect += 1;
          }
        }

        // finalize
        page.dm = {
          id: undefined,
          title: pm.root.title,
          subtitle: pm.root.subtitle,
          instructionText: '',
          helpText: ''
        };

        results.save(results.current);
        disk.currentResults = undefined; // can reset this now - we have generated a proper result

        page.result = undefined;
        exam.dm.state.mode = 'FINALIZED';
        // If previous page was scrolled down this page will be too, scroll back to top - starting at the bottom is annoying!
        window.scrollTo(0,0);
        noSleep.allowSleepAgain();
      };


      /**
       * Mark first correct or incorrect answer.
       * Useful for testing.
       * Returns false if it wasn't able to do it.
       * */
      function chooseCorrect() {
        if (page.dm.responseArea.hasOwnProperty('choices')) {
          for (var i = 0; i < page.dm.responseArea.choices.length; i++) {
            var choice = page.dm.responseArea.choices[i];
            if (choice.correct === true) {
              page.result.response = choice.id;
              exam.act.submit();
              return true;
            }
          }
        } else if (page.dm.responseArea.type === 'omtResponseArea') {
          page.result.response = page.dm.responseArea.correct;
          exam.act.submit();
          return true;
        }
        return false; // No correct answer was found...
      }

      function chooseIncorrect() {
        if (page.dm.responseArea.hasOwnProperty('choices')) {
          for (var i = 0; i < page.dm.responseArea.choices.length; i++) {
            var choice = page.dm.responseArea.choices[i];
            if (choice.correct !== true) {
              page.result.response = choice.id;
              exam.act.submit();
              return true;
            }
          }
        } else if (page.dm.responseArea.type === 'omtResponseArea') {
          if (page.dm.responseArea.language === 'american') {
            page.result.response = 'Allen bought two cheap chairs';
          } else if (page.dm.responseArea.language === 'british') {
            page.result.response = 'Alan bought some big beds';
          }
          exam.act.submit();
          return true;
        }
        return false; // No correct answer was found...
      }

      function choose(id) {
        page.result.response = id;
        exam.act.submit();
        return true;
      }

      /**
       * Reset exam to pristine state so it is ready to begin a new subject.
       * */
      exam.act.reset = function reset(startPage){
        logger.debug('Resetting exam');
        plugins.runEvent('resetStart');
        tabsintNative.resetAudio(null,  tabsintNative.onVolumeErr);

        try {chaExams.reset(); }
        catch (e) { logger.debug('CHA - chaExams.reset failed on exam reset with error: ' + angular.toJson(e));  }

        try {chaExams.clearStorage(); }  // reset placeholders within complex pages like audiometry-list
        catch (e) { logger.debug('CHA - chaExams.clearStorage failed on exam reset with error: ' + angular.toJson(e)); }

        // logger siteid
        try{ logger.param.siteId = disk.protocol.siteId; }
        catch(e){ logger.warn('Logger failed to set siteId');}

        // reset slm - these methods will immediately return if SLM is not available
        slm.stop()
          .then(slm.close);

        // setup exam
        page.result = undefined;
        exam.dm.state.iQuestion = 1;
        exam.dm.state.mode = 'READY';
        exam.dm.state.progress = {
          pctProgress : 0,
          anticipatedProtocols: [],
          activatedProtocols: []
        };

        results.current = undefined;
        exam.dm.state.flags = {};
        exam.dm.state.nRepeats = 0;

        var pg = {};
        if (disk.externalMode){
          pg = resetExternal(startPage);
        } else {
          pg = resetInternal();
        }

        if (!pg){
          return;
        }

        page.dm = {
          title: pg.title,
          subtitle: pg.subtitle,
          instructionText: pg.instructionText,
          helpText: pg.helpText
        };

        // perform asynchronous loading  when app is ready
        return app.ready()
          .then(hardware.checkHeadsetTabletCalib)
          .then(function() {
            if (pm.root._hasSubjectIdResponseArea) {
              return subjectHistory.load()
                .then(function() {
                  exam.dm.state.flags.subjectHistory = subjectHistory.history.data;
                });
            }
          })
          .finally(function() {
            plugins.runEvent('resetEnd');
          });
      };

      function resetInternal () {
        if (!pm.root) {
          exam.dm.state.mode = 'NOT-READY';
          return false;
        }

        // load protocol
        protocol.reset();
        protocolStack.splice(0,protocolStack.length); // reset/empty it.

        // prepare progress tracking structures.
        if (pm.root) {
          fillAnticipatedProtocols(pm.root);
          activateNewProtocol(pm.root);
        } else {
          exam.dm.state.mode = 'NOT-READY';
          return false;
        }

        // Set up 'instruction' page.
        return currProtocol(); // shortcut
      }

      function resetExternal (startPage){
        startPage = startPage || exam.dm.startPageForExternalControl;

        pm.root = {
          _audioProfileVersion: undefined,
          _calibrationPySVNRevision: undefined,
          _calibrationPyManualReleaseDate: undefined
        };

        return startPage;
      }

      /**
       * Begin exam. Initialize any necessary structures and proceed to first page.
       * */
      exam.act.begin = function () {

        // check to see how much memory is left on disk
        if (angular.toJson(disk).length > 8000000){
          notifications.alert(gettextCatalog.getString('The tablet\'s storage space is too low to continue. Please upload exams as soon as possible to avoid data loss.'));
          logger.error('Memory maxed at 8M');
          return;
        } else if (angular.toJson(disk).length > 2000000){
          notifications.alert(gettextCatalog.getString('The tablet\'s storage space is getting low. Please upload exams as soon as possible to avoid data loss.'));
          logger.warn('Warned memory low');
        }

        // turn screen sleep off
        noSleep.keepAwake();

        // reset audio
        tabsintNative.resetAudio(null,  tabsintNative.onVolumeErr);

        // switch exam state displayMode
        exam.dm.state.displayMode = 'DISABLED';
        $timeout(function () {
          exam.dm.state.displayMode = 'ENABLED';
        }, 150);

        // initialize examResults
        results.current = {

          siteId: disk.protocol.siteId || null,
          protocolId: disk.protocol.id || null,
          protocolName: disk.protocol.name,
          protocolHash: disk.protocol.hash,
          qrString : undefined,
          testDateTime: (new Date()).toJSON(),
          elapsedTime: undefined,
          subjectId: undefined,
          testResults: {
            protocol: angular.copy(disk.protocol),
            responses: [],
            softwareVersion : {
              version: version.dm.tabsint,
              date: version.dm.date,
              rev: version.dm.rev
            },
            cordovaPlugins: config.cordovaPlugins,
            tabsintPlugins: config.tabsintPlugins,
            buildName: config.build,
            platform: devices.platform,
            network: null,   // this gets set when uploaded, if uploaded
            tabletUUID: devices.UUID,
            tabletModel: devices.model,
            tabletLocation: tabletLocation.position,
            partialResults: undefined,
            headset: disk.headset || 'None',
            calibrationVersion: {
              audioProfileVersion: pm.root._audioProfileVersion,
              calibrationPySVNRevision: pm.root._calibrationPySVNRevision,
              calibrationPyManualReleaseDate: pm.root._calibrationPyManualReleaseDate
            },
            isAdminMode: disk.debugMode
          }
        };

        // delete repo field from gitlab protocols - way too verbose
        if (results.current.testResults.protocol.repo) {
          delete results.current.testResults.protocol.repo;
        }

        // initialize temp storage of current test results
        disk.currentResults = angular.copy(results.current);  // load all the fields into the temp storage option

        // get the current position and save it in the results
        tabletLocation.updateCurrentPosition()
          .then(function(){
            if (angular.isDefined(results.current)){
              results.current.tabletLocation = disk.tabletLocation;   // if position is not updated, this value will be the same as before
            }
          });

        logger.info(`Beginning exam on tablet UUID: ${results.current.testResults.tabletUUID} at Location: ${Object.hasOwnProperty(results.current.testResults.tabletLocation)}`);

        if (disk.externalMode) {
          getExternalPage();
        } else {
          activatePage(getCurrentPage());
        }
      };


      /**
       * Go back to previous page. (current changes to page will be lost.  Previous results are overwritten)
       * */
      exam.act.back = function back () {
        if (decrementPage(1)) {
          var popPage = results.current.testResults.responses.pop();
          logger.info('User pressed back button. Going back 1 page, overwriting previous result which was: ' + JSON.stringify(popPage));
          activatePage(getCurrentPage());  // load page
        } else {
          logger.warn('User pressed back button, cannot go back.');
        }
      };

      /**
       * Evaluate a condition in a context that includes copies of:
       *  flags: an object whose properties are any flags that have been set. (exam.dm.state.flags)
       *  result: the most recent result (as recorded in results.current)
       **/
      function evalConditional ( conditional, locals ) {
        locals = locals || {};
        var ret;

        _.extend(locals, {
          _: _,
          arrayContains: function(strArray, item) {return _.includes(angular.fromJson(strArray),item);},
          getPresentation: function(presId) {return _.filter(results.current.testResults.responses, function(res) {return res.presentationId === presId;})[0] ;},
          getLastPresentation: function(presId) { return _.filter(results.current.testResults.responses, function(res) {return res.presentationId === presId;}).pop() ;},
          flags: angular.copy(exam.dm.state.flags),
          result: angular.copy(_.last(results.current.testResults.responses)),
          examResults: angular.copy(results.current),
          Math: Math
        });

        // Co-opting AngularJS's eval because it's so much safer than native javascript eval.
        try {
          ret = $rootScope.$new().$eval(conditional, locals);
        } catch (err) {
          notifications.alert(gettextCatalog.getString('There is an error in this page\'s conditional in the exam protocol')+': \n\n' + err.toString() + '\n\n'+gettextCatalog.getString('The conditional is: \n\t') + conditional);
          ret = false;
        }
        return ret;
      }

      /**
       * Prepare results for given page, either during submission, or if a page wants to save intermediate results
       * do any required close-out to finalize the result.
       * - mark responseDateTime.
       * - mark if correct/incorrect.
       * - mark result.presentationId w/ corresponding pageId.
       */
      exam.act.pushResults = function() {
        // add response elapsted time to result
        page.result.responseElapTimeMS = ((new Date()) - page.result.responseStartTime);

        // Check correctness of different response areas
        if ( page.dm.responseArea ) {
          exam.act.gradeResponse();
        }

        // push the results onto the testresults stack and disk `currentResults` object (in case tabsint crashes)
        logger.info('RESULT: ' + angular.toJson(page.result));
        results.current.testResults.responses.push(page.result);
        disk.currentResults.testResults.responses.push(page.result);
      };

      // generic grading for responseAreas with choices
      function gradeResponses (choices) {
        page.result.correct = undefined;
        page.result.eachCorrect = undefined;

        // page.result.response should be a string in this case
        for (var i = 0; i < choices.length; i++) {
          var choice = choices[i];
          // If we find our choice id, and it's correct, mark it as such.
          if (choice.id === page.result.response) {
            if (choice.correct === true || choice.correct === 'true') {
              page.result.correct = true;
            }
          }
          // Furthermore, if we find *any* correct
          // choices, make sure that result.correct
          // is not 'undefined', because now we know
          // it is false. (i.e., it's not a 'questionnaire'
          // question without a right answer.
          if (choice.correct === true || choice.correct === 'true') {
            if (page.result.correct === undefined) {
              page.result.correct = false;
            }
          }
        }
      }

      // default grading - any responseArea ctrl can overwrite act.gradeResponse
      // act.gradeResponse gets reset at beginning of each page in activate page
      exam.act.gradeResponseDefault = function(){
        if ( page.dm.responseArea.choices ) {
          gradeResponses(page.dm.responseArea.choices);
        } else if ( page.dm.responseArea.hotspots ) {
          gradeResponses(page.dm.responseArea.hotspots);
        } else if ( page.dm.responseArea.correct ) {
          page.result.correct = (page.result.response === page.dm.responseArea.correct );
        }
      };

      exam.act.stopAllMedia = function() {
        // Stop media files here
        media.stopAudio();

        // find the video element and stop playback
        if (page.dm.video !== undefined) {
          var vid = document.getElementById('video1');
          logger.debug('Stopping video playback, vid = ' + vid);
          if (vid !== undefined && vid !== null) {
            vid.pause();
          }
        }
      };

      // common calls when finishing a page.  async now, for slm
      function finishPage(){
        var deferredFinishPage = $q.defer();

        exam.act.stopAllMedia();

        try {chaExams.clearStorage(); }  // reset placeholders within complex pages like audiometry-list
        catch (e) { logger.debug('chaExams.clearStorage() got caught for: ' + angular.toJson(e)); }

        plugins.runEvent('pageEnd'); // NOTE - could put this in the promise chain if we don't want it running parallel with SLM calls

        // lock submit button off until next page loads
        page.dm.isSubmittable = function() {return false;};

        // store SLM data onto the results structure of the current page. this method closes the SLM
        if (page.dm.slm) {
          slm.getSLM(page.dm)
            .then(function(slmResult) {
              page.result.slm = slmResult;
            })
            .catch(function(e){
              logger.warn('examLogic.finishPage caught on slm.getSLM call with error: ' + JSON.stringify(e));
            })
            .finally(function() {
              deferredFinishPage.resolve();
            });
        } else {
          deferredFinishPage.resolve();
        }

        return deferredFinishPage.promise;
      }

      /*    called by submit, navigateToTarget, and submitPartial
            combines common calls for the fancy transitions
       */
      function startPageTransition(callback){
        exam.dm.state.displayMode = 'DISABLED';   // slight pause disabled, then switch to next page
        $timeout(function () {
          callback();
        }, 100);//submitPauseInterval
      }

      /**
       * Called when user selects a custom navMenu target in the admin dropdown menu during an exam.
       *
       * @params target - the navOption object, containing:
       *    .target:  the page or reference to navigate to
       *    .text: the display text for the menu item
       *    .returnHereAfterward: a flag determining whether the exam should come back to the current point or abort all current subprotocols before navigating away.
       */
      exam.act.navigateToTarget = function(navOption){
        logger.debug('Navigating to '+angular.toJson(navOption.target));

        function finishNavigate() {
          exam.dm.state.displayMode = 'ENABLED';
          // if moving on to a new section and not coming back, remove all the current items except the top level.
          if (!!navOption.returnHereAfterward){
            logger.debug('saving current location');
            decrementPage(1); // decrease by 1, so when we finish the subprotocol and increment, we end up coming back to the current page
          } else {
            logger.debug('removing all but top level protocol');
            removeCurrentProtocol(protocolStack.length-1); // remove all but bottom protocol (oldest parent?)
            setCurrPageIndex(currPageQueue().length-1); // set index of remaining protocol to complete
            exam.dm.state.nRepeats = 0; // need to reset this number!  todo are there other numbers we should re-init?
          }

          try {
            var activationSuccess = activatePage(navOption.target);
            if (activationSuccess) {
              return;
            }
          } catch (err) {
            notifications.alert(err.toString() + '\n\n'+gettextCatalog.getString('Please alert an administrator. Unfortunately, this exam cannot be completed. Reset the exam to try again.'));
            logger.error('In navigateToTarget.  Details: ' + err.toString());
          }
        }

        finishPage()
          .then(function() {
            startPageTransition(finishNavigate); // stop audio, transition
          });
      };

      /**
       * Called when user submits partial.  check for @PARTIAL, then finalize
       */
      exam.act.submitPartial = function () {
        function finishSubmitPartial() {
          exam.dm.state.displayMode = 'ENABLED';
          // Empty the protocol stack, just like we do for @END_ALL.
          removeCurrentProtocol(protocolStack.length);
          // Mark the results as partial.
          results.current.testResults.partialResults = true;

          // If there is one, run the @PARTIAL subprotocol just like we would
          // in a normal protocol, using a 'reference'.
          var activationSuccess = activatePage({ 'reference': '@PARTIAL' });
          if (activationSuccess) {
            logger.info('Activated a @PARTIAL subprotocol after request for submitting partial results.');
            $rootScope.$apply();
            return;
          } else {
            logger.info('No @PARTIAL subprotocol found. Finalizing partial exam.');
          }

          // Otherwise, follow the usual finalize() logic immediately.
          exam.act.finalize();
        }

        if (disk.externalMode){
          logger.info('Beginning external partial exam results submission.');
          results.current.testResults.partialResults = true;
          console.log('EXTERNAL COMMAND: User Has Selected End Partial.  Send mandatory page, or send end command to finalize');
          exam.act.finalize();
        } else {
          logger.info('Beginning partial exam results submission.');

          finishPage()
            .then(function() {
              startPageTransition(finishSubmitPartial); // stop audio, transition
            });
        }
      };

      /**
       * Submit results if available, and go to next page.
       * */
      exam.act.submit = exam.act.submitDefault;
      exam.act.submitDefault = function() {
        // if not ready to submit, alert with error and just return.
        if (!page.dm.isSubmittable()) {
          logger.warn('Cannot submit, not submittable...');
          return;
        }

        finishPage()
          .then(exam.act.pushResults) // save the current result and reset temp result object (page.result)
          .then(function() {
            if (disk.externalMode){
              submitExternal();
            } else {
              submitInternal();
            }
          });
      };

      function submitInternal () {
        // This Section Works With The animate-switch-container and animate-switch.
        // a bit of a hack, using 2 exam pages (exactly the same) and flipping between them to trigger a switch slide
        if (page.dm.showFeedback === undefined) {
          startPageTransition(finishSubmit); // stop audio, transition
        } else {
          startPageTransition(function(){
            page.dm.showFeedback();
            $timeout(function(){
              finishSubmit();
            }, 1250);
          });
        }
      }

      function submitExternal () {
        // count it as a question if the answer is not undefined...
        if ( page.result.response !== undefined ) {
          exam.dm.state.iQuestion += 1;
        }

        exam.dm.state.displayMode = 'DISABLED';   // slight pause disabled, then switch to next page

        // This Section Works With The animate-switch-container and animate-switch.
        // a bit of a hack, using 2 exam pages (exactly the same) and flipping between them to trigger a switch slide
        if (page.dm.showFeedback === undefined) {
          console.log('EXTERNAL COMMAND: page done, with XY results in case they are needed for next page logic, send next page or finalize');
        } else {
          page.dm.showFeedback();
          $timeout(function(){
            console.log('EXTERNAL COMMAND: page done, with XY results in case they are needed for next page logic, send next page or finalize');
          }, 1250);
        }
      }

      function finishSubmit(){
        exam.dm.state.displayMode = 'ENABLED';
        var i;


        // Add pages done
        protocolStack[protocolStack.length-1].nPagesDone += 1;

        // count it as a question if the answer is not undefined...
        if ( page.result.response !== undefined ) {
          exam.dm.state.iQuestion += 1;
        }

        // re-initialize the result to undefined
        page.result = undefined;

        checkTimeouts();  // will remove a protocol if timed out based on nPages or actual time

        // Set any flags...  Use page.dm, even if that means we will use flags from a time out page.
        if (page.dm.setFlags) {
          _.forEach(page.dm.setFlags, function (setFlag) {
            if (_.isUndefined(setFlag.conditional)) {
              exam.dm.state.flags[setFlag.id] = true;
            }
            else {
              exam.dm.state.flags[setFlag.id] = evalConditional(setFlag.conditional);
            }
          });
        }

        // check for repeat logic - Note - cannot use page.dm, in case we timed out already.  Use getCurrentPage()
        if ( getCurrentPage().repeatPage !== undefined){ // does it have repeat logic

          var r = getCurrentPage().repeatPage;
          r.nRepeats = (r.nRepeats !== angular.undefined)? r.nRepeats : 2; // cap number of repeats

          if (exam.dm.state.nRepeats < r.nRepeats) { // check number of repeats
            if ( (r.repeatIf !== undefined && evalConditional(r.repeatIf)) || (r.repeatIf === undefined)) {

              if (activatePage(getCurrentPage())) {  // just feed the same page in again
                exam.dm.state.nRepeats++;
                logger.info('Repeating the page, nRepeats = '+exam.dm.state.nRepeats);
                return;
              }
            }
          } else {
            exam.dm.state.nRepeats = 0;   // finished repeating
          }
        } else {
          exam.dm.state.nRepeats = 0;      // repeats only work for a page - they are not saved once you navigate somewhere else, like a subprotocol
        }

        // Do a follow-on, if applicable.
        if (page.dm.followOns) {
          for (i = 0; i < page.dm.followOns.length; i++) {
            var followOn = page.dm.followOns[i];
            if (_.isUndefined(followOn.conditional) || evalConditional(followOn.conditional)) {
              try {
                var activationSuccess = activatePage(followOn.target);
                if (activationSuccess) {
                  return;
                }
              } catch (err) {
                notifications.alert(err.toString() + '\n\n'+gettextCatalog.getString('Please alert an administrator. Unfortunately, this exam cannot be completed. Reset the exam to try again.'));
                logger.error('In submit() followOns.  Details: ' + err.toString());
              }
            }
          }
        }

        // Otherwise, go to the next page in the list (if there is one)...
        if (activatePage(getNextPage()) ){  return;  }

        // If there are no pages left, finalize the exam
        exam.act.finalize();
      }

      /**
       * Show help text on a page
       * Questions will inherit helpText from protocol helpText unless defined individually on each page
       * Disable help button if no help text is defined
       */
      exam.act.help = function() {
        if (exam.dm && page.dm && page.dm.helpText) {
          notifications.alert(page.dm.helpText);
        }
      };

      exam.act.skip = function() {
        logger.info('Skipping Page');
        page.result.isSkipped = true;
        page.dm.isSubmittable = function() {return true;};
        exam.act.submit = exam.act.submitDefault;
        exam.act.submit();
      };


      /**
       * Utility to properly handle state switch to exam view
       */
      exam.act.switchToExamView = function() {

        // try loading a protocol from disk.protocol if one is not already available
        if (!angular.isDefined(pm.root)){
          protocol.load(undefined, disk.validateProtocols)
            .then(exam.act.reset)  // reset the test - still need to reset to show the proper 'exam disabled' note
            .then(finishSwitch)
            .catch(function() {
              notifications.alert(gettextCatalog.getString('No protocol has been loaded. Please navigate to the Admin View and load a protocol.'));
            });
        } else {
          finishSwitch();
        }

        function finishSwitch() {
          // Call plugins act
          plugins.runEvent('switchToExamView', {disk: disk, page: page.dm});

          // switch the view
          router.goto('EXAM');

          if (page.dm && page.dm.id) {
            logger.debug('re-activating page ' + page.dm.id);
            finishActivate(page.dm, page.result);
          }
        }
      };


      /**
       * Methods used in testing
       * @type {Object}
       */
      exam.testing = {
        goToId: goToId,
        choose: choose,
        chooseCorrect: chooseCorrect,
        chooseIncorrect: chooseIncorrect,
        protocolStack: protocolStack
      };


      return exam;
    });
});
