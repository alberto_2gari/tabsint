/*...*/

define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.advanced-protocol', [])

    .factory('advancedProtocol', function(logger, $window, $q, notifications, gettextCatalog) {

        var advancedProtocol = {};

        /**
         * @type {Object} Registry to hold pre-process functions during protocols.
         * This gets reset everytime a protocol is loaded
         */
        advancedProtocol.registry = {};

        /**
         * Define global function registering method
         * @param  {string} name     name of function
         * @param  {function} func - function handle to evaluate
         */
        $window.tabsint.register = function(name, func) {
          if (!_.includes(_.keys(advancedProtocol.registry), name)) {
            advancedProtocol.registry[name] = func;
          } else {
            logger.error('Function name already registered for this protocol');
          }
        };

        /**
         * Clear the advanced protocol registry
         */
        advancedProtocol.reset = function() {
          advancedProtocol.registry = {};
        };

        /**
         * Load javascript files associated with protocol
         * @param  {string} prefix - path prefix to protocol
         * @param  {array} files - list of javascript files to load
         */
        advancedProtocol.loadJs = function (prefix, files) {
          var promises = [];
          var js;

          if (typeof(files) === 'object') {
            js = handleFiles(files);
          } else {
            js = handleFiles([files]);
          }

          function handleFiles(fileList) {
            return _.map(fileList, function (file) { // append prefix to each js file
              if (file.slice(-3) !== '.js') {
                file = file + '.js';
              }
              return prefix + file;
            });
          }

          _.forEach(js, function(file, idx) {
            var deferred = $q.defer();
            $.ajax({
              'async': false,
              'cache': false,
              'global': false,
              url: file,
              dataType: 'script',
              success: function() {
                logger.debug('Sucessfully loaded script: ' + file);
                logger.debug('Sucessfully registered functions: ' + _.keys(advancedProtocol.registry));
                deferred.resolve();
              },
              error: function(e, type) {
                logger.error('Failed to load script: ' + file + ' because of error ' + type);
                //notifications.alert('Failed to load custom javascript file: ' + files + ' because of error: "' + type +'".  Please make sure the file exists, has proper syntax, and is referenced at the top level of the protocol in the "js" field.');
                deferred.reject(gettextCatalog.getString('Failed to load custom javascript file: ') + file + gettextCatalog.getString(' because of error: "') + type +gettextCatalog.getString('".  Please make sure the file exists, has proper syntax, and is referenced at the top level of the protocol in the "js" field.'));
              }
            });
            promises.push(deferred.promise);
          });

          return $q.all(promises);
        };

        /**
         * Run preprocessing function
         * @param  {string} name - function to run
         * @param  {[type]} dm - data model to pass to function
         */
        advancedProtocol.run = function(name, dm) {
          try {
            return advancedProtocol.registry[name](dm);
          } catch (e) {
            logger.error('Error caught while preprocessing with ' + name + '. Traceback: ' + angular.toJson(e));
          }
        };

        return advancedProtocol;

    });

});
