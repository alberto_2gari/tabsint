/* jshint bitwise: false */
/* globals ChaWrap: false */

/**
 * Created by mls on 7/11/2014.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.services.cha.exams', [])
    .factory('chaExams', function ($q, $timeout, page, cha, chaResults, logger, media, notifications) {

      var api = {
        polling: undefined,
        cancelPolling: undefined,
        setup: undefined,
        wait: undefined,
        reset: undefined,
        getChaInfo: undefined,
        state: undefined,
        storage: {},
        clearStorage: undefined,
        examType: undefined,
        complexExamType: undefined,
        examProperties: undefined
      };

      // The storage field provides storage for the current state in a complex exam, such as audiometry-list
      // The storage field is cleared using clearStorage on certain events using the plugins onEvent api
      api.clearStorage = function(){
        api.storage = {};
      };

      var exams = {
        HughsonWestlake: {},
        HughsonWestlakeFrequency: {},
        BekesyLike: {},
        BekesyFrequency: {},
        BHAFT: {},
        ThreeDigit: {},
        MLD: {},
        HINT: {},
        ThirdOctaveBands: {},
        ToneGeneration: {},
        DPOAE: {},
        TAT: {}
      };

      function isNumber(n) {
        if (typeof(n) === 'number') {return true; }
        else {return false; }
      }

      // Exam Reset Functions
      api.reset = function() {
        api.examType = undefined;
        api.examProperties = {};
        api.complexExamType = undefined;
        return api.resetPage();
      };

      api.resetPage = function() {
        if (angular.isDefined(page.dm)) {
          page.dm.hideProgressbar = true;
        }

        api.cancelPolling();
        return cha.abortExams()
          .then(cha.stopNoiseFeature)
          .then(cha.disconnectA2DP);
      };


      // Setup Functions
      api.setup = function(examType, examProp) {
        api.examType = examType;
        var properties = examProp || {};

        // must convert to an examProperties object that can be sent to the CHA
        if (exams[api.examType].hasOwnProperty('setup')) {
          api.examProperties = exams[api.examType].setup(properties) || {};
        } else {
          api.examProperties = properties;
        }
      };

      exams.ThirdOctaveBands = {
        setup: function(examProp){
          if (page.dm.responseArea.measureBothEars || angular.isUndefined(examProp.InputChannel)){
            // go to default left input channel
            examProp.InputChannel = 'SMICR0';
          }
          return examProp;
        }
      };

      exams.DPOAE = {
        setup: function(examProp) {
          if (examProp.F2 && !examProp.F1) {
            examProp.F1 = examProp.F2/1.2; // F2 = 1.2*F1
          }
          return examProp;
        }
      };

      exams.MLD = {
        setup: function(examProp) {
          if (angular.isUndefined(examProp.UseSoftwareButton)) {
            examProp.UseSoftwareButton = true;
          }
          return examProp;
        }
      };

      exams.TAT = {
        setup: function(examProp) {
          if(angular.isDefined(examProp.Ear)){
            var ear = String(examProp.Ear);
            if(ear.toLowerCase() === "left"){
              examProp.Ear = 0;
            } else if(ear.toLowerCase() === "right"){
              examProp.Ear = 1;
            } else if (ear.toLowerCase() === 'both'){
              examProp.Ear = 2;
            } else {
              examProp.Ear = 0; // default to Left
            }
          }
          else {
            examProp.Ear = 0; //default to Left
          }
          return examProp;
        }
      };

      exams.HughsonWestlake = {
        setup: function(examProp) {

          // Dynamic Start Level
          if (examProp.DynamicStartLevel){
            examProp.Lstart = calculateDynamicStartLevel(examProp.DynamicStartLevel, examProp.Lstart);
            delete examProp.DynamicStartLevel;
          }

          // Relative Frequency
          if (examProp.RelativeF) {
            examProp.F = calculateRelativeF(examProp.F, examProp.RelativeF);
            delete examProp.RelativeF;
          }

          return examProp;

          function calculateDynamicStartLevel(dynStartLevel, Lstart){
            var base, offset, ret;
            var idList = dynStartLevel.baseIdList || ['HW1000'];
            var resultsList = chaResults.getPastResults(idList, ['HughsonWestlake']);

            for (var i = resultsList.length-1; i>=0; i--){
              if (resultsList[i].ResultType === 'Threshold'){
                base = resultsList[i].Threshold;
                break;
              }
            }

            if (angular.isUndefined(base)){
              ret = angular.isDefined(Lstart)? Lstart : 40 ;
            } else {
              offset = angular.isDefined(dynStartLevel.offset) ? dynStartLevel.offset : 15;
              Lstart = angular.isDefined(Lstart) ? Lstart : 40;
              ret = Math.max(Lstart, base+offset);
            }

            //console.log('newLstart: '+ret+', from offset: '+offset+', base: '+base);

            return ret;
          }

          function calculateRelativeF(F, rel) {
            var LUT, i;
            if (typeof(rel[1]) !== 'number' || typeof(rel[2]) !== 'number') {
              logger.warn('Numerator or Denominator in relative frequency conditions are not specified as numbers, returning un-modified HAF');
              return F;
            }
            if (angular.isDefined(rel[3])){
              // protocol defined a look up table - use that
              if (page.dm.lookUpTables){
                _.forEach(page.dm.lookUpTables,function(lut){
                  if (lut.name === rel[3]){
                    LUT = lut.table;
                  }
                });
              }

              if (angular.isUndefined(LUT)){
                return F;
              }
              var lutInd = LUT.indexOf(F);

              if (rel[0] === 'below') {
                if (lutInd < 0){
                  for (i = 0; i < LUT.length; i++){
                    if (LUT[i] > F){
                      lutInd = i;
                      break;
                    }
                  }
                }
                if (lutInd -rel[1] >= 0) {
                  return LUT[lutInd - rel[1]];
                } else {
                  logger.warn('CHA calculateRelativeF reached lowest value in look up table.  HAF = '+F+', index = '+lutInd+', step = '+rel[1]);
                  return LUT[0];
                }
              } else if (rel[0] === 'above') {
                if (lutInd < 0){
                  for (i = LUT.length; i >= 0; i--){
                    if (LUT[i] < F){
                      lutInd = i;
                      break;
                    }
                  }
                }
                if (lutInd + rel[1] < LUT.length) {
                  return LUT[lutInd + rel[1]];
                } else {
                  logger.warn('CHA calculateRelativeF reached highest value in look up table.  HAF = '+F+', index = '+lutInd+', step = '+rel[1]);
                  return LUT[LUT.length-1];
                }
              }
            } else {
              // no lookup table provided, use math
              if (rel[0] === 'below') {
                return Math.round(F * Math.pow(2, -rel[1] / rel[2]));
              } else if (rel[0] === 'above') {
                return Math.round(F*Math.pow(2, rel[1]/rel[2]));
              }
            }
          }

        }
      };


      api.getChaInfo = function(){
        var chaInfo = {
          serialNumber: cha.info.id.serialNumber,
          buildDateTime: cha.info.id.buildDateTime,
          probeId: cha.info.probeId,
          vBattery: cha.info.vBattery
        };

        return chaInfo;
      };


      api.wait = {
        forReadyState: function(initialDelay, intervalDelay) {
          var deferredExam = $q.defer();
          if (angular.isUndefined(initialDelay)) {initialDelay = 500;}
          if (angular.isUndefined(intervalDelay)) {intervalDelay = 500;}
          logger.debug('CHA - Beginning to poll status every ' + intervalDelay +' ms, after a delay of ' + initialDelay + ' ms.');

          api.cancelPolling();
          $timeout(function() {
            api.polling = setInterval(function () {
              //console.log('-- cha wait.forReadyState polling');
              return cha.requestStatus()
                .then(function (status) {
                  //console.log('-- cha wait.forReadyState returned status: ' + JSON.stringify(status));
                  if (status.State === 1) {
                    api.cancelPolling();
                    deferredExam.resolve();
                  }
                }, function (err) {
                  clearInterval(api.polling);
                  deferredExam.reject(err);
                });
            }, intervalDelay);
          }, initialDelay);

          return deferredExam.promise;
        }
      };

      api.cancelPolling = function() {
        try {
          clearInterval(api.polling);
        } catch(e) {
          logger.debug('CHA - Faild to clear results polling in chaExams with error: ' + JSON.stringify(e));
        }
      };

      api.playSound = function(path1, path2) {
        path1 = path1 || 'C:HINT/LIST1/TIS001.WAV';
        path2 = path2 || 'C:HINT/NOISE_R.WAV';
        return cha.requestStatus()
          .then(function(status) {
            status.vBattery = Math.round(status.vBattery*100)/100;
            logger.debug('CHA - Status before queueing exam: ' + angular.toJson(status));
            if (status.State === 2) {
              logger.warn('CHA - exam is still running while queuing playSound. Aborting exams...');
              return cha.abortExams();
            } else if (status.State !== 1) {
              return $q.reject({code: 66, msg: 'Cha is in an unknown state'});
            }
          })
          .then(function() {
            if (path2) {
              return cha.queueExam('PlaySound', {SoundFileName: path1, SecondSoundFileName: path2});
            } else {
              return cha.queueExam('PlaySound', {SoundFileName: path1});
            }

          })
          .then(api.wait.forReadyState)
          .then(function () {
            logger.debug('CHA - play sound(s) done');
          })
          .catch(function (e) {
            logger.error('CHA - play sound on failed with error: ' + JSON.stringify(e));
          });
      };

      api.playWavs = function(chaWavFiles) {
        if (chaWavFiles && chaWavFiles.length > 0) {
          return cha.requestStatus()
            .then(function (status) {
              status.vBattery = Math.round(status.vBattery * 100) / 100;
              logger.debug('CHA - Status before queueing exam: ' + angular.toJson(status));
              if (status.State === 2) {
                logger.warn('CHA exam is still running while user queues an exam. Aborting exams...');
                return cha.abortExams();
              } else if (status.State !== 1) {
                return $q.reject({code: 66, msg: 'Cha is in an unknown state'});
              }
            })
            .then(function () {

              function processWav(wav) {
                // assume files that don't start with C: are in the C:USER/ directory
                if (!wav.path.startsWith('C:')) {
                  wav.path = 'C:USER/' + wav.path;
                }

                if (!wav.Leq) {     // Leq default
                  wav.Leq = [72,72,0,0];
                } else if (wav.Leq.length === 2) {    // handle 2 single inputs
                  wav.Leq.concat(0,0);
                }
                return wav;
              }

              var wav = processWav(chaWavFiles[0]);
              var useMetaRMS = angular.isDefined(wav.useMetaRMS)? wav.useMetaRMS : false;
              var pse = {
                UseMetaRMS: useMetaRMS, // old way - need to make sure nobody depends on this: wav.path.startsWith('C:USER/'),
                SoundFileName: wav.path,
                Leq: wav.Leq
              };

              if (chaWavFiles.length === 2) {
                var wav2 = processWav(chaWavFiles[1]);
                pse.SecondSoundFileName = wav2.path;
                pse.SecondLeq = wav2.Leq;
                // pse.SecondFileDelay = wav2.SecondFileDelay || 0;
              }

              return cha.queueExam('PlaySound', pse);
            })
            .then(api.wait.forReadyState)
            .then(cha.requestResults)
            .then(function(result){
              // should be {State: 3}
            })
            .then(function () {
              logger.debug('CHA - playWavs done');
            })
            .catch(function (e) {
              logger.error('CHA - playWavs failed with error: ' + JSON.stringify(e));
            });
        }
      };

      api.startTalkThrough = function() {
        var deferredA2DP = $q.defer();
        logger.debug('CHA - starting talkThrough');

        media.stopAudio();

        cha.requestStatus()
          .then(function(status) {
            logger.debug('CHA - Status before queueing talkThrough: ' + angular.toJson(status));
            if (status.State === 2) {
              logger.warn('CHA - CHA exam is still running while user starts talkThrough. Aborting exams...');
              return cha.abortExams();
            } else if (status.State !== 1) {
              return $q.reject({code: 66, msg: 'Cha is in an unknown state'});
            }
          })
          .then(cha.connectA2DP)
          .then(cha.checkA2DPConnection)
          .then(function() {
            // TalkThrough times out after 5 seconds so CHA doesn't get stuck in headset mode if connection dies.
            return cha.queueExam('TalkThrough', {});
          })
          .then(function() {
            deferredA2DP.resolve();
            return api.wait.forReadyState(); // need to poll, to keep the TalkThrough alive.
          })
          .then(function() {
            // this shouldn't happen - it should be killed on pageEnd
            logger.debug('CHA - TalkThrough interval ended');
            //cha.abortExams(); // TODO unnecessary?
          })
          .catch(function(err) {
            var msg = '';
            logger.error('CHA - TalkThrough failed with error: ' + JSON.stringify(err));
            if (err.code && err.code === 64) {
              msg = 'Connection with the wireless headset was lost while streaming audio.  ' +
                    'The audio may start playing through the tablet speakers.  ' +
                    'Please hand the tablet to an administrator.';
            } else if (err.code && (err.code === 554 || err.code === 557)) {
              msg = 'The tablet is not set up to stream to the headset. Please hand the tablet to an administrator. \n\nTo set up the streaming connection, try reconnecting to the headset.';
            } else if (err.code && (err.code === 558 || err.code === 555 || err.code === 560)) {
              msg = 'The tablet failed to set up a streaming connection. Please hand the tablet to an administrator.';
            } else {
              msg = 'The tablet was unable to initialize streaming to the WAHTS.  Please hand the tablet to an administrator.';
              deferredA2DP.reject('CHA Streaming not Ready');
            }
            media.stopAudio();
            cha.abortExams();
            notifications.alert(msg);
          });

        return deferredA2DP.promise;
      };

      api.stopTalkThrough = function() {
        logger.debug('CHA - stopping talkThrough');
        media.stopAudio();
        api.reset();
      };


      return api;
    });


});
