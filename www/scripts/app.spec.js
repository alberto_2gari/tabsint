

define(['test-dep','app'], function () {
  'use strict';

  beforeEach(function() {
    localStorage.clear();
  });

  describe('Exam Controller: ExamCtrl', function () {

    // load the controller's module
    beforeEach(module('tabsint'));

    beforeEach(inject(function (_$httpBackend_){
      _$httpBackend_.whenGET('res/translations/translations.json')
        .respond({});
    }));

    var TabsintCtrl, scope, app;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
      $rootScope.$digest(); // necessary to get app.run to fire all the way through

      //load new scope and then load controller
      scope = $rootScope.$new();
      $controller('TabsintCtrl', {
        $scope: scope
      });

    }));


    it('should have the router on its scope', function () {
      expect(scope.router).toBeDefined();
    });

    it('should be on the welcome page', function () {
      expect(scope.router.page).toEqual('WELCOME');
    });


  });


});
