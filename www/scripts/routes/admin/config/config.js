
define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.routes.admin.config', [])

    .directive('configView', function () {
      return {
        restrict: 'E',
        controller: 'ConfigCtrl',
        templateUrl: 'scripts/routes/admin/config/config.html',
        scope: {}
      };
    })

   .controller('ConfigCtrl', function ($scope, $q, $interval, plugins, adminLogic, disk, protocol, pm, app,
                                      media, config, devices, notifications, tasks, remote, logger, results,
                                      gitlab, tabsintServer, logExport, $sce, version, networkModel, $http,
                                      $cordovaFileTransfer, paths, tabsintNative, changePin, gettextCatalog, localServer) {
      $scope.pm = pm;
      $scope.plugins= plugins;
      $scope.devices = devices;
      $scope.adminLogic = adminLogic;
      $scope.disk = disk;
      $scope.config = config;
      $scope.gitlab = gitlab;
      $scope.tabsintServer = tabsintServer;
      $scope.localServer = localServer;
      $scope.tasks = tasks;
      $scope.logExport = logExport;
      $scope.remote = remote;
      $scope.version = version;
      $scope.networkModel = networkModel;
      $scope.changePin = changePin;

      // Placeholders for release information gathered from gitlab API in `checkGitlabRelease`
      $scope.latestRelease = undefined;
      $scope.latestReleaseUrl = undefined;

      /**
       * Check Github API for releases 
       */
      function checkGitlabRelease() {
        var projectUrl = `${config.gitlab.creareHost}${gitlab.api}projects/${config.releases.id}`;
        var tagsUrl = `${config.gitlab.creareHost}${gitlab.api}projects/${config.releases.id}/repository/tags`;
        var token = config.releases.token ? config.releases.token : config.gitlab.creareToken;  // see if config overrides default token
        var webUrl;  // to be returned by project API

        tasks.register('check-release', 'Checking latest TabSINT release');
        gitlab.get(projectUrl, token)
          .then(function(ret) {
            webUrl = ret.data.web_url;   // save the project's web url for use below
            return gitlab.get(tagsUrl, token);
          })
          .then(function(ret) {
            if (ret.data.length > 0) {

              // find the most recent tag
              var commits = _.map(ret.data, 'commit');
              var dates = _.map(commits, function(c) {
                var d = new Date(c.committed_date).toJSON();
                return d;
              });
              var maxVersion = angular.copy(dates).sort().reverse()[0]; // sorts in ascending order, need to flip with reverse...
              var ind = _.findIndex(dates, function(d) {return d === maxVersion});
              logger.info(`Most recent release tag: ${ret.data[ind].name}`);
              $scope.latestRelease = ret.data[ind].name;  // set latest release name to the scope so we can see it on the config page
              try {
                $scope.latestReleaseUrl = `${webUrl}${ret.data[ind].release.description.split('[tabsint.apk]')[1].split('(')[1].split(')')[0]}`;  // convoluted string parsing
              } catch(e) {
                logger.error('Failed to parse latestReleaseUrl from release notes');
              }
            } else {
              logger.error(`No tags found at url while checking for latest release: ${tagsUrl}`);
            }
          })
          .catch(function(e) {
            if (e && e.msg) {
              logger.error(`Error while checking for latest release: ${e.msg}`);
            } else {
              logger.error(`Unknown error while checking for latest release: ${angular.toJson(e)}`);
            }
          })
          .finally(function() {
            tasks.deregister('check-release');
          });
      }

      /**
       * Check the latest release of TabSINT.
       * If user presses the button, it will take them directly to the config defined release page.
       * If this is called internally, it will try to use the API to find the name of the latest release tag 
       *   and url of the latest release binary.
       * @param  {boolean} userPressed - boolean dictating if the method was called by button press or internal function
       * @return {[type]}             [description]
       */
      $scope.checkLatestRelease = function(userPressed) {
        disk.lastReleaseCheck = (new Date()).toJSON();
        if (userPressed) {
          $scope.viewReleases();
        } else if (config.releases && config.releases.id) {
          checkGitlabRelease();
        }
      };

      // perform this every time the user comes to the admin page
      if (app.tablet) {
        if (disk.lastReleaseCheck) {
          var dReleaseCheck = (new Date()) - (new Date(disk.lastReleaseCheck));  // get the amount of time since last release check (in ms)
          if (dReleaseCheck/60000/60/24 > 1) {
            $scope.checkLatestRelease();
          }
        } else {
          $scope.checkLatestRelease();
        }
      }

      /**
       * Method to download latest release automatically from gitlab, if the tag and url of binary has already been found using `checkLatestRelease`.
       * This method gets called synchronously and will block the browser while downloading.
       * Would like to have this non-blocking in the future.
       *
       * If no release url has been found, this opens the release page via `viewReleases()`
       */
      $scope.getLatestRelease = function() {
        if ($scope.latestRelease && $scope.latestReleaseUrl) {
          notifications.confirm(gettextCatalog.getString('Downloading TabSINT will take a few minutes depending on your internet connection.'),
            function (buttonIndex){
              if (buttonIndex === 1){
                remote.link($scope.latestReleaseUrl);
              }
            });
        } else {
          $scope.viewReleases();
        }
      };

      /**
       * View releases url. Defaults to the open source tabsint page.
       */
      $scope.viewReleases = function() {
        if (config.releases.url) {
          remote.link(config.releases.url);
        } else {
          remote.link('https://gitlab.com/creare-com/tabsint/tags');
        }
      };

      // Popover text
      $scope.adminPopover = $sce.trustAsHtml(gettextCatalog.getString('Includes additional configuration options, displays expandable <b>debug</b> menus showing program state at the bottom of exam pages, and suppresses Admin Password prompts'));
      $scope.externalControlPopover = $sce.trustAsHtml(gettextCatalog.getString('Enable control of protocol flow using external tools like Matlab.'));
      $scope.disableLogsPopover = $sce.trustAsHtml(gettextCatalog.getString('Disable log messages from being stored and uploaded. <br><br>Logs are useful for investigating software bugs, but may introduce privacy concerns. Disable logging anytime sensitive data is being collected.'));
      $scope.disableVolumePopover = $sce.trustAsHtml(gettextCatalog.getString('This option will disable TabSINT from setting the volume to 100% on every page. This feature is essential to the functionality of TabSINT while playing calibrated audio through the speaker.<br><br>' +
        'Check this box if you would like to set the volume of the app manually using the buttons on the side of the device. <br><br>In almost all cases, this box should remain unchecked.'));
      $scope.adminSkipModeControlPopover = $sce.trustAsHtml(gettextCatalog.getString('This option will enable skipping on every page of an Exam. This feature is should only be enabled while developing or debugging, and may cause issues with some protocols.'));
      $scope.languagePopover = $sce.trustAsHtml(gettextCatalog.getString('Select preferred language for the application. This language will be used where supported. Otherwise, English will be used. Note this cannot change any text configured in protocols.'));

      $scope.headsetPopover = $sce.trustAsHtml(gettextCatalog.getString('Select the headset you will use to adminster hearing tests. <br><br> If you are using audio files calibrated for a specific headset, ' +
          'this value must match the value in the protocol\'s <code>calibration.json</code> file.'));
      $scope.tabletPopover = $sce.trustAsHtml(gettextCatalog.getString('The model of the mobile device being used to run TabSINT. <br><br> If you are using audio files calibrated for a specific tablet model, ' +
          'this value must match the value in <code>calibration.json</code>.'));

      $scope.latestReleasePopover = $sce.trustAsHtml(gettextCatalog.getString('Download the latest TabSINT version.  ' +
          'Swipe down from top of screen to open status bar and follow download progress.  ' +
          'Once complete, tap `tabsint.apk` in the status bar or downloads directory to install.'));

      $scope.interAppNamePopover = $sce.trustAsHtml('Package name of other app.  example:  com.companyname.appname');
      $scope.interAppMessagePopover = $sce.trustAsHtml('Message to send');
      $scope.interAppDataPopover = $sce.trustAsHtml('Data to send');
      $scope.interAppSendPopover = $sce.trustAsHtml('Send message and data to app specified.');
      $scope.interAppLastMessagePopover = $sce.trustAsHtml('Last message and data received from another app');

      $scope.adminPinPopover = $sce.trustAsHtml(gettextCatalog.getString('Change the Admin PIN to any numerical value.  This PIN is required to switch to Admin View and to reset exams when Admin Mode is off.'));

      $scope.automaticallyOutpuResultsPopover = $sce.trustAsHtml('Automatically upload or export the result when a test is finished. The result will be uploaded or exported on the <b>Exam Complete</b> page. <br><br> Once the result is uploaded to a server or exported to a local file, it will be removed from TabSINT.');

      // log messages
      $scope.logs = {
        count: logger.getCount,
        show: false,
        nLogs: 50,
        disp: []
      };

      // method to show logs
      $scope.displayLogs = function() {
        // get logs from sqlite if the logs.disp array is less than the count
        if ($scope.logs.disp.length !== logger.getCount.logs) {
          logger.getLogs()
            .then(function(ret){
              $scope.logs.disp = ret;
            });
        }

        // toggle the well
        $scope.logs.show = !$scope.logs.show;
      };

      /**
       * Update language names from text catalog
       * TODO: Dynamically select languages available using res/translations.json
       * @return {object} dictionary of languages available displayed in the selected language
       */
      function updateLanguages() {
        return {
          en: gettextCatalog.getString('English'),
          ja: gettextCatalog.getString('Japanese'),   // not yet available in the open source build
          fr: gettextCatalog.getString('French')
        };
      }

      /**
       * Language selection method
       * @param  {string} language - language selected from the dropdown
       */
      $scope.changeLanguage = function(language) {
        disk.language = language;
        gettextCatalog.setCurrentLanguage(language);
        $scope.languages = updateLanguages();
        logger.info('Changed language to "' + disk.language + '" and saved it to disk');
      };

      // Object with language name available for choosing
      $scope.languages = updateLanguages();

      $scope.changeHeadset = function(headset) {
        if (headset === 'None') {disk.headset = undefined;}
        else {disk.headset = headset;}
        logger.info('Changed headset to "' + disk.headset + '" and saved it to disk');
      };

      // Object with headset and Display name available for choosing - must correspond with calibration names (see protocol_schema/headset)
      $scope.headsets = {
        None: 'None',
        HDA200: 'HDA 200',
        VicFirth: 'Vic Firth',
        WAHTS: 'WAHTS',
        Audiometer: 'Audiometer'
      };

      // Add plugin headsets
      _.forEach(plugins.elems.headsets, function(headset) {
        $scope.headsets[headset.id] = headset.display;
      });


      $scope.play1kHz94dB = function () {
        media.stopAudio();
        var wavfile = {
          'path': '/android_asset/www/res/wavs/1kHz_cal_tone.wav',
          'playbackMethod': 'arbitrary',
          'targetSPL': 94
        };

        // A seperate calibration is needed for each distinct tablet/headset
        // combination. Each calibration must have:
        //      {'wavRMSZ': 0.70231, 'RMSZ': 0.70231, 'scaleFactor': X.XXXXX};
        // The RMSs always have these values - a function of the wav file.
        // The scaleFactor is hardware dependent and can be found in the
        // tablet_headset-audio_profile.json file.
        if (disk.headset === 'VicFirth') {
          wavfile.cal = {'wavRMSZ': 0.70231, 'RMSZ': 0.70231, 'scaleFactor': 0.60027};
        } else if(disk.headset === 'HDA200') {
          wavfile.cal = {'wavRMSZ': 0.70231, 'RMSZ': 0.70231, 'scaleFactor': 0.27740};
        } else if(disk.headset === 'Creare Headset' || disk.headset === 'WAHTS'){
          wavfile.cal = {'wavRMSZ': 0.70231, 'RMSZ': 0.70231, 'scaleFactor': 0.13519823071552697 };
        } else if(disk.headset === 'Audiometer'){
            wavfile.cal = {'wavRMSZ': 1, 'RMSZ': 1, 'scaleFactor': 1 };
        } else {
          // default to VicFirth
          notifications.alert(gettextCatalog.getString('No calibrated sound available for this headset.'));
          return;
        }

        media.playWavCalTone(wavfile);
      };

      $scope.playCompAudio = function () {
        media.stopAudio();
        var startDelayTime = 0;
        media.playAudio([{src:'/android_asset/www/res/wavs/CompAudioTest.wav',vol:1.0}], startDelayTime);
      };

      $scope.toggleAutoUpload = function() {

        if (disk.autoUpload) {
          if (!networkModel.status) {
            notifications.alert(gettextCatalog.getString('The tablet is not currently online. Results will auto-upload when an internet connection is established.'));
          } else {
            notifications.alert(gettextCatalog.getString('Auto-upload Enabled. Results will auto-upload at the end of each exam and/or whenever a wifi or cellular internet connection is available.'));
            results.uploadAll();
          }
        } else {
          notifications.alert(gettextCatalog.getString('Auto-upload Disabled.  Please manually upload results often to ensure that data is not lost.'));
        }

        logger.info('Auto upload changed to: '+disk.autoUpload);
      };

    });
});
