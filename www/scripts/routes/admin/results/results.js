/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.routes.admin.results', [])

    .directive('resultsView', function () {
      return {
        restrict: 'E',
        controller: 'ResultsCtrl',
        templateUrl: 'scripts/routes/admin/results/results.html',
        scope: {}
      };
    })

    .controller('ResultsCtrl', function ($scope, logger, disk, results, notifications, localServer, tasks, $uibModal, networkModel, gettextCatalog) {
      $scope.disk = disk;
      $scope.localServer = localServer;
      $scope.tasks = tasks;
      $scope.uploadLogExpanded = false;

      $scope.viewResult = function(ind) {
        var modal = $uibModal.open({
          templateUrl: 'scripts/routes/admin/results/result.modal.html',
          controller: 'ResultViewCtrl',
          resolve: {
            ind: function () {
              return ind;
            }
          }
          // size: 'lg'
        });
      };

      // only app developer
      $scope.deleteAll = function(){
        notifications.confirm(gettextCatalog.getString('Are you sure you want to delete all result files? These results cannot be restored.'),
          function (buttonIndex){
            if (buttonIndex === 1){
              results.deleteAll();
            }
          }
        );

      };


    })

    .controller('ResultViewCtrl', function($scope, $uibModalInstance, results, disk, notifications, tasks, ind, gettextCatalog) {
      $scope.idx = ind;
      $scope.result = disk.queuedResults[ind];
      $scope.results = results;
      $scope.tasks = tasks;

      $scope.close = function() {
        $uibModalInstance.close();
      };

      $scope.delete = function() {
        notifications.confirm(gettextCatalog.getString('Are you sure you want to delete this exam? This action is irreversible.'),
          function (buttonIndex){
            if (buttonIndex === 1){
              results.delete($scope.idx);
              $scope.close();
            }
          },
          'Confirm Delete Result',
          ['OK', 'Cancel']);
      };

      $scope.upload = function() {
        results.upload($scope.idx);
        $scope.close();
      };

      $scope.export = function() {
        results.export($scope.idx);
        $scope.close();
      };

      $scope.uploadable = function() {
        return _.includes(['tabsintServer', 'gitlab'], $scope.result.testResults.protocol.server);
      };

    });


});
