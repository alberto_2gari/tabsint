/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */


define(['angular'], function (angular) {
  'use strict';

  angular.module('tabsint.routes.welcome', [])

    .directive('welcomeView', function () {
      return {
        restrict: 'E',
        templateUrl: 'scripts/routes/welcome/welcome.html',
        controller: 'WelcomeCtrl',
        scope: {}
      };
    })

    .controller('WelcomeCtrl', function ($scope, app, disk, examLogic, router, $uibModal, adminLogic) {
      $scope.app = app;
      $scope.disk = disk;
      $scope.router = router;

      $scope.switchToExamView = function() {
        examLogic.act.switchToExamView();
      };

      $scope.switchToAdminView = function() {
        adminLogic.switchToAdminView();
      };

      var DisclaimerInstanceCtrl = function ($scope, $uibModalInstance, disk) {
        $scope.ok = function() {
          $uibModalInstance.close();
          disk.init = false;
        };
      };

      if(disk.init){ // only run when the app loads for the first time (when disk.init is true)
        var modalInstance = $uibModal.open({
          templateUrl: 'scripts/routes/welcome/disclaimer.html',
          controller: DisclaimerInstanceCtrl //is there a reason not to use this simplistic logic?
        });
      }
    });
});
