/**
 * Created by mls on 7/8/2015.
 */


'use strict';


// TEST: find .spec files during karma tests
if (window.__karma__) {

  // finds files names test/spec/*.spec.js and pushes them into a list of tests. These will all be loaded as 'deps' in require.config
  var tests = [];
  for (var file in window.__karma__.files) {
    if (window.__karma__.files.hasOwnProperty(file)) {
      if (/spec\.js$/.test(file)) {
        tests.push(file);
      }
    }
  }
}


require.config({
    baseUrl: window.__karma__ ? '/base/www/scripts' : 'scripts',            // base path
    waitSeconds: 200,
    paths: {
      // dependency paths (leave off .js) - these are relative to the baseUrl above
      angular: '../bower_components/angular/angular.min',
      jquery: '../bower_components/jquery/dist/jquery.min',
      lodash: '../bower_components/lodash/dist/lodash.min',
      es6shim: '../bower_components/es6-shim/es6-shim.min',
      d3: '../bower_components/d3/d3.min',
      angularGettext: '../bower_components/angular-gettext/dist/angular-gettext.min',
      ngStorage: '../bower_components/ngstorage/ngStorage.min',
      ngSanitize: '../bower_components/angular-sanitize/angular-sanitize.min',
      'ui.bootstrap': '../bower_components/angular-bootstrap/ui-bootstrap-tpls.min',
      tv4: '../bower_components/tv4/tv4',
      ngCordova: '../bower_components/ngCordova/dist/ng-cordova.min',
      jsonFormatter: '../bower_components/json-formatter/dist/json-formatter.min',

      // set up groups of scripts to load here:
      plugins: '../tabsint_plugins',  // tabsint-plugins path

      // TEST dependencies:
      'angular-mocks': '../bower_components/angular-mocks/angular-mocks',
      test: window.__karma__ ? '../../test/spec' : '../'

    },
    shim: {  // shims are for dependencies that aren't AMD compatible. I've included all dependencies here
      angular: {exports: 'angular'},
      ngSanitize : {deps: ['angular']},
      ngStorage: {deps: ['angular']},
      ngCordova: {deps: ['angular']},
      jsonFormatter: {deps: ['angular']},
      angularGettext: {deps: ['angular']},
      'ui.bootstrap' : {deps: ['angular']},
      lodash: {exports: '_'},
      es6shim: {exports: 'es6shim'},
      d3: {exports: 'd3'},   // might not need this
      //tv4: {exports: 'tv4'},

      // TEST:
      'angular-mocks': {
        deps: ['angular'],
        exports:'angular.mock'
      }
    },

    deps: window.__karma__ ? tests : null,
    callback: window.__karma__ ? window.__karma__.start : null

});



require(['app'], function () {
  // manually bootstrap app together, putting it on the document element (instead of ng-app)

  if (window.__karma__) {
    console.log('-- Testing:')
  } else {
    angular.bootstrap(document, ['tabsint']);
  }

});

// TEST: define test dependencies for requiring in each .spec file
define('test-dep', ['angular-mocks'], function () {});
